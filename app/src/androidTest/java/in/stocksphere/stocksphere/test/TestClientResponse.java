package in.stocksphere.stocksphere.test;

import junit.framework.TestCase;

//import in.stocksphere.stocksphere.data.async.BackgroundTask;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import in.stocksphere.stocksphere.Logger;
import in.stocksphere.stocksphere.data.rest.APIHelper;
import in.stocksphere.stocksphere.data.rest.SSAPI;
import in.stocksphere.stocksphere.data.common.SSConstants;
import in.stocksphere.stocksphere.data.rest.RequestObjectCreator;
import in.stocksphere.stocksphere.data.models.CompanyStatus;
import in.stocksphere.stocksphere.data.models.SSBase;
import in.stocksphere.stocksphere.data.models.Status;
import in.stocksphere.stocksphere.data.models.UserList;
import in.stocksphere.stocksphere.data.models.UserStatus;
import in.stocksphere.stocksphere.data.models.WatchList;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * Created by srishti on 13/06/15.
 */
public class TestClientResponse extends TestCase {

    /** Countdown latch */
    private CountDownLatch lock = new CountDownLatch(1);


    /** TESTS CASES **/
    public void testGetWatchlistResponse() throws Exception{

        int userId = 2;

        SSAPI ssapi = APIHelper.getSsapi();
        ssapi.getWatchlist(RequestObjectCreator.getWatchlistObject(userId), new Callback<WatchList>() {
            @Override
            public void success(WatchList watchList, Response response) {
                Logger.log("get watchlist successful" + watchList.getUserId());
            }

            @Override
            public void failure(RetrofitError error) {
                Logger.log("Exception in get watchlist :> " + error);
            }
        });

        lock.await(2000, TimeUnit.MILLISECONDS);
    }

    public void testUpdateWatchlist() throws Exception {

        int userId = 1;
        boolean toAdd = true;
        String stockSymbol = "$SBIN";
        double minPrice  = 250;
        double maxPrice = 300;
        boolean isPushNotifEnabled = true;

        SSAPI ssapi = APIHelper.getSsapi();
        ssapi.updateWatchlist(RequestObjectCreator.getUpdateWatchlistObject(userId,stockSymbol,toAdd, minPrice, maxPrice, isPushNotifEnabled), new Callback<SSBase>() {
            @Override
            public void success(SSBase SSBase, Response response) {
                Logger.log("update watchlist successful" + SSBase.toString());
            }

            @Override
            public void failure(RetrofitError error) {
                Logger.log("Exception in update watchlist :> " + error);
            }
        });

        lock.await(2000, TimeUnit.MILLISECONDS);
    }

    public void testGetFavouriteStatusObject() throws Exception{

        String statusId = "556377da30042976553d392b";
        int userId = 5;
        boolean isFav = true;

        SSAPI ssapi = APIHelper.getSsapi();
        ssapi.favoriteStatus(RequestObjectCreator.getFavoriteStatusObject(statusId,userId,isFav), new Callback<SSBase>() {
            @Override
            public void success(SSBase SSBase, Response response) {
                Logger.log("Favorite successful" + SSBase.toString());
            }

            @Override
            public void failure(RetrofitError error) {
                Logger.log("Exception in Favorite status :> " + error);
            }
        });

        lock.await(2000, TimeUnit.MILLISECONDS);
    }

    public void testGetLikeStatusObject() throws Exception{

        String statusId = "556377da30042976553d392b";
        int userId = 5;
        boolean isLiked = true;

        SSAPI ssapi = APIHelper.getSsapi();
        ssapi.likeStatus(RequestObjectCreator.getLikeStatusObject(statusId,userId,isLiked), new Callback<SSBase>() {
            @Override
            public void success(SSBase SSBase, Response response) {
                Logger.log("Like successful" + SSBase.toString());
            }

            @Override
            public void failure(RetrofitError error) {
                Logger.log("Exception in like status :> " + error);
            }
        });

        lock.await(2000, TimeUnit.MILLISECONDS);
    }

    public void testGetFollowUserObject() throws Exception{

        int followeeId = 4;
        int followerId = 5;
        boolean isFollowing = true;

        SSAPI ssapi = APIHelper.getSsapi();
        ssapi.followUser(RequestObjectCreator.getFollowUserObject(followeeId,followerId,isFollowing),new Callback<SSBase>() {
            @Override
            public void success(SSBase SSBase, Response response) {
                Logger.log("Success follow "+ SSBase.toString());
            }

            @Override
            public void failure(RetrofitError error) {
                Logger.log("Exception in follow user :> " + error);
            }
        });

        lock.await(2000, TimeUnit.MILLISECONDS);
    }

    public void testAddUserObject() throws Exception{

        String userName = "vim";
        String firstName = "vim";
        String lastName = "sag";
        String email = "vim@vima.com";
        String phone = "+91-9916607244";
        String bio = "hi";
        String dob = "13-12-1989";

//        SSAPI ssapi = ClientHelper.getSsapi();
//        ssapi.addUser(RequestObjectCreator.addUserObject(userName,firstName,lastName,email,phone,bio,dob),new Callback<User>() {
//            @Override
//            public void success(User user, Response response) {
//                Logger.log("User created");
//                Logger.log(user.toString());
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                Logger.log("Unable to create user :> " + error);
//            }
//        });

        lock.await(2000, TimeUnit.MILLISECONDS);
    }

    public void testUserStatusUpdatesObject() throws Exception{

        int userId = 1;

        SSAPI ssapi = APIHelper.getSsapi();
        ssapi.getUserStatusUpdates(RequestObjectCreator.getUserStatusUpdatesObject(userId,userId), new Callback<UserStatus>() {
            @Override
            public void success(UserStatus userStatus, Response response) {

                Logger.log("In Success..");
            }

            @Override
            public void failure(RetrofitError error) {

                Logger.log("UserStatus is null :> " + error);
            }
        });

        lock.await(2000, TimeUnit.MILLISECONDS);
    }

    public void testUpdateStatusObject() throws Exception{

        int userId = 1;
        String statusText = "HI Android";
        String statusSentiment = SSConstants.SENTIMENT_NEUTRAL;

        SSAPI ssapi = APIHelper.getSsapi();
        ssapi.updateStatus(RequestObjectCreator.getUpdateStatusObject(userId,statusText, statusSentiment), new Callback<Status>() {
            @Override
            public void success(Status status, Response response) {
                Logger.log("Status updated successfully");
                Logger.log(status.toString());
            }

            @Override
            public void failure(RetrofitError error) {
                Logger.log("Unable to update status :> " + error);
            }
        });

        lock.await(2000, TimeUnit.MILLISECONDS);
    }

    public void testUserTimelineObject() throws Exception{

        int userId = 3;

        SSAPI ssapi = APIHelper.getSsapi();
        ssapi.getUserTimeline(RequestObjectCreator.getUserTimelineObject(userId), new Callback<UserStatus>() {
            @Override
            public void success(UserStatus userStatus, Response response) {
                Logger.log("In Success..");
            }

            @Override
            public void failure(RetrofitError error) {
                Logger.log("UserStatus is null :> " + error);
            }
        });

        lock.await(2000, TimeUnit.MILLISECONDS);
    }

    public void testUserFollowingObject() throws Exception{

        int userId = 2;

        SSAPI ssapi = APIHelper.getSsapi();
        ssapi.getUserFollowing(RequestObjectCreator.getUserFollowingObject(userId),new Callback<UserList>() {
            @Override
            public void success(UserList userList, Response response) {
                Logger.log("In Success..");
            }

            @Override
            public void failure(RetrofitError error) {
                Logger.log("userList is null :> " + error);
            }
        });

        lock.await(2000, TimeUnit.MILLISECONDS);
    }

    public void testUserFollowerObject() throws Exception{

        int userId = 1;

        SSAPI ssapi = APIHelper.getSsapi();
        ssapi.getUserFollowers(RequestObjectCreator.getUserFollowerObject(userId),new Callback<UserList>() {
            @Override
            public void success(UserList userList, Response response) {
                Logger.log("In Success..");
            }

            @Override
            public void failure(RetrofitError error) {
                Logger.log("userList is null :> " + error);
            }
        });

        lock.await(2000, TimeUnit.MILLISECONDS);
    }

    public void testGetStatusById() throws Exception{

        String statusId = "556377da30042976553d392b";

        SSAPI ssapi = APIHelper.getSsapi();
        ssapi.getStatusById(statusId,1,new Callback<Status>() {
            @Override
            public void success(Status status, Response response) {
                Logger.log("status.toString() = " + status.toString());
            }

            @Override
            public void failure(RetrofitError error) {
                Logger.log("Status is null :> " + error);
            }
        });

        lock.await(2000, TimeUnit.MILLISECONDS);
    }


    public void testGetUserByEmail() throws Exception{

        String email = "abc@gmail.com";

//        SSAPI ssapi = ClientHelper.getSsapi();
//        ssapi.getUserByEmail(email, new Callback<User>() {
//            @Override
//            public void success(User user, Response response) {
//                Logger.log("in Success");
//                Logger.log("user.toString() = " + user.toString());
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                Logger.log("in failure");
//            }
//        });
//
//        lock.await(2000, TimeUnit.MILLISECONDS);
    }

    public void testGetFollowCompanyObject() throws Exception{

        String ssSymbol= "$AXIS";
        int followerId = 5;
        boolean isFollowing = false;

        APIHelper.getSsapi().followCompany(RequestObjectCreator.getFollowCompanyObject(followerId,ssSymbol,isFollowing), new Callback<SSBase>() {
            @Override
            public void success(SSBase SSBase, Response response) {

                System.out.println("Success follow "+ SSBase.toString());
            }

            @Override
            public void failure(RetrofitError error) {

                Logger.log("Exception in follow company :> " + error);
            }
        });

        lock.await(2000, TimeUnit.MILLISECONDS);
    }

    public void testCompanyTimelineObject() throws Exception{

        String ssSymbol= "$INFY";

        SSAPI ssapi = APIHelper.getSsapi();
        ssapi.getCompanyTimeline(RequestObjectCreator.getCompanyStatusUpdatesObject(ssSymbol,3), new Callback<CompanyStatus>() {
            @Override
            public void success(CompanyStatus companyStatus, Response response) {

                Logger.log("CompanyStatus received :> " + companyStatus.getStatuses().get(0).getStatusText());
            }

            @Override
            public void failure(RetrofitError error) {
                Logger.log("CompanyStatus is null :> " + error);
            }
        });

        lock.await(2000, TimeUnit.MILLISECONDS);
    }
}
