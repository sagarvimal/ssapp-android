package in.stocksphere.stocksphere.data.models;

import java.util.ArrayList;

public class UserList extends SSBase {

    private long userId;

    private String ssSymbol;

    private ArrayList<User> followers;

    private ArrayList<User> followingUsers;

    private ArrayList<User> watchingUsers;

    private ArrayList<User> statusLikedUsers;

    private ArrayList<User> statusFavouritedUsers;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public ArrayList<User> getFollowers() {
        return followers;
    }

    public void setFollowers(ArrayList<User> followers) {
        this.followers = followers;
    }

    public ArrayList<User> getFollowingUsers() {
        return followingUsers;
    }

    public void setFollowingUsers(ArrayList<User> followingUsers) {
        this.followingUsers = followingUsers;
    }

    public String getSsSymbol() {
        return ssSymbol;
    }

    public void setSsSymbol(String ssSymbol) {
        this.ssSymbol = ssSymbol;
    }

    public ArrayList<User> getWatchingUsers() {
        return watchingUsers;
    }

    public void setWatchingUsers(ArrayList<User> watchingUsers) {
        this.watchingUsers = watchingUsers;
    }

    public ArrayList<User> getStatusLikedUsers() {
        return statusLikedUsers;
    }

    public void setStatusLikedUsers(ArrayList<User> statusLikedUsers) {
        this.statusLikedUsers = statusLikedUsers;
    }

    public ArrayList<User> getStatusFavouritedUsers() {
        return statusFavouritedUsers;
    }

    public void setStatusFavouritedUsers(ArrayList<User> statusFavouritedUsers) {
        this.statusFavouritedUsers = statusFavouritedUsers;
    }

    @Override
    public String toString() {
        return "UserList{" +
                "userId=" + userId +
                ", ssSymbol='" + ssSymbol + '\'' +
                ", followers=" + followers +
                ", followingUsers=" + followingUsers +
                ", watchingUsers=" + watchingUsers +
                ", statusLikedUsers=" + statusLikedUsers +
                ", statusFavouritedUsers=" + statusFavouritedUsers +
                '}';
    }
}
