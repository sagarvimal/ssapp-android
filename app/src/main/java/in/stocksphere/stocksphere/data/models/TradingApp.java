package in.stocksphere.stocksphere.data.models;

import android.graphics.drawable.Drawable;

/**
 * Created by vtiwari on 10/3/15.
 */
public class TradingApp {

    private Drawable appIcon;

    private String appName;

    private String appPackageName;

    public Drawable getAppIcon() {
        return appIcon;
    }

    public void setAppIcon(Drawable appIcon) {
        this.appIcon = appIcon;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getAppPackageName() {
        return appPackageName;
    }

    public void setAppPackageName(String appPackageName) {
        this.appPackageName = appPackageName;
    }
}
