package in.stocksphere.stocksphere.data.sqlite.greendao;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.Property;
import de.greenrobot.dao.internal.DaoConfig;

import in.stocksphere.stocksphere.data.sqlite.greendao.StockWatchlist;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table STOCK_WATCHLIST.
*/
public class StockWatchlistDao extends AbstractDao<StockWatchlist, String> {

    public static final String TABLENAME = "STOCK_WATCHLIST";

    /**
     * Properties of entity StockWatchlist.<br/>
     * Can be used for QueryBuilder and for referencing column names.
    */
    public static class Properties {
        public final static Property Name = new Property(0, String.class, "name", false, "NAME");
        public final static Property PrimaryKey = new Property(1, String.class, "primaryKey", true, "PRIMARY_KEY");
        public final static Property SsSymbol = new Property(2, String.class, "ssSymbol", false, "SS_SYMBOL");
        public final static Property NseSymbol = new Property(3, String.class, "nseSymbol", false, "NSE_SYMBOL");
        public final static Property LastMarketPrice = new Property(4, Double.class, "lastMarketPrice", false, "LAST_MARKET_PRICE");
        public final static Property ChangePrice = new Property(5, Double.class, "changePrice", false, "CHANGE_PRICE");
        public final static Property PercentageChange = new Property(6, Double.class, "percentageChange", false, "PERCENTAGE_CHANGE");
        public final static Property OpenPrice = new Property(7, Double.class, "openPrice", false, "OPEN_PRICE");
        public final static Property HighPrice = new Property(8, Double.class, "highPrice", false, "HIGH_PRICE");
        public final static Property LowPrice = new Property(9, Double.class, "lowPrice", false, "LOW_PRICE");
        public final static Property PreviousClosePrice = new Property(10, Double.class, "previousClosePrice", false, "PREVIOUS_CLOSE_PRICE");
        public final static Property TradedVolume = new Property(11, Double.class, "tradedVolume", false, "TRADED_VOLUME");
        public final static Property TradedValue = new Property(12, Double.class, "tradedValue", false, "TRADED_VALUE");
        public final static Property LastPriceUpdateDate = new Property(13, java.util.Date.class, "lastPriceUpdateDate", false, "LAST_PRICE_UPDATE_DATE");
        public final static Property ClosingPrice = new Property(14, Double.class, "closingPrice", false, "CLOSING_PRICE");
        public final static Property Price52WkHigh = new Property(15, Double.class, "price52WkHigh", false, "PRICE52_WK_HIGH");
        public final static Property Price52WkLow = new Property(16, Double.class, "price52WkLow", false, "PRICE52_WK_LOW");
        public final static Property IsPushNotifEnabled = new Property(17, Boolean.class, "isPushNotifEnabled", false, "IS_PUSH_NOTIF_ENABLED");
        public final static Property MaxPriceNotif = new Property(18, Double.class, "maxPriceNotif", false, "MAX_PRICE_NOTIF");
        public final static Property MinPriceNotif = new Property(19, Double.class, "minPriceNotif", false, "MIN_PRICE_NOTIF");
        public final static Property ThisUserId = new Property(20, Long.class, "thisUserId", false, "THIS_USER_ID");
    };


    public StockWatchlistDao(DaoConfig config) {
        super(config);
    }
    
    public StockWatchlistDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /** Creates the underlying database table. */
    public static void createTable(SQLiteDatabase db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "'STOCK_WATCHLIST' (" + //
                "'NAME' TEXT," + // 0: name
                "'PRIMARY_KEY' TEXT PRIMARY KEY NOT NULL ," + // 1: primaryKey
                "'SS_SYMBOL' TEXT," + // 2: ssSymbol
                "'NSE_SYMBOL' TEXT," + // 3: nseSymbol
                "'LAST_MARKET_PRICE' REAL," + // 4: lastMarketPrice
                "'CHANGE_PRICE' REAL," + // 5: changePrice
                "'PERCENTAGE_CHANGE' REAL," + // 6: percentageChange
                "'OPEN_PRICE' REAL," + // 7: openPrice
                "'HIGH_PRICE' REAL," + // 8: highPrice
                "'LOW_PRICE' REAL," + // 9: lowPrice
                "'PREVIOUS_CLOSE_PRICE' REAL," + // 10: previousClosePrice
                "'TRADED_VOLUME' REAL," + // 11: tradedVolume
                "'TRADED_VALUE' REAL," + // 12: tradedValue
                "'LAST_PRICE_UPDATE_DATE' INTEGER," + // 13: lastPriceUpdateDate
                "'CLOSING_PRICE' REAL," + // 14: closingPrice
                "'PRICE52_WK_HIGH' REAL," + // 15: price52WkHigh
                "'PRICE52_WK_LOW' REAL," + // 16: price52WkLow
                "'IS_PUSH_NOTIF_ENABLED' INTEGER," + // 17: isPushNotifEnabled
                "'MAX_PRICE_NOTIF' REAL," + // 18: maxPriceNotif
                "'MIN_PRICE_NOTIF' REAL," + // 19: minPriceNotif
                "'THIS_USER_ID' INTEGER);"); // 20: thisUserId
    }

    /** Drops the underlying database table. */
    public static void dropTable(SQLiteDatabase db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "'STOCK_WATCHLIST'";
        db.execSQL(sql);
    }

    /** @inheritdoc */
    @Override
    protected void bindValues(SQLiteStatement stmt, StockWatchlist entity) {
        stmt.clearBindings();
 
        String name = entity.getName();
        if (name != null) {
            stmt.bindString(1, name);
        }
        stmt.bindString(2, entity.getPrimaryKey());
 
        String ssSymbol = entity.getSsSymbol();
        if (ssSymbol != null) {
            stmt.bindString(3, ssSymbol);
        }
 
        String nseSymbol = entity.getNseSymbol();
        if (nseSymbol != null) {
            stmt.bindString(4, nseSymbol);
        }
 
        Double lastMarketPrice = entity.getLastMarketPrice();
        if (lastMarketPrice != null) {
            stmt.bindDouble(5, lastMarketPrice);
        }
 
        Double changePrice = entity.getChangePrice();
        if (changePrice != null) {
            stmt.bindDouble(6, changePrice);
        }
 
        Double percentageChange = entity.getPercentageChange();
        if (percentageChange != null) {
            stmt.bindDouble(7, percentageChange);
        }
 
        Double openPrice = entity.getOpenPrice();
        if (openPrice != null) {
            stmt.bindDouble(8, openPrice);
        }
 
        Double highPrice = entity.getHighPrice();
        if (highPrice != null) {
            stmt.bindDouble(9, highPrice);
        }
 
        Double lowPrice = entity.getLowPrice();
        if (lowPrice != null) {
            stmt.bindDouble(10, lowPrice);
        }
 
        Double previousClosePrice = entity.getPreviousClosePrice();
        if (previousClosePrice != null) {
            stmt.bindDouble(11, previousClosePrice);
        }
 
        Double tradedVolume = entity.getTradedVolume();
        if (tradedVolume != null) {
            stmt.bindDouble(12, tradedVolume);
        }
 
        Double tradedValue = entity.getTradedValue();
        if (tradedValue != null) {
            stmt.bindDouble(13, tradedValue);
        }
 
        java.util.Date lastPriceUpdateDate = entity.getLastPriceUpdateDate();
        if (lastPriceUpdateDate != null) {
            stmt.bindLong(14, lastPriceUpdateDate.getTime());
        }
 
        Double closingPrice = entity.getClosingPrice();
        if (closingPrice != null) {
            stmt.bindDouble(15, closingPrice);
        }
 
        Double price52WkHigh = entity.getPrice52WkHigh();
        if (price52WkHigh != null) {
            stmt.bindDouble(16, price52WkHigh);
        }
 
        Double price52WkLow = entity.getPrice52WkLow();
        if (price52WkLow != null) {
            stmt.bindDouble(17, price52WkLow);
        }
 
        Boolean isPushNotifEnabled = entity.getIsPushNotifEnabled();
        if (isPushNotifEnabled != null) {
            stmt.bindLong(18, isPushNotifEnabled ? 1l: 0l);
        }
 
        Double maxPriceNotif = entity.getMaxPriceNotif();
        if (maxPriceNotif != null) {
            stmt.bindDouble(19, maxPriceNotif);
        }
 
        Double minPriceNotif = entity.getMinPriceNotif();
        if (minPriceNotif != null) {
            stmt.bindDouble(20, minPriceNotif);
        }
 
        Long thisUserId = entity.getThisUserId();
        if (thisUserId != null) {
            stmt.bindLong(21, thisUserId);
        }
    }

    /** @inheritdoc */
    @Override
    public String readKey(Cursor cursor, int offset) {
        return cursor.getString(offset + 1);
    }    

    /** @inheritdoc */
    @Override
    public StockWatchlist readEntity(Cursor cursor, int offset) {
        StockWatchlist entity = new StockWatchlist( //
            cursor.isNull(offset + 0) ? null : cursor.getString(offset + 0), // name
            cursor.getString(offset + 1), // primaryKey
            cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2), // ssSymbol
            cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3), // nseSymbol
            cursor.isNull(offset + 4) ? null : cursor.getDouble(offset + 4), // lastMarketPrice
            cursor.isNull(offset + 5) ? null : cursor.getDouble(offset + 5), // changePrice
            cursor.isNull(offset + 6) ? null : cursor.getDouble(offset + 6), // percentageChange
            cursor.isNull(offset + 7) ? null : cursor.getDouble(offset + 7), // openPrice
            cursor.isNull(offset + 8) ? null : cursor.getDouble(offset + 8), // highPrice
            cursor.isNull(offset + 9) ? null : cursor.getDouble(offset + 9), // lowPrice
            cursor.isNull(offset + 10) ? null : cursor.getDouble(offset + 10), // previousClosePrice
            cursor.isNull(offset + 11) ? null : cursor.getDouble(offset + 11), // tradedVolume
            cursor.isNull(offset + 12) ? null : cursor.getDouble(offset + 12), // tradedValue
            cursor.isNull(offset + 13) ? null : new java.util.Date(cursor.getLong(offset + 13)), // lastPriceUpdateDate
            cursor.isNull(offset + 14) ? null : cursor.getDouble(offset + 14), // closingPrice
            cursor.isNull(offset + 15) ? null : cursor.getDouble(offset + 15), // price52WkHigh
            cursor.isNull(offset + 16) ? null : cursor.getDouble(offset + 16), // price52WkLow
            cursor.isNull(offset + 17) ? null : cursor.getShort(offset + 17) != 0, // isPushNotifEnabled
            cursor.isNull(offset + 18) ? null : cursor.getDouble(offset + 18), // maxPriceNotif
            cursor.isNull(offset + 19) ? null : cursor.getDouble(offset + 19), // minPriceNotif
            cursor.isNull(offset + 20) ? null : cursor.getLong(offset + 20) // thisUserId
        );
        return entity;
    }
     
    /** @inheritdoc */
    @Override
    public void readEntity(Cursor cursor, StockWatchlist entity, int offset) {
        entity.setName(cursor.isNull(offset + 0) ? null : cursor.getString(offset + 0));
        entity.setPrimaryKey(cursor.getString(offset + 1));
        entity.setSsSymbol(cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2));
        entity.setNseSymbol(cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3));
        entity.setLastMarketPrice(cursor.isNull(offset + 4) ? null : cursor.getDouble(offset + 4));
        entity.setChangePrice(cursor.isNull(offset + 5) ? null : cursor.getDouble(offset + 5));
        entity.setPercentageChange(cursor.isNull(offset + 6) ? null : cursor.getDouble(offset + 6));
        entity.setOpenPrice(cursor.isNull(offset + 7) ? null : cursor.getDouble(offset + 7));
        entity.setHighPrice(cursor.isNull(offset + 8) ? null : cursor.getDouble(offset + 8));
        entity.setLowPrice(cursor.isNull(offset + 9) ? null : cursor.getDouble(offset + 9));
        entity.setPreviousClosePrice(cursor.isNull(offset + 10) ? null : cursor.getDouble(offset + 10));
        entity.setTradedVolume(cursor.isNull(offset + 11) ? null : cursor.getDouble(offset + 11));
        entity.setTradedValue(cursor.isNull(offset + 12) ? null : cursor.getDouble(offset + 12));
        entity.setLastPriceUpdateDate(cursor.isNull(offset + 13) ? null : new java.util.Date(cursor.getLong(offset + 13)));
        entity.setClosingPrice(cursor.isNull(offset + 14) ? null : cursor.getDouble(offset + 14));
        entity.setPrice52WkHigh(cursor.isNull(offset + 15) ? null : cursor.getDouble(offset + 15));
        entity.setPrice52WkLow(cursor.isNull(offset + 16) ? null : cursor.getDouble(offset + 16));
        entity.setIsPushNotifEnabled(cursor.isNull(offset + 17) ? null : cursor.getShort(offset + 17) != 0);
        entity.setMaxPriceNotif(cursor.isNull(offset + 18) ? null : cursor.getDouble(offset + 18));
        entity.setMinPriceNotif(cursor.isNull(offset + 19) ? null : cursor.getDouble(offset + 19));
        entity.setThisUserId(cursor.isNull(offset + 20) ? null : cursor.getLong(offset + 20));
     }
    
    /** @inheritdoc */
    @Override
    protected String updateKeyAfterInsert(StockWatchlist entity, long rowId) {
        return entity.getPrimaryKey();
    }
    
    /** @inheritdoc */
    @Override
    public String getKey(StockWatchlist entity) {
        if(entity != null) {
            return entity.getPrimaryKey();
        } else {
            return null;
        }
    }

    /** @inheritdoc */
    @Override    
    protected boolean isEntityUpdateable() {
        return true;
    }
    
}
