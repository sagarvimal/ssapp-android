package in.stocksphere.stocksphere.data.models;

import java.io.Serializable;

/**
 * Created by vtiwari on 7/29/15.
 * Stock info from google finance
 */
public class GFGetInfo implements Serializable{

    private String t; //ticker

    private String l; //last market price

    private String lt; // last trading time

    private String c; // change price

    private String cp; //percentage change price

    private String e; // exchange

    public String getT() {
        return t;
    }

    public void setT(String t) {
        this.t = t;
    }

    public String getL() {
        return l;
    }

    public void setL(String l) {
        this.l = l;
    }

    public String getLt() {
        return lt;
    }

    public void setLt(String lt) {
        this.lt = lt;
    }

    public String getC() {
        return c;
    }

    public void setC(String c) {
        this.c = c;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public String getE() {
        return e;
    }

    public void setE(String e) {
        this.e = e;
    }
}
