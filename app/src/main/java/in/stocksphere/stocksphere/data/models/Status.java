package in.stocksphere.stocksphere.data.models;

import java.util.ArrayList;

public class Status extends SSBase {

	private String id;

	private long userId;

    private String userName;

    private String userFullName;

	private String statusText;

	private boolean hasAttachment;

	private String attachmentId;

	private int cLikes;

	private int cFavorites;

	private int cReplies;

	private String replyOfId;

	private String sentiment;

	private String operation;

    private String userOfOperation;

    private String userNameOfOperation;

    private long userIdOfOperation;

    private boolean isLiked;

    private boolean isFavourited;

    private boolean isDeleted;

	private ArrayList<Status> statusReplies;

    private long userFbAndroidUId;

    private long userFbAndroidUIdOfOperation;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public boolean getHasAttachment() {
		return hasAttachment;
	}

	public void setHasAttachment(boolean hasAttachment) {
		this.hasAttachment = hasAttachment;
	}

	public String getAttachmentId() {
		return attachmentId;
	}

	public void setAttachmentId(String attachmentId) {
		this.attachmentId = attachmentId;
	}

	public int getcLikes() {
		return cLikes;
	}

	public void setcLikes(int cLikes) {
		this.cLikes = cLikes;
	}

	public int getcFavorites() {
		return cFavorites;
	}

	public void setcFavorites(int cFavorites) {
		this.cFavorites = cFavorites;
	}

	public int getcReplies() {
		return cReplies;
	}

	public void setcReplies(int cReplies) {
		this.cReplies = cReplies;
	}

	public String getReplyOfId() {
		return replyOfId;
	}

	public void setReplyOfId(String replyOfId) {
		this.replyOfId = replyOfId;
	}

	public String getSentiment() {
		return sentiment;
	}

	public void setSentiment(String sentiment) {
		this.sentiment = sentiment;
	}

	public ArrayList<Status> getStatusReplies() {
		return statusReplies;
	}

	public void setStatusReplies(ArrayList<Status> statusReplies) {
		this.statusReplies = statusReplies;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public String getUserOfOperation() {
		return userOfOperation;
	}

	public void setUserOfOperation(String userOfOperation) {
		this.userOfOperation = userOfOperation;
	}

	public long getUserIdOfOperation() {
		return userIdOfOperation;
	}

	public void setUserIdOfOperation(long userIdOfOperation) {
		this.userIdOfOperation = userIdOfOperation;
	}

	public String getStatusText() {
		return statusText;
	}

	public void setStatusText(String statusText) {
		this.statusText = statusText;
	}

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserFullName() {
        return userFullName;
    }

    public void setUserFullName(String userFullName) {
        this.userFullName = userFullName;
    }

    public boolean getIsFavourited() {
        return isFavourited;
    }

    public void setIsFavourited(boolean isFavourited) {
        this.isFavourited = isFavourited;
    }

    public boolean getIsLiked() {
        return isLiked;
    }

    public void setIsLiked(boolean isLiked) {
        this.isLiked = isLiked;
    }

    public String getUserNameOfOperation() {
        return userNameOfOperation;
    }

    public void setUserNameOfOperation(String userNameOfOperation) {
        this.userNameOfOperation = userNameOfOperation;
    }

    public boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public long getUserFbAndroidUId() {
        return userFbAndroidUId;
    }

    public void setUserFbAndroidUId(long userFbAndroidUId) {
        this.userFbAndroidUId = userFbAndroidUId;
    }

    public long getUserFbAndroidUIdOfOperation() {
        return userFbAndroidUIdOfOperation;
    }

    public void setUserFbAndroidUIdOfOperation(long userFbAndroidUIdOfOperation) {
        this.userFbAndroidUIdOfOperation = userFbAndroidUIdOfOperation;
    }

    @Override
    public String toString() {
        return "Status{" +
                "id='" + id + '\'' +
                ", userId=" + userId +
                ", userName='" + userName + '\'' +
                ", userFullName='" + userFullName + '\'' +
                ", statusText='" + statusText + '\'' +
                ", hasAttachment=" + hasAttachment +
                ", attachmentId='" + attachmentId + '\'' +
                ", cLikes=" + cLikes +
                ", cFavorites=" + cFavorites +
                ", cReplies=" + cReplies +
                ", replyOfId='" + replyOfId + '\'' +
                ", sentiment='" + sentiment + '\'' +
                ", operation='" + operation + '\'' +
                ", userOfOperation='" + userOfOperation + '\'' +
                ", userIdOfOperation=" + userIdOfOperation +
                ", statusReplies=" + statusReplies +
                ", deleted=" + isDeleted +
                '}';
    }
}
