package in.stocksphere.stocksphere.data.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import in.stocksphere.stocksphere.data.sqlite.greendao.DaoMaster;
import in.stocksphere.stocksphere.data.sqlite.greendao.DaoSession;

/**
 * Created by vtiwari on 7/11/15.
 */
public class GreenDaoConnectionManager {

    private static final String DB_NAME = "STOCK_SPHERE_DB";

    private static DaoMaster.DevOpenHelper helper;
    private static SQLiteDatabase db;
    private DaoSession daoSession;
    private DaoMaster daoMaster;

    public DaoSession getWritableDaoSession(Context context) {

        daoMaster = new DaoMaster(getWritableDatabase(context));
        daoSession = daoMaster.newSession();

        return daoSession;
    }

    public DaoSession getReadableDaoSession(Context context) {

        daoMaster = new DaoMaster(getReadableDatabase(context));
        daoSession = daoMaster.newSession();

        return daoSession;
    }

    public void clearSession() {

        if (daoSession != null) {
            daoSession.clear();
        }
    }

    private SQLiteDatabase getWritableDatabase(Context context) {

        if (db == null) {
            helper = new DaoMaster.DevOpenHelper(context, DB_NAME, null);
            db = helper.getWritableDatabase();
        }
        return db;
    }

    private SQLiteDatabase getReadableDatabase(Context context) {

        if (db == null) {
            helper = new DaoMaster.DevOpenHelper(context, DB_NAME, null);
            db = helper.getReadableDatabase();
        }
        return db;
    }
}
