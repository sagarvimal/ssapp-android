package in.stocksphere.stocksphere.data.rest;


import com.google.gson.JsonObject;

import java.util.List;

import in.stocksphere.stocksphere.data.models.Company;
import in.stocksphere.stocksphere.data.models.CompanyList;
import in.stocksphere.stocksphere.data.models.CompanySocialDetails;
import in.stocksphere.stocksphere.data.models.CompanyStatus;
import in.stocksphere.stocksphere.data.models.GFGetInfo;
import in.stocksphere.stocksphere.data.models.SSBase;
import in.stocksphere.stocksphere.data.models.Status;
import in.stocksphere.stocksphere.data.models.User;
import in.stocksphere.stocksphere.data.models.UserList;
import in.stocksphere.stocksphere.data.models.UserStatus;
import in.stocksphere.stocksphere.data.models.WatchList;
import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by vtiwari on 6/14/15.
 */
public interface SSAPI {

    @GET(SSAPIResources.PATH_GET_USER_BY_USER_ID)
    void getUserByUserId(@Query("userId") long userId, @Query("callingUserId") long callingUserId, Callback<User> callback);

    @GET(SSAPIResources.PATH_GET_STATUS_BY_ID)
    void getStatusById(@Path("id") String id, @Query("callingUserId") long callingUserId, Callback<Status> callback);

    @POST(SSAPIResources.PATH_GET_USER_FOLLOWERS)
    void getUserFollowers(@Body JsonObject followeeId, Callback<UserList> callback);

    @POST(SSAPIResources.PATH_GET_USER_FOLLOWING)
    void getUserFollowing(@Body JsonObject followerId, Callback<UserList> callback);

    @POST(SSAPIResources.PATH_GET_USER_TIMELINE)
    void getUserTimeline(@Body JsonObject userId, Callback<UserStatus> callback);

    @POST(SSAPIResources.PATH_UPDATE_STATUS)
    void updateStatus(@Body JsonObject statusDetails, Callback<Status> callback);

    @POST(SSAPIResources.PATH_GET_USER_STATUS_UPDATES)
    void getUserStatusUpdates(@Body JsonObject userId, Callback<UserStatus> callback);

    @POST(SSAPIResources.PATH_ADD_USER)
    void addUser(@Body JsonObject user, Callback<User> callback);

    @POST(SSAPIResources.PATH_FOLLOW_USER)
    void followUser(@Body JsonObject userDetails, Callback<SSBase> callback);

    @POST(SSAPIResources.PATH_LIKE_STATUS)
    void likeStatus(@Body JsonObject statusDetails, Callback<SSBase> callback);

    @POST(SSAPIResources.PATH_FAVORITE_STATUS)
    void favoriteStatus(@Body JsonObject statusDetails, Callback<SSBase> callback);

    @POST(SSAPIResources.PATH_UPDATE_WATCHLIST)
    void updateWatchlist(@Body JsonObject details, Callback<SSBase> callback);

    @POST(SSAPIResources.PATH_GET_WATCHLIST)
    void getWatchlist(@Body JsonObject details, Callback<WatchList> callback);

    @POST(SSAPIResources.PATH_FOLLOW_COMPANY)
    void followCompany(@Body JsonObject details, Callback<SSBase> callback);

    @POST(SSAPIResources.PATH_GET_COMPANY_TIMELINE)
    void getCompanyTimeline(@Body JsonObject details, Callback<CompanyStatus> callback);

    @POST(SSAPIResources.PATH_GET_COMPANY_DETAILS)
    void getCompanyDetails(@Body JsonObject details, Callback<Company> callback);

    @POST(SSAPIResources.PATH_DELETE_STATUS)
    void deleteStatus(@Body JsonObject details, Callback<SSBase> callback);

    @POST(SSAPIResources.PATH_UPDATE_USER)
    void updateUser(@Body JsonObject user, Callback<User> callback);

    @POST(SSAPIResources.PATH_GET_ALL_COMPANY_NAMES)
        // post without body
    void getAllCompanyNames(@Body JsonObject details, Callback<CompanyList> callback);

    @GET(SSAPIResources.PATH_GET_COMPANY_QUOTE_FROM_G_F)
    void getStockQuoteGF(@Query("q") String query, Callback<List<GFGetInfo>> callback);

    @GET(SSAPIResources.PATH_GET_USER_BY_USER_NAME)
    void getUserByUserName(@Query("username") String username, Callback<User> callback);

    @POST(SSAPIResources.PATH_GET_COMPANY_SOCIAL_DETAILS)
    void getCompanySocialDetails(@Body JsonObject details, Callback<CompanySocialDetails> callback);

    @POST(SSAPIResources.PATH_GET_WATCHING_USERS)
    void getWatchingUsers(@Body JsonObject details, Callback<UserList> callback);

    @POST(SSAPIResources.PATH_STATUS_LIKED_USERS)
    void getStatusLikedUsers(@Body JsonObject details, Callback<UserList> callback);

    @POST(SSAPIResources.PATH_STATUS_FAVOURITED_USERS)
    void getStatusFavouritedUsers(@Body JsonObject details, Callback<UserList> callback);
}
