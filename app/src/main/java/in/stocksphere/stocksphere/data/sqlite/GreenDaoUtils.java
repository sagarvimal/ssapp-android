package in.stocksphere.stocksphere.data.sqlite;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import hugo.weaving.DebugLog;
import in.stocksphere.stocksphere.data.common.SSConstants;
import in.stocksphere.stocksphere.data.common.SSUtils;
import in.stocksphere.stocksphere.data.models.Company;
import in.stocksphere.stocksphere.data.models.Status;
import in.stocksphere.stocksphere.data.sqlite.greendao.CompanyNames;
import in.stocksphere.stocksphere.data.sqlite.greendao.CompanyNamesDao;
import in.stocksphere.stocksphere.data.sqlite.greendao.CompanyTimeline;
import in.stocksphere.stocksphere.data.sqlite.greendao.CompanyTimelineDao;
import in.stocksphere.stocksphere.data.sqlite.greendao.HomeTimeline;
import in.stocksphere.stocksphere.data.sqlite.greendao.HomeTimelineDao;
import in.stocksphere.stocksphere.data.sqlite.greendao.OpenGraphMetadata;
import in.stocksphere.stocksphere.data.sqlite.greendao.OpenGraphMetadataDao;
import in.stocksphere.stocksphere.data.sqlite.greendao.StockWatchlist;
import in.stocksphere.stocksphere.data.sqlite.greendao.StockWatchlistDao;
import in.stocksphere.stocksphere.data.sqlite.greendao.UserTimeline;
import in.stocksphere.stocksphere.data.sqlite.greendao.UserTimelineDao;
import in.stocksphere.stocksphere.ui.SharedPreferencesManager;

/**
 * Created by vtiwari on 7/8/15.
 */
public class GreenDaoUtils {

    @DebugLog
    public synchronized static void addToHomeTimeline(Context context, List<Status> statusList) {

        if (statusList != null && !statusList.isEmpty()) {

            HomeTimeline homeTimeline;
            GreenDaoConnectionManager gcm = new GreenDaoConnectionManager();
            long thisUserId = SharedPreferencesManager.getCurrentlyLoggedInUserId(context);
            try {
                HomeTimelineDao homeTimelineDao = gcm.getWritableDaoSession(context).getHomeTimelineDao();
                for (Status status : statusList) {

                    homeTimeline = SSModelTransformer.getHomeTimeLine(status, thisUserId);
                    homeTimelineDao.insertOrReplace(homeTimeline);
                }
            } finally {
                gcm.clearSession();
            }
        }
    }

    @DebugLog
    public synchronized static void addToUserTimeline(Context context, List<Status> statusList, long userId) {

        if (statusList != null && !statusList.isEmpty()) {

            UserTimeline userTimeline;
            GreenDaoConnectionManager gcm = new GreenDaoConnectionManager();
            try {
                long thisUserId = SharedPreferencesManager.getCurrentlyLoggedInUserId(context);
                UserTimelineDao userTimelineDao = gcm.getWritableDaoSession(context).getUserTimelineDao();
                for (Status status : statusList) {

                    userTimeline = SSModelTransformer.getUserTimeLine(status, userId, thisUserId);
                    userTimelineDao.insertOrReplace(userTimeline);
                }
            } finally {
                gcm.clearSession();
            }
        }
    }

    @DebugLog
    public synchronized static void addToCompanyTimeline(Context context, List<Status> statusList, String ssSymbol) {

        if (statusList != null && !statusList.isEmpty()) {

            CompanyTimeline companyTimeline;
            GreenDaoConnectionManager gcm = new GreenDaoConnectionManager();
            try {
                long thisUserId = SharedPreferencesManager.getCurrentlyLoggedInUserId(context);
                CompanyTimelineDao companyTimelineDao = gcm.getWritableDaoSession(context).getCompanyTimelineDao();
                for (Status status : statusList) {

                    companyTimeline = SSModelTransformer.getCompanyTimeLine(status, ssSymbol, thisUserId);
                    companyTimelineDao.insertOrReplace(companyTimeline);
                }
            } finally {
                gcm.clearSession();
            }
        }
    }

    @DebugLog
    public static ArrayList<Status> getHomeTimelineStatus(Context context) {

        ArrayList<HomeTimeline> homeTimelines;
        ArrayList<Status> statuses = null;
        Status status;
        GreenDaoConnectionManager gcm = new GreenDaoConnectionManager();
        try {
            long thisUserId = SharedPreferencesManager.getCurrentlyLoggedInUserId(context);
            homeTimelines = (ArrayList<HomeTimeline>) gcm.getReadableDaoSession(context).getHomeTimelineDao()
                    .queryBuilder().where(HomeTimelineDao.Properties.ThisUserId.eq(thisUserId))
                    .orderDesc(HomeTimelineDao.Properties.LastUpdatedDate)
                    .limit(SSConstants.GREEN_DAO_MAX_FETCH_ROW_LIMIT).build().list();

            if (homeTimelines != null && !homeTimelines.isEmpty()) {

                statuses = new ArrayList<Status>();
                for (HomeTimeline homeTimeline : homeTimelines) {

                    if (homeTimeline.getIsDeleted() == null || homeTimeline.getIsDeleted() == false) {
                        status = SSModelTransformer.getStatus(homeTimeline);
                        statuses.add(status);
                    }
                }
            }
        } finally {
            gcm.clearSession();
        }
        return statuses;
    }

    @DebugLog
    public static ArrayList<Status> getUserTimeLineStatus(Context context, long userId) {

        ArrayList<UserTimeline> userTimelines;
        ArrayList<Status> statuses = null;
        Status status;
        GreenDaoConnectionManager gcm = new GreenDaoConnectionManager();
        try {
            long thisUserId = SharedPreferencesManager.getCurrentlyLoggedInUserId(context);
            userTimelines = (ArrayList<UserTimeline>) gcm.getReadableDaoSession(context).getUserTimelineDao()
                    .queryBuilder().where(UserTimelineDao.Properties.UserTimelineUserId.eq(userId))
                    .where(UserTimelineDao.Properties.ThisUserId.eq(thisUserId))
                    .orderDesc(UserTimelineDao.Properties.LastUpdatedDate)
                    .limit(SSConstants.GREEN_DAO_MAX_FETCH_ROW_LIMIT).build().list();

            if (userTimelines != null && !userTimelines.isEmpty()) {

                statuses = new ArrayList<Status>();
                for (UserTimeline userTimeline : userTimelines) {

                    if (userTimeline.getIsDeleted() == null || userTimeline.getIsDeleted() == false) {
                        status = SSModelTransformer.getStatus(userTimeline);
                        statuses.add(status);
                    }
                }
            }
        } finally {
            gcm.clearSession();
        }
        return statuses;
    }

    @DebugLog
    public static ArrayList<Status> getCompanyTimeLineStatus(Context context, String ssSymbol) {

        ArrayList<CompanyTimeline> companyTimelines;
        ArrayList<Status> statuses = null;
        Status status;
        GreenDaoConnectionManager gcm = new GreenDaoConnectionManager();
        try {
            long thisUserId = SharedPreferencesManager.getCurrentlyLoggedInUserId(context);
            companyTimelines = (ArrayList<CompanyTimeline>) gcm.getReadableDaoSession(context).getCompanyTimelineDao()
                    .queryBuilder().where(CompanyTimelineDao.Properties.CompanySsSymbol.eq(ssSymbol))
                    .where(CompanyTimelineDao.Properties.ThisUserId.eq(thisUserId))
                    .orderDesc(CompanyTimelineDao.Properties.LastUpdatedDate).limit(SSConstants.GREEN_DAO_MAX_FETCH_ROW_LIMIT)
                    .build().list();

            if (companyTimelines != null && !companyTimelines.isEmpty()) {

                statuses = new ArrayList<Status>();
                for (CompanyTimeline companyTimeline : companyTimelines) {

                    if (companyTimeline.getIsDeleted() == null || companyTimeline.getIsDeleted() == false) {
                        status = SSModelTransformer.getStatus(companyTimeline);
                        statuses.add(status);
                    }
                }
            }
        } finally {
            gcm.clearSession();
        }
        return statuses;
    }

    @DebugLog
    public static void addStockWatchList(Context context, List<Company> companyList) {

        if (companyList != null && !companyList.isEmpty()) {
            GreenDaoConnectionManager gcm = new GreenDaoConnectionManager();
            StockWatchlist stockWatchlist;
            try {
                long thisUserId = SharedPreferencesManager.getCurrentlyLoggedInUserId(context);
                StockWatchlistDao stockWatchlistDao = gcm.getWritableDaoSession(context).getStockWatchlistDao();

                for (Company company : companyList) {

                    if (company.getNseQuote() != null) {

                        stockWatchlist = SSModelTransformer.getStockWatchlist(company, thisUserId);
                        stockWatchlistDao.insertOrReplace(stockWatchlist);
                    }
                }
            } finally {
                gcm.clearSession();
            }
        }
    }

    @DebugLog
    public static ArrayList<Company> getStockWatchlist(Context context) {

        ArrayList<StockWatchlist> stockWatchlists;
        ArrayList<Company> companies = null;
        Company company;
        GreenDaoConnectionManager gcm = new GreenDaoConnectionManager();
        try {
            long thisUserId = SharedPreferencesManager.getCurrentlyLoggedInUserId(context);
            stockWatchlists = (ArrayList<StockWatchlist>) gcm.getReadableDaoSession(context).getStockWatchlistDao()
                    .queryBuilder()
                    .where(StockWatchlistDao.Properties.ThisUserId.eq(thisUserId))
                    .orderAsc(StockWatchlistDao.Properties.Name).build().list();

            if (stockWatchlists != null && !stockWatchlists.isEmpty()) {

                companies = new ArrayList<Company>();
                for (StockWatchlist stockWatchlist : stockWatchlists) {

                    company = SSModelTransformer.getCompanyForWatchlist(stockWatchlist);
                    companies.add(company);
                }
            }
        } finally {
            gcm.clearSession();
        }
        return companies;
    }

    @DebugLog
    public static void removeStockFromWatchList(Context context, String ssSymbol) {

        if (ssSymbol != null && !ssSymbol.isEmpty()) {
            GreenDaoConnectionManager gcm = new GreenDaoConnectionManager();
            try {
                long thisUserId = SharedPreferencesManager.getCurrentlyLoggedInUserId(context);
                StockWatchlistDao stockWatchlistDao = gcm.getWritableDaoSession(context).getStockWatchlistDao();
                StockWatchlist watchlist = new StockWatchlist();
                watchlist.setPrimaryKey(thisUserId + SSConstants.HYPHEN + ssSymbol);
                stockWatchlistDao.delete(watchlist);

            } finally {
                gcm.clearSession();
            }
        }
    }

    @DebugLog
    public static void updateWatchlist(Context context, String ssSymbol, boolean isPushNotifEnabled, double minLimit, double maxLimit) {

        if (ssSymbol != null && !ssSymbol.isEmpty()) {
            GreenDaoConnectionManager gcm = new GreenDaoConnectionManager();
            try {
                long thisUserId = SharedPreferencesManager.getCurrentlyLoggedInUserId(context);
                StockWatchlistDao stockWatchlistDao = gcm.getWritableDaoSession(context).getStockWatchlistDao();
                StockWatchlist watchlist = stockWatchlistDao.queryBuilder()
                        .where(StockWatchlistDao.Properties.PrimaryKey.eq(thisUserId + SSConstants.HYPHEN + ssSymbol)).build().unique();
                if (watchlist == null) {//if not added to watchlist then add it
                    watchlist = new StockWatchlist();
                    watchlist.setPrimaryKey(thisUserId + SSConstants.HYPHEN + ssSymbol);
                }
                watchlist.setIsPushNotifEnabled(isPushNotifEnabled);
                watchlist.setMinPriceNotif(minLimit);
                watchlist.setMaxPriceNotif(maxLimit);

                stockWatchlistDao.insertOrReplace(watchlist);
            } finally {
                gcm.clearSession();
            }
        }
    }

    @DebugLog
    public static Company getCompanyFromWatchList(Context context, String ssSymbol) {

        Company company = null;
        if (ssSymbol != null && !ssSymbol.isEmpty()) {
            GreenDaoConnectionManager gcm = new GreenDaoConnectionManager();
            try {
                long thisUserId = SharedPreferencesManager.getCurrentlyLoggedInUserId(context);
                StockWatchlistDao stockWatchlistDao = gcm.getReadableDaoSession(context).getStockWatchlistDao();
                StockWatchlist watchlist = stockWatchlistDao.queryBuilder()
                        .where(StockWatchlistDao.Properties.PrimaryKey.eq(thisUserId + SSConstants.HYPHEN + ssSymbol)).build().unique();
                if (watchlist != null) {

                    company = SSModelTransformer.getCompanyForWatchlist(watchlist);
                }

            } finally {
                gcm.clearSession();
            }
        }

        return company;
    }

    @DebugLog
    public static void addToCompanyNames(Context context, List<Company> companyList) {

        if (companyList != null && !companyList.isEmpty()) {

            CompanyNames companyNames;
            GreenDaoConnectionManager gcm = new GreenDaoConnectionManager();
            try {
                CompanyNamesDao companyNamesDao = gcm.getWritableDaoSession(context).getCompanyNamesDao();
                for (Company company : companyList) {

                    companyNames = new CompanyNames();
                    companyNames.setSsSymbol(company.getSsSymbol());

                    if (company.getName() != null) {
                        companyNames.setName(company.getName());
                        companyNames.setSsSymbolName(company.getSsSymbol() + SSConstants.AUTO_COMPLETE_SEPARATOR + company.getName());
                    } else {
                        companyNames.setName(company.getName());
                        companyNames.setSsSymbolName(company.getSsSymbol() + SSConstants.AUTO_COMPLETE_SEPARATOR + company.getSsSymbol());
                    }

                    companyNamesDao.insertOrReplace(companyNames);
                }
            } finally {
                gcm.clearSession();
            }
        }
    }

    @DebugLog
    public static boolean isCompanyNamesPresent(Context context) {

        boolean isCompanyNamesPresent = false;
        GreenDaoConnectionManager gcm = new GreenDaoConnectionManager();
        try {
            CompanyNamesDao companyNamesDao = gcm.getReadableDaoSession(context).getCompanyNamesDao();
            long rowCount = companyNamesDao.count();

            if (rowCount > 0) {
                isCompanyNamesPresent = true;
            }

        } finally {
            gcm.clearSession();
        }

        return isCompanyNamesPresent;
    }

    @DebugLog
    public static String[] getSSSymbolNamesArray(Context context) {

        GreenDaoConnectionManager gcm = new GreenDaoConnectionManager();
        String[] ssSymbolNamesArray = null;
        CompanyNames companyNames;
        try {
            CompanyNamesDao companyNamesDao = gcm.getReadableDaoSession(context).getCompanyNamesDao();
            ArrayList<CompanyNames> companyNamesList = (ArrayList<CompanyNames>) companyNamesDao.loadAll();

            if (companyNamesList != null && !companyNamesList.isEmpty()) {

                ssSymbolNamesArray = new String[companyNamesList.size()];
                for (int i = 0; i < companyNamesList.size(); i++) {

                    companyNames = companyNamesList.get(i);
                    ssSymbolNamesArray[i] = companyNames.getSsSymbolName();
                }
            } else {
                ssSymbolNamesArray = new String[4]; //dummy data
                ssSymbolNamesArray[0] = "$INFY" + SSConstants.AUTO_COMPLETE_SEPARATOR + "INFOSYS";
                ssSymbolNamesArray[1] = "$TCS" + SSConstants.AUTO_COMPLETE_SEPARATOR + "Tata Consultancy Services Limited";
                ssSymbolNamesArray[2] = "$SBIN" + SSConstants.AUTO_COMPLETE_SEPARATOR + "State Bank Of India";
                ssSymbolNamesArray[3] = "$COALINDIA" + SSConstants.AUTO_COMPLETE_SEPARATOR + "Coal India";
            }
        } finally {
            gcm.clearSession();
        }

        return ssSymbolNamesArray;
    }

    @DebugLog
    public static void deleteStatus(Context context, String statusId, String statusText) {

        GreenDaoConnectionManager gcm = new GreenDaoConnectionManager();
        try {
            long thisUserId = SharedPreferencesManager.getCurrentlyLoggedInUserId(context);
            //delete from home time line
            HomeTimelineDao timelineDao = gcm.getWritableDaoSession(context).getHomeTimelineDao();
            HomeTimeline homeTimeline = new HomeTimeline();
            homeTimeline.setPrimaryKey(thisUserId + SSConstants.HYPHEN + statusId);
            timelineDao.delete(homeTimeline);

            //delete from user timeline
            UserTimelineDao userTimelineDao = gcm.getWritableDaoSession(context).getUserTimelineDao();
            UserTimeline userTimeline = new UserTimeline();
            userTimeline.setPrimaryKey(thisUserId + SSConstants.HYPHEN + statusId + SSConstants.HYPHEN + thisUserId);
            userTimelineDao.delete(userTimeline);

            //delete from companyTimeline
            ArrayList<String> ssSymbolList = SSUtils.getSSSymbolList(statusText);
            if (ssSymbolList != null && !ssSymbolList.isEmpty()) {
                CompanyTimelineDao companyTimelineDao = gcm.getWritableDaoSession(context).getCompanyTimelineDao();
                CompanyTimeline companyTimeline = null;

                for (String ssSymbol : ssSymbolList) {
                    companyTimeline = new CompanyTimeline();
                    companyTimeline.setPrimaryKey(thisUserId + SSConstants.HYPHEN + statusId + SSConstants.HYPHEN + ssSymbol);
                    companyTimelineDao.delete(companyTimeline);
                }
            }

        } finally {
            gcm.clearSession();
        }
    }

    @DebugLog
    public static String[] getSSSymbolsArray(Context context) {

        GreenDaoConnectionManager gcm = new GreenDaoConnectionManager();
        String[] ssSymbolsArray = null;
        CompanyNames companyNames;
        try {
            CompanyNamesDao companyNamesDao = gcm.getReadableDaoSession(context).getCompanyNamesDao();
            ArrayList<CompanyNames> companyNamesList = (ArrayList<CompanyNames>) companyNamesDao.loadAll();

            if (companyNamesList != null && !companyNamesList.isEmpty()) {

                ssSymbolsArray = new String[companyNamesList.size()];
                for (int i = 0; i < companyNamesList.size(); i++) {

                    companyNames = companyNamesList.get(i);
                    ssSymbolsArray[i] = companyNames.getSsSymbol();
                }
            } else {
                ssSymbolsArray = new String[4]; //dummy data
                ssSymbolsArray[0] = "$INFY";
                ssSymbolsArray[1] = "$TCS";
                ssSymbolsArray[2] = "$SBIN";
                ssSymbolsArray[3] = "$COALINDIA";
            }
        } finally {
            gcm.clearSession();
        }

        return ssSymbolsArray;
    }

    @DebugLog
    public static void updateStatus(Context context, Status status) {

        GreenDaoConnectionManager gcm = new GreenDaoConnectionManager();
        try {
            long thisUserId = SharedPreferencesManager.getCurrentlyLoggedInUserId(context);
            //update status to home time line if status is added to home timeline
            HomeTimelineDao timelineDao = gcm.getWritableDaoSession(context).getHomeTimelineDao();
            HomeTimeline homeTimeline = timelineDao.queryBuilder()
                    .where(HomeTimelineDao.Properties.StatusId.eq(status.getId()))
                    .where(HomeTimelineDao.Properties.ThisUserId.eq(thisUserId)).build().unique();
            if (homeTimeline != null) {
                homeTimeline = SSModelTransformer.getHomeTimeLine(status, thisUserId);
                timelineDao.insertOrReplace(homeTimeline);
            }

            //update status to user timeline if status is added to user timeline
            UserTimelineDao userTimelineDao = gcm.getWritableDaoSession(context).getUserTimelineDao();
            UserTimeline userTimeline = userTimelineDao.queryBuilder()
                    .where(UserTimelineDao.Properties.StatusId.eq(status.getId()))
                    .where(UserTimelineDao.Properties.ThisUserId.eq(thisUserId)).build().unique();
            if (userTimeline != null) {
                userTimeline = SSModelTransformer.getUserTimeLine(status, userTimeline.getUserId(), thisUserId);
                userTimelineDao.insertOrReplace(userTimeline);
            }

            //update status to companyTimeline
            ArrayList<String> ssSymbolList = SSUtils.getSSSymbolList(status.getStatusText());
            if (ssSymbolList != null && !ssSymbolList.isEmpty()) {
                CompanyTimelineDao companyTimelineDao = gcm.getWritableDaoSession(context).getCompanyTimelineDao();
                CompanyTimeline companyTimeline;

                for (String ssSymbol : ssSymbolList) {
                    companyTimeline = SSModelTransformer.getCompanyTimeLine(status, ssSymbol, thisUserId);
                    companyTimelineDao.insertOrReplace(companyTimeline);
                }
            }

        } finally {
            gcm.clearSession();
        }
    }

    public static void updateOpenGraphMetadata(Context context, in.stocksphere.stocksphere.data.jsoup.OpenGraphMetadata openGraphMetadata) {

        //openGraphMetadata is for generic url thats why currenly logged in user is not stored
        GreenDaoConnectionManager gcm = new GreenDaoConnectionManager();
        try {
            OpenGraphMetadataDao openGraphMetadataDao = gcm.getWritableDaoSession(context).getOpenGraphMetadataDao();
            OpenGraphMetadata metadata = new OpenGraphMetadata();
            metadata.setUrl(openGraphMetadata.getWebPageUrl());
            metadata.setTitle(openGraphMetadata.getTitle());
            metadata.setHostUrl(openGraphMetadata.getUrl());
            metadata.setImageUrl(openGraphMetadata.getImageURL());

            openGraphMetadataDao.insertOrReplace(metadata);

        } finally {
            gcm.clearSession();
        }
    }

    @DebugLog
    public static in.stocksphere.stocksphere.data.jsoup.OpenGraphMetadata getOpenGraphMetadata(Context context, String webPageUrl) {

        //openGraphMetadata is for generic url thats why currenly logged in user is not stored
        in.stocksphere.stocksphere.data.jsoup.OpenGraphMetadata openGraphMetadata = null;
        if (webPageUrl != null && !webPageUrl.isEmpty()) {
            GreenDaoConnectionManager gcm = new GreenDaoConnectionManager();
            try {
                OpenGraphMetadataDao openGraphMetadataDao = gcm.getReadableDaoSession(context).getOpenGraphMetadataDao();
                OpenGraphMetadata metadata = openGraphMetadataDao.queryBuilder()
                        .where(OpenGraphMetadataDao.Properties.Url.eq(webPageUrl)).build().unique();
                if (metadata != null) {

                    openGraphMetadata = new in.stocksphere.stocksphere.data.jsoup.OpenGraphMetadata();
                    openGraphMetadata.setWebPageUrl(webPageUrl);
                    openGraphMetadata.setUrl(metadata.getHostUrl());
                    openGraphMetadata.setImageURL(metadata.getImageUrl());
                    openGraphMetadata.setTitle(metadata.getTitle());
                }

            } finally {
                gcm.clearSession();
            }
        }

        return openGraphMetadata;
    }
}
