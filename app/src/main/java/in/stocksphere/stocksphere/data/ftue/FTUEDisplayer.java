package in.stocksphere.stocksphere.data.ftue;

import android.app.Activity;
import android.view.View;

import in.stocksphere.stocksphere.R;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseSequence;
import uk.co.deanwild.materialshowcaseview.ShowcaseConfig;

/**
 * Created by vtiwari on 9/20/15.
 * <p/>
 * https://github.com/deano2390/MaterialShowcaseView
 */
public class FTUEDisplayer {

    public static final int DELAY_MS = 500;
    private static final String SHOWCASE_ID_WATCHLIST = "1.1";
    private static final String SHOWCASE_ID_STATUS_UPDATE = "2.1";
    private static final String SHOWCASE_ID_COMPANY = "3.1";
    private static final String SHOWCASE_ID_MY_PAGE = "4.1";
    private static final String SHOWCASE_ID_USER_PAGE = "5.1";

    public static void showFTUEOnWatchlist(Activity activity) {

        if (activity == null || activity.findViewById(R.id.toolbar_search) == null
                || activity.findViewById(R.id.toolbar_post) == null) {
            return;
        }

        ShowcaseConfig config = new ShowcaseConfig();
        config.setDelay(DELAY_MS);
        MaterialShowcaseSequence sequence = new MaterialShowcaseSequence(activity, SHOWCASE_ID_WATCHLIST);
        sequence.setConfig(config);
        sequence.addSequenceItem(activity.findViewById(R.id.toolbar_search),
                "Search companies to add in your watchlist", "GOT IT");
        sequence.addSequenceItem(activity.findViewById(R.id.toolbar_post),
                "Post your opinion about stocks", "GOT IT");
        sequence.start();
    }

    public static void showFTUEOnUpdateStatus(Activity activity, View bullishBtn, View bearingBtn, View postBtn, View dollarSymbol) {

        ShowcaseConfig config = new ShowcaseConfig();
        config.setDelay(DELAY_MS);
        MaterialShowcaseSequence sequence = new MaterialShowcaseSequence(activity, SHOWCASE_ID_STATUS_UPDATE);
        sequence.setConfig(config);
        sequence.addSequenceItem(dollarSymbol,
                "Use it to enter $ symbol", "GOT IT");
        sequence.addSequenceItem(bullishBtn,
                "Select this if you feel bullish/ positive about market", "GOT IT");
        sequence.addSequenceItem(bearingBtn,
                "Select this if you feel bearish/ negative about market", "GOT IT");
        sequence.addSequenceItem(postBtn,
                "Post your update", "GOT IT");
        sequence.start();
    }

    public static void showFTUEOnCompanyScreen(Activity activity, View addToWatchlist, View watchingUsers) {

        ShowcaseConfig config = new ShowcaseConfig();
        config.setDelay(DELAY_MS);
        MaterialShowcaseSequence sequence = new MaterialShowcaseSequence(activity, SHOWCASE_ID_COMPANY);
        sequence.setConfig(config);
        sequence.addSequenceItem(addToWatchlist,
                "Add to watchlist and set price change alert", "GOT IT");
        sequence.addSequenceItem(watchingUsers,
                "See the list of users who are watching this company", "GOT IT");
        sequence.start();
    }

    public static void showFTUEOnMyPage(Activity activity, View updateProfile) {

        ShowcaseConfig config = new ShowcaseConfig();
        config.setDelay(DELAY_MS);
        MaterialShowcaseSequence sequence = new MaterialShowcaseSequence(activity, SHOWCASE_ID_MY_PAGE);
        sequence.setConfig(config);
        sequence.addSequenceItem(updateProfile,
                "Update your profile", "GOT IT");
        sequence.start();
    }

    public static void showFTUEOnUserPage(Activity activity, View followUser) {

        ShowcaseConfig config = new ShowcaseConfig();
        config.setDelay(DELAY_MS);
        MaterialShowcaseSequence sequence = new MaterialShowcaseSequence(activity, SHOWCASE_ID_USER_PAGE);
        sequence.setConfig(config);
        sequence.addSequenceItem(followUser,
                "Select this to follow", "GOT IT");
        sequence.start();
    }
}
