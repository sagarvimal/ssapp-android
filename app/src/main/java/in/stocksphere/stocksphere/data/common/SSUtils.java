package in.stocksphere.stocksphere.data.common;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.Snackbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import org.joda.time.Interval;
import org.joda.time.Period;

import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import hugo.weaving.DebugLog;
import in.stocksphere.stocksphere.Logger;
import in.stocksphere.stocksphere.data.fabric.FabricUtils;
import in.stocksphere.stocksphere.data.models.Company;
import in.stocksphere.stocksphere.data.models.GFGetInfo;
import in.stocksphere.stocksphere.data.models.Quote;
import in.stocksphere.stocksphere.data.models.TradingApp;
import in.stocksphere.stocksphere.ui.SharedPreferencesManager;
import in.stocksphere.stocksphere.ui.custom.IntentConstants;

/**
 * Created by vtiwari on 6/16/15.
 */
public class SSUtils {

    private static List<Date> holidayList = null;

    public static String getPresentableDate(long milliSeconds) {
        Date date = new Date(milliSeconds);

        return new SimpleDateFormat(SSConstants.PRESENTABLE_DATE_FORMAT).format(date);
    }

    public static String getTimeElapsed(long milliSeconds) {
        String timeElapsed = SSConstants.EMPTY_STRING;

        Date startDate = new Date(milliSeconds);
        Date endDate = new Date();

        Interval interval =
                new Interval(startDate.getTime(), endDate.getTime());
        Period period = interval.toPeriod();

        if (period.getYears() > 0) {
            timeElapsed = Integer.toString(period.getYears()) + "y";
        } else if (period.getMonths() > 0) {
            timeElapsed = Integer.toString(period.getMonths()) + "M";
        } else if (period.getWeeks() > 0) {
            timeElapsed = Integer.toString(period.getWeeks()) + "w";
        } else if (period.getDays() > 0) {
            timeElapsed = Integer.toString(period.getDays()) + "d";
        } else if (period.getHours() > 0) {
            timeElapsed = Integer.toString(period.getHours()) + "h";
        } else if (period.getMinutes() > 0) {
            timeElapsed = Integer.toString(period.getMinutes()) + "m";
        } else if (period.getSeconds() > 0) {
            timeElapsed = Integer.toString(period.getSeconds()) + "s";
        }

        return timeElapsed;
    }

    /*
     validates indian phone number
     Pattern: +91-xxxxxxxxxx
    */
    public static boolean isValidPhoneNumber(String phone) {

        if (phone != null && !phone.isEmpty()) {
            return phone.matches(SSConstants.REG_EX_PHONE);
        }

        return false;
    }

    public static boolean isValidEmail(String email) {

        if (email != null && !email.isEmpty()) {
            return email.matches(SSConstants.REG_EX_EMAIL);
        }

        return false;
    }

    public static boolean isNetworkAvailable(Context context) {

        ConnectivityManager cm = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();

        // if no network is available networkInfo will be null
        // otherwise check if we are connected
        return networkInfo != null && networkInfo.isConnected();
    }

    public static void displayNoNetworkMsg(Context context) {
        Toast.makeText(context, "Unable to connect right now. Check your network settings.", Toast.LENGTH_SHORT).show();
    }

    private static List<Date> getAllNSEHolidays() {

        if(holidayList != null && !holidayList.isEmpty()){
            return holidayList;
        }

        try {
            String holidays = "26-Jan-2016,19-Feb-2016,07-Mar-2016,24-Mar-2016,25-Mar-2016,01-Apr-2016,08-Apr-2016," +
                    "14-Apr-2016,15-Apr-2016,19-Apr-2016,06-Jul-2016,15-Aug-2016,17-Aug-2016,05-Sep-2016,13-Sep-2016," +
                    "11-Oct-2016,12-Oct-2016,31-Oct-2016,14-Nov-2016,12-Dec-2016";

            String[] holidayArray = holidays.split(SSConstants.COMMA);

            holidayList = new ArrayList<Date>();

            for (String dateStr : holidayArray) {
                holidayList.add(new SimpleDateFormat(SSConstants.NSE_DATE_FORMAT).parse(dateStr));
            }
        } catch (Exception e) {
            System.out.println("Exception in getAllNationalHolidays");
            e.printStackTrace();
        }
        return holidayList;
    }

    /**
     * Returns true if the given date is a business day
     *
     * @return
     */
    public static boolean isBusinessHour() {
        // get all the holidays as java.util.Date
        List<Date> holidays = getAllNSEHolidays();

        boolean isBusinessDay = false;

        // get the current date without the hours, minutes, seconds and millis
        Calendar cal = getISTCalender();

        // if the time is not between 9:17am to 3:45pm, return false
        if (cal.get(Calendar.HOUR_OF_DAY) < 9 || cal.get(Calendar.HOUR_OF_DAY) > 16
                || (cal.get(Calendar.HOUR_OF_DAY) == 9 && cal.get(Calendar.MINUTE) < 17)
                || (cal.get(Calendar.HOUR_OF_DAY) == 15 && cal.get(Calendar.MINUTE) > 35)) {

            return isBusinessDay;
        }

        //set all time parameters to zero to compare only date from holiday list
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);

        if (dayOfWeek != Calendar.SATURDAY && dayOfWeek != Calendar.SUNDAY
                && !holidays.contains(cal.getTime())) {
            isBusinessDay = true;
        }
        return isBusinessDay;
    }

    /*
    Generates hash keys - used for facebook login
     */
    @DebugLog
    public static void showHashKey(Context context) {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(
                    context.getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

            Logger.log(e.getMessage());

        } catch (NoSuchAlgorithmException e) {

            Logger.log(e.getMessage());
        }
    }

    @DebugLog
    public static String getFacebookProfilePicUrl(long appSpecificUserId) {

        return "https://graph.facebook.com/" + appSpecificUserId + "/picture?type=large";
    }

    public static void hideKeyboard(Activity activity) {
        try {
            // Check if no view has focus:
            View view = activity.getCurrentFocus();
            if (view != null) {
                InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
        } catch (Exception e) {
            FabricUtils.logException(e);

        }
    }

    @DebugLog
    public static String getGFCNameQueryStr(List<Company> companies) {

        String q = null;

        if (companies != null && !companies.isEmpty()) {

            q = SSConstants.NSE_STRING + SSConstants.COLON;
            for (Company company : companies) {
                q = q + company.getNseSymbol();
                q = q + SSConstants.COMMA;
            }

            // remove last comma
            if (q.length() > 0 && String.valueOf(q.charAt(q.length() - 1)).equals(SSConstants.COMMA)) {
                q = q.substring(0, q.length() - 1);
            }

            //replace ampersand
            if (q.contains(SSConstants.AMPERSAND)) {
                q = q.replaceAll(SSConstants.AMPERSAND, SSConstants.AMPERSAND_URL_CODE);
            }
        }
        return q;
    }

    public static Double getDouble(String str) {

        try {

            if (str != null && !str.trim().isEmpty()) {
                str = str.replace(SSConstants.COMMA, SSConstants.EMPTY_STRING);
                return Double.parseDouble(str);
            }

        } catch (Exception e) {
            Logger.log("GET_DOUBLE_EXCEPTION "+e.getMessage());
            FabricUtils.logException(e);
            FabricUtils.logError("GET_DOUBLE_EXCEPTION", str);
        }

        return 0.0;
    }

    @DebugLog
    public static boolean isFetchDataFromGF() {

        return SSConstants.IS_GET_DATA_FROM_GOOGLE_FINANCE && isBusinessHour();
    }

    @DebugLog
    public static void populateCompanyDetailsFromGF(Company company, GFGetInfo gfGetInfo) {
        Quote quote;
        Date lastUpdatedDate;

        if (gfGetInfo.getT().trim().equals(company.getNseSymbol())) {

            quote = company.getNseQuote();
            quote.setLastMarketPrice(SSUtils.getDouble(gfGetInfo.getL()));
            quote.setChangePrice(SSUtils.getDouble(gfGetInfo.getC()));
            quote.setPercentageChange(SSUtils.getDouble(gfGetInfo.getCp()));

            try {
                lastUpdatedDate = new SimpleDateFormat(SSConstants.DATE_FORMAT_G_FINANCE)
                        .parse(getISTCalender().get(Calendar.YEAR) + SSConstants.SPACE + gfGetInfo.getLt());
                quote.setLastPriceUpdateDate(lastUpdatedDate.getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (quote.getHighPrice() < SSUtils.getDouble(gfGetInfo.getL())) {
                quote.setHighPrice(SSUtils.getDouble(gfGetInfo.getL()));
            }

            if (quote.getLowPrice() > SSUtils.getDouble(gfGetInfo.getL())) {
                quote.setLowPrice(SSUtils.getDouble(gfGetInfo.getL()));
            }
        }
    }

    /*
     * This will return list of all companies mentioned in status text
	 */
    public static ArrayList<String> getSSSymbolList(String statusText) {

        ArrayList<String> companyList = null;
        try {
            if (statusText != null && !statusText.isEmpty()) {
                companyList = new ArrayList<String>();

                //this regular expression checks all the words starting with $ symbol
                Matcher matcher = Pattern.compile(SSConstants.REG_EX_START_WITH_$).matcher(statusText);

                while (matcher.find()) {
                    companyList.add(matcher.group());
                }
            }
        } catch (Exception e) {
            FabricUtils.logException(e);
        }
        return companyList;
    }

    /*
     * This will return list of all users mentioned in status text
	 */
    public static ArrayList<String> getUserNameList(String statusText) {

        ArrayList<String> userNameList = null;
        try {
            if (statusText != null && !statusText.isEmpty()) {
                userNameList = new ArrayList<String>();

                //this regular expression checks all the words starting with @ symbol
                Matcher matcher = Pattern.compile(SSConstants.REG_EX_START_WITH_AT_THE_RATE).matcher(statusText);

                while (matcher.find()) {
                    userNameList.add(matcher.group());
                }
            }
        } catch (Exception e) {
            FabricUtils.logException(e);
        }
        return userNameList;
    }

    public static void showSnackBarMsg(Activity activity, String message) {

        final Snackbar snackBar = Snackbar.make(activity.findViewById(android.R.id.content),
                message, Snackbar.LENGTH_LONG);

        View view = snackBar.getView();
        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.WHITE);

        snackBar.setAction("Dismiss", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackBar.dismiss();
            }
        }).setActionTextColor(Color.WHITE).show();
    }

    public static void SendShowSnackBarMsgBroadcast(Context context, String message) {
        Intent intent = new Intent();
        intent.setAction(IntentConstants.INTENT_ACTION_SHOW_SNACK_BAR);
        intent.putExtra(IntentConstants.SNACK_BAR_MESSAGE, message);
        context.sendBroadcast(intent);
    }

    public static boolean isSenIndexPriceNotif(Context context) {

        boolean isSenIndexPriceNotif = false;
        Calendar cal = getISTCalender();

        if (isBusinessHour()) {

            if ((cal.get(Calendar.HOUR_OF_DAY) == 9
                    && cal.get(Calendar.MINUTE) > 25
                    && cal.get(Calendar.MINUTE) <= 35)
                    || (cal.get(Calendar.HOUR_OF_DAY) == 12
                    && cal.get(Calendar.MINUTE) > 1
                    && cal.get(Calendar.MINUTE) <= 10)
                    || (cal.get(Calendar.HOUR_OF_DAY) == 15
                    && cal.get(Calendar.MINUTE) > 1
                    && cal.get(Calendar.MINUTE) <= 10)) {

                isSenIndexPriceNotif = true;
            }
        }

        //Check if last index price notif time
        if (isSenIndexPriceNotif) {
            long lastNotifTime = SharedPreferencesManager.getLastIndexNotifTime(context);
            long timeDiff = TimeUnit.MILLISECONDS.toMinutes(cal.getTime().getTime() - lastNotifTime);
            if (timeDiff < 30) {
                isSenIndexPriceNotif = false;
            }
        }

        return isSenIndexPriceNotif;
    }

    public static String getHost(String url) {

        String host = SSConstants.EMPTY_STRING;

        try {
            host = new URL(url).getHost();
        } catch (MalformedURLException e) {
            e.printStackTrace();
            FabricUtils.logException(e);
        }

        return host;
    }

    public static boolean isAppInstalled(Context context, String appPackageName) {

        boolean isAppInstalled;
        try {
            context.getPackageManager().getApplicationInfo(appPackageName, 0);
            isAppInstalled = true;
        } catch (PackageManager.NameNotFoundException e) {
            isAppInstalled = false;
        }
        return isAppInstalled;
    }

    public static TradingApp getApplicationInfo(Context context, String appPackageName) {

        try {
            TradingApp tradingApp = new TradingApp();
            tradingApp.setAppIcon(context.getPackageManager().getApplicationIcon(appPackageName));
            tradingApp.setAppName(context.getPackageManager().getApplicationInfo(appPackageName, 0)
                    .loadLabel(context.getPackageManager()).toString());
            tradingApp.setAppPackageName(appPackageName);
            return tradingApp;
        } catch (PackageManager.NameNotFoundException e) {
            FabricUtils.logException(e);
            e.printStackTrace();
            return null;
        }

    }

    public static List<TradingApp> getInstalledTradingApps(Context context) {
        List<TradingApp> appsList = new ArrayList<>();
        for (int i = 0; i < TradingAppConstants.TRADING_APPS_LIST.size(); i++) {
            if (isAppInstalled(context, TradingAppConstants.TRADING_APPS_LIST.get(i))) {
                TradingApp tradingApp = getApplicationInfo(context, TradingAppConstants.TRADING_APPS_LIST.get(i));
                if (tradingApp != null) {
                    appsList.add(tradingApp);
                }
            }
        }
        return appsList;
    }

    public static void launchApp(Context context, String appPackageName) {

        if (isAppInstalled(context, appPackageName)) {
            Intent launchIntent = context.getPackageManager().getLaunchIntentForPackage(appPackageName);
            context.startActivity(launchIntent);
        }
    }

    public static Calendar getISTCalender() {
        Calendar cal = new GregorianCalendar(TimeZone.getTimeZone(SSConstants.IST_TIME_ZONE_ID));
        cal.setTime(new Date());
        return cal;
    }
}
