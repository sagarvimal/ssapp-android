package in.stocksphere.stocksphere.data.loaders;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import java.util.Iterator;
import java.util.List;

import hugo.weaving.DebugLog;
import in.stocksphere.stocksphere.data.common.SSConstants;
import in.stocksphere.stocksphere.data.models.Status;
import in.stocksphere.stocksphere.data.sqlite.GreenDaoUtils;

/**
 * Created by vtiwari on 7/26/15.
 */
public class StatusLoader extends AsyncTaskLoader<List<Status>> {

    private Context context;
    private int statusType;

    public static final int TIMELINE_STATUS = 1;
    public static final int NEWS_INFO = 2;

    public StatusLoader(Context context, int statusType) {
        super(context);
        this.context = context;
        this.statusType = statusType;
    }

    @DebugLog
    @Override public List<Status> loadInBackground() {

        if(statusType == TIMELINE_STATUS) {
            return GreenDaoUtils.getHomeTimelineStatus(context);
        }
        else if(statusType == NEWS_INFO) {
            List<Status> statusList = GreenDaoUtils.getUserTimeLineStatus(context, SSConstants.STOCK_SPHERE_USER_ID);

            //Remove updates which contains "@" --> they might be replies or conversations
            if(statusList != null && !statusList.isEmpty()){
                Iterator itr = statusList.iterator();
                while(itr.hasNext()) {
                    Status status = (Status)itr.next();
                    if(status.getStatusText() != null && status.getStatusText().contains(SSConstants.AT_THE_RATE)){
                        itr.remove();
                    }
                }
            }

            return statusList;
        }

        return null; // default
    }
}
