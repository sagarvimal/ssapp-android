package in.stocksphere.stocksphere.data.ganalytics;

/**
 * Created by vtiwari on 8/23/15.
 */
public class GAnalyticsConstants {

    public static final String SWIPE_REFRESH = "SWIPE REFRESH";

    public static final String STATUS_UPDATE = "STATUS UPDATE";

    public static final String LIKE_STATUS = "LIKE STATUS";

    public static final String FAVOURITE_STATUS = "FAVOURITE STATUS";

    public static final String REPLY_STATUS = "REPLY STATUS";

    public static final String DELETE_STATUS = "DELETE STATUS";

    public static final String CLICK_COMPANY_ROW = "CLICK COMPANY ROW";

    public static final String CLICK_STATUS_ROW = "CLICK STATUS ROW";

    public static final String LOGIN_SUCCESS = "LOGIN SUCCESS";

    public static final String LOGIN_FAILED = "LOGIN FAILED";

    public static final String SEARCH_COMPANY = "SEARCH COMPANY";

    public static final String CLICK_BULLISH_ICON = "BULLISH ICON CLICK";

    public static final String CLICK_BEARISH_ICON = "BEARISH ICON CLICK";

    public static final String CANCEL = "CANCEL";

    public static final String PROFILE_UPDATE = "UPDATE PROFILE";

    public static final String OK = "OK";

    public static final String LOGOUT = "LOGOUT";

    public static final String PUSH_NOTIFICATION = "PUSH NOTIFICATION";

    public static final String DISPLAY_PIC_CLICK = "DISPLAY PIC CLICK";

    public static final String PUSH_NOTIFICATION_WATCHLIST = "WATCHLIST";

    public static final String PUSH_NOTIFICATION_PARSE = "PARSE";

    public static final String PUSH_NOTIFICATION_INDEX_PRICES = "INDEX PRICES";

    public static final String PUSH_NOTIFICATION_TRENDING_COMPANY = "TRENDING_COMPANY";

    public static final String SHOW_TRADING_APPS = "SHOW TRADING APPS";

    public static final String LAUNCH_TRADING_APP = "LAUNCH TRADING APP";

    public static final String TIMELINE = "TIMELINE";

    public static final String NEWS_INFO = "NEWS INFO";

    public static final String WATCHLIST = "WATCHLIST";

    public static final String SCREEN_VIEW = "SCREEN";
}
