package in.stocksphere.stocksphere.data.jsoup;

/**
 * Created by vtiwari on 9/14/15.
 *
 * For more details >> http://ogp.me/
 */
public class OpenGraphMetadata {

    private String title;

    private String description;

    private String imageURL;

    private String url;

    private String webPageUrl;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getWebPageUrl() {
        return webPageUrl;
    }

    public void setWebPageUrl(String webPageUrl) {
        this.webPageUrl = webPageUrl;
    }
}
