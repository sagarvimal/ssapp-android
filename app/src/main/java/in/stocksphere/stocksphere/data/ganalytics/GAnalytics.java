package in.stocksphere.stocksphere.data.ganalytics;

import android.app.Application;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import in.stocksphere.stocksphere.SSApplication;
import in.stocksphere.stocksphere.data.fabric.AnswersEvents;

/**
 * Created by vtiwari on 8/23/15.
 */
public class GAnalytics {

    public static Tracker getTracker(Application application) {
        return ((SSApplication) application).getDefaultTracker();
    }

    public static void sendScreenView(Tracker mTracker, String screenName) {
        mTracker.setScreenName("Image~" + screenName);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        AnswersEvents.sendEvent(GAnalyticsConstants.SCREEN_VIEW, screenName);
    }

    public static void sendEvent(Tracker mTracker, String screenName, String eventName, String type, long userId) {
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory(screenName)
                .setAction(eventName)
                .setLabel(type)
                .setValue(1)
                .build());

        //Send event to Answers
        AnswersEvents.sendEvent(screenName, eventName, type, userId);
    }
}
