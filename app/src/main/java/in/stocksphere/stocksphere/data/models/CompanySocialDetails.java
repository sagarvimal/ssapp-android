package in.stocksphere.stocksphere.data.models;

/**
 * Created by vtiwari on 8/9/15.
 */
public class CompanySocialDetails {

    private String ssSymbol;

    private long statusCount;

    private long watchingUserCount;

    private double sentimentPositive;

    private double sentimentNegative;

    public String getSsSymbol() {
        return ssSymbol;
    }

    public void setSsSymbol(String ssSymbol) {
        this.ssSymbol = ssSymbol;
    }

    public long getStatusCount() {
        return statusCount;
    }

    public void setStatusCount(long statusCount) {
        this.statusCount = statusCount;
    }

    public long getWatchingUserCount() {
        return watchingUserCount;
    }

    public void setWatchingUserCount(long watchingUserCount) {
        this.watchingUserCount = watchingUserCount;
    }

    public double getSentimentPositive() {
        return sentimentPositive;
    }

    public void setSentimentPositive(double sentimentPositive) {
        this.sentimentPositive = sentimentPositive;
    }

    public double getSentimentNegative() {
        return sentimentNegative;
    }

    public void setSentimentNegative(double sentimentNegative) {
        this.sentimentNegative = sentimentNegative;
    }
}
