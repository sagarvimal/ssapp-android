package in.stocksphere.stocksphere.data.receivers;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;

import in.stocksphere.stocksphere.Logger;
import in.stocksphere.stocksphere.data.services.RefreshWatchlistService;

public class RefreshWatchlistReceiver extends BroadcastReceiver {

    public static final String INTENT_ACTION_REFRESH_WATCHLIST = "in.stocksphere.stocksphere.REFRESH_WATCHLIST";
    private static final String INTENT_ACTION_MY_PACKAGE_REPLACED = "android.intent.action.MY_PACKAGE_REPLACED";
    private static final String INTENT_ACTION_BOOT_COMPLETED = "android.intent.action.BOOT_COMPLETED";
    private static final long REFRESH_INTERVAL = 5*60*1000; // 10 minutes in millis

    @Override
    public void onReceive(Context context, Intent intent) {

        String intentAction = intent.getAction();

        if (intentAction != null && !intentAction.isEmpty()) {

            if (intentAction.equals(INTENT_ACTION_BOOT_COMPLETED) || intentAction.equals(INTENT_ACTION_MY_PACKAGE_REPLACED)) {
                refreshWatchList(context, intent);
            } else if (intentAction.equals(INTENT_ACTION_REFRESH_WATCHLIST)) {
                refreshWatchList(context, intent);
            } else {
                Logger.log("This is not correct intent : " + intentAction);
            }
        }
    }

    private void refreshWatchList(Context context, Intent intent) {

        Logger.log("===== In refresh watchlist method of receiver");

        Intent startServiceIntent = new Intent(context, RefreshWatchlistService.class);
        context.startService(startServiceIntent);
    }

    public static void setAlarmToRefreshWatchlist(Context context)
    {
        Logger.log("- - - - - >>> in setAlarmToRefreshWatchlist");

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent alarmIntent = new Intent(context, RefreshWatchlistReceiver.class);
        alarmIntent.setAction(RefreshWatchlistReceiver.INTENT_ACTION_REFRESH_WATCHLIST);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        alarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + 25000, REFRESH_INTERVAL, pendingIntent);
    }
}
