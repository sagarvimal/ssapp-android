package in.stocksphere.stocksphere.data.models;

/**
 * NSE/Stock-Exchange Quote Details here..
 */
public class Quote extends SSBase {
	
	private double lastMarketPrice;
	
	private double closingPrice;
	
	private long lastPriceUpdateDate;
	
	private double price52WkHigh;
	
	private double price52WkLow;
	
	private double tradedVolume; // shares
	
	private double tradedValue; //in lacs
	
	private double openPrice;
	
	private double highPrice;
	
	private double lowPrice;
	
	private double previousClosePrice;
	
	private double changePrice;
	
	private double percentageChange;

	public double getLastMarketPrice() {
		return lastMarketPrice;
	}

	public void setLastMarketPrice(double lastMarketPrice) {
		this.lastMarketPrice = lastMarketPrice;
	}

	public double getClosingPrice() {
		return closingPrice;
	}

	public void setClosingPrice(double closingPrice) {
		this.closingPrice = closingPrice;
	}

	public long getLastPriceUpdateDate() {
		return lastPriceUpdateDate;
	}

	public void setLastPriceUpdateDate(long lastPriceUpdateDate) {
		this.lastPriceUpdateDate = lastPriceUpdateDate;
	}

	public double getPrice52WkHigh() {
		return price52WkHigh;
	}

	public void setPrice52WkHigh(double price52WkHigh) {
		this.price52WkHigh = price52WkHigh;
	}

	public double getPrice52WkLow() {
		return price52WkLow;
	}

	public void setPrice52WkLow(double price52WkLow) {
		this.price52WkLow = price52WkLow;
	}

	public double getTradedVolume() {
		return tradedVolume;
	}

	public void setTradedVolume(double tradedVolume) {
		this.tradedVolume = tradedVolume;
	}

	public double getTradedValue() {
		return tradedValue;
	}

	public void setTradedValue(double tradedValue) {
		this.tradedValue = tradedValue;
	}

	public double getOpenPrice() {
		return openPrice;
	}

	public void setOpenPrice(double openPrice) {
		this.openPrice = openPrice;
	}

	public double getHighPrice() {
		return highPrice;
	}

	public void setHighPrice(double highPrice) {
		this.highPrice = highPrice;
	}

	public double getLowPrice() {
		return lowPrice;
	}

	public void setLowPrice(double lowPrice) {
		this.lowPrice = lowPrice;
	}

	public double getPreviousClosePrice() {
		return previousClosePrice;
	}

	public void setPreviousClosePrice(double previousClosePrice) {
		this.previousClosePrice = previousClosePrice;
	}

	public double getChangePrice() {
		return changePrice;
	}

	public void setChangePrice(double changePrice) {
		this.changePrice = changePrice;
	}

	public double getPercentageChange() {
		return percentageChange;
	}

	public void setPercentageChange(double percentageChange) {
		this.percentageChange = percentageChange;
	}
}
