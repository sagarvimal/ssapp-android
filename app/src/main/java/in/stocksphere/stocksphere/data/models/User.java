package in.stocksphere.stocksphere.data.models;

public class User extends SSBase {

    private long userId;

    private String userName;

    private String name;

    private String aboutUser;

    private long fbAndroidUId;

    private String email;

    private String dob;

    private String gender;

    private String phone;

    private String account;

    private long cFollowingUsers;

    private long cFollowingCompanies;

    private long cFollowsers;

    private long cStatus;

    private boolean isActive;

    private boolean isCallingUserFollowing;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public long getcFollowingUsers() {
        return cFollowingUsers;
    }

    public void setcFollowingUsers(long cFollowingUsers) {
        this.cFollowingUsers = cFollowingUsers;
    }

    public long getcFollowingCompanies() {
        return cFollowingCompanies;
    }

    public void setcFollowingCompanies(long cFollowingCompanies) {
        this.cFollowingCompanies = cFollowingCompanies;
    }

    public long getcFollowsers() {
        return cFollowsers;
    }

    public void setcFollowsers(long cFollowsers) {
        this.cFollowsers = cFollowsers;
    }

    public boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAboutUser() {
        return aboutUser;
    }

    public void setAboutUser(String aboutUser) {
        this.aboutUser = aboutUser;
    }

    public long getFbAndroidUId() {
        return fbAndroidUId;
    }

    public void setFbAndroidUId(long fbAndroidUId) {
        this.fbAndroidUId = fbAndroidUId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public boolean getIsCallingUserFollowing() {
        return isCallingUserFollowing;
    }

    public void setIsCallingUserFollowing(boolean isCallingUserFollowing) {
        this.isCallingUserFollowing = isCallingUserFollowing;
    }

    public long getcStatus() {
        return cStatus;
    }

    public void setcStatus(long cStatus) {
        this.cStatus = cStatus;
    }
}
