package in.stocksphere.stocksphere.data.sqlite;

import java.util.Date;

import in.stocksphere.stocksphere.data.common.SSConstants;
import in.stocksphere.stocksphere.data.models.Company;
import in.stocksphere.stocksphere.data.models.Quote;
import in.stocksphere.stocksphere.data.models.Status;
import in.stocksphere.stocksphere.data.models.WatchlistNotification;
import in.stocksphere.stocksphere.data.sqlite.greendao.CompanyTimeline;
import in.stocksphere.stocksphere.data.sqlite.greendao.HomeTimeline;
import in.stocksphere.stocksphere.data.sqlite.greendao.StockWatchlist;
import in.stocksphere.stocksphere.data.sqlite.greendao.UserTimeline;

/**
 * Created by vtiwari on 7/8/15.
 */
public class SSModelTransformer {

    public static HomeTimeline getHomeTimeLine(Status status, long thisUserId) {

        HomeTimeline homeTimeline = new HomeTimeline();
        homeTimeline.setCFavorites(status.getcFavorites());
        homeTimeline.setCLikes(status.getcLikes());
        homeTimeline.setCreatedDate(new Date(status.getCreatedDate()));
        homeTimeline.setCReplies(status.getcReplies());
        homeTimeline.setIsDeleted(status.getIsDeleted());
        homeTimeline.setIsFavourited(status.getIsFavourited());
        homeTimeline.setIsLiked(status.getIsLiked());
        homeTimeline.setLastUpdatedDate(new Date(status.getLastUpdatedDate()));
        homeTimeline.setOperation(status.getOperation());
        homeTimeline.setReplyOfId(status.getReplyOfId());
        homeTimeline.setSentiment(status.getSentiment());
        homeTimeline.setStatusId(status.getId());
        homeTimeline.setStatusText(status.getStatusText());
        homeTimeline.setUserFullName(status.getUserFullName());
        homeTimeline.setUserId(status.getUserId());
        homeTimeline.setUserIdOfOperation(status.getUserIdOfOperation());
        homeTimeline.setUserName(status.getUserName());
        homeTimeline.setUserNameOfOperation(status.getUserNameOfOperation());
        homeTimeline.setUserOfOperation(status.getUserOfOperation());
        homeTimeline.setUserFbAndroidUId(status.getUserFbAndroidUId());
        homeTimeline.setUserFbAndroidUIdOfOperation(status.getUserFbAndroidUIdOfOperation());
        homeTimeline.setPrimaryKey(thisUserId + SSConstants.HYPHEN + status.getId());
        homeTimeline.setThisUserId(thisUserId);
        return homeTimeline;
    }

    public static Status getStatus(HomeTimeline homeTimeline) {

        Status status = new Status();
        status.setcFavorites(homeTimeline.getCFavorites());
        status.setcLikes(homeTimeline.getCLikes());
        status.setUserFullName(homeTimeline.getUserFullName());
        status.setUserOfOperation(homeTimeline.getUserOfOperation());
        status.setUserNameOfOperation(homeTimeline.getUserNameOfOperation());
        status.setUserName(homeTimeline.getUserName());
        status.setIsLiked(homeTimeline.getIsLiked());
        status.setIsDeleted(homeTimeline.getIsDeleted());
        status.setIsFavourited(homeTimeline.getIsFavourited());
        status.setStatusText(homeTimeline.getStatusText());
        status.setOperation(homeTimeline.getOperation());
        status.setLastUpdatedDate(homeTimeline.getLastUpdatedDate().getTime());
        status.setCreatedDate(homeTimeline.getCreatedDate().getTime());
        status.setUserIdOfOperation(homeTimeline.getUserIdOfOperation());
        status.setUserId(homeTimeline.getUserId());
        status.setId(homeTimeline.getStatusId());
        status.setcReplies(homeTimeline.getCReplies());
        status.setReplyOfId(homeTimeline.getReplyOfId());
        status.setSentiment(homeTimeline.getSentiment());
        status.setUserFbAndroidUId(homeTimeline.getUserFbAndroidUId());
        status.setUserFbAndroidUIdOfOperation(homeTimeline.getUserFbAndroidUIdOfOperation());
        return status;
    }

    public static UserTimeline getUserTimeLine(Status status, long userTimelineUserId, long thisUserId) {

        UserTimeline userTimeline = new UserTimeline();
        userTimeline.setCFavorites(status.getcFavorites());
        userTimeline.setCLikes(status.getcLikes());
        userTimeline.setCreatedDate(new Date(status.getCreatedDate()));
        userTimeline.setCReplies(status.getcReplies());
        userTimeline.setIsDeleted(status.getIsDeleted());
        userTimeline.setIsFavourited(status.getIsFavourited());
        userTimeline.setIsLiked(status.getIsLiked());
        userTimeline.setLastUpdatedDate(new Date(status.getLastUpdatedDate()));
        userTimeline.setOperation(status.getOperation());
        userTimeline.setReplyOfId(status.getReplyOfId());
        userTimeline.setSentiment(status.getSentiment());
        userTimeline.setStatusId(status.getId());
        userTimeline.setStatusText(status.getStatusText());
        userTimeline.setUserFullName(status.getUserFullName());
        userTimeline.setUserId(status.getUserId());
        userTimeline.setUserIdOfOperation(status.getUserIdOfOperation());
        userTimeline.setUserName(status.getUserName());
        userTimeline.setUserNameOfOperation(status.getUserNameOfOperation());
        userTimeline.setUserOfOperation(status.getUserOfOperation());
        userTimeline.setUserTimelineUserId(userTimelineUserId);
        userTimeline.setUserFbAndroidUId(status.getUserFbAndroidUId());
        userTimeline.setUserFbAndroidUIdOfOperation(status.getUserFbAndroidUIdOfOperation());
        userTimeline.setPrimaryKey(thisUserId + SSConstants.HYPHEN + status.getId() + SSConstants.HYPHEN + userTimelineUserId);
        userTimeline.setThisUserId(thisUserId);
        return userTimeline;
    }

    public static Status getStatus(UserTimeline userTimeline) {

        Status status = new Status();
        status.setcFavorites(userTimeline.getCFavorites());
        status.setcLikes(userTimeline.getCLikes());
        status.setUserFullName(userTimeline.getUserFullName());
        status.setUserOfOperation(userTimeline.getUserOfOperation());
        status.setUserNameOfOperation(userTimeline.getUserNameOfOperation());
        status.setUserName(userTimeline.getUserName());
        status.setIsLiked(userTimeline.getIsLiked());
        status.setIsDeleted(userTimeline.getIsDeleted());
        status.setIsFavourited(userTimeline.getIsFavourited());
        status.setStatusText(userTimeline.getStatusText());
        status.setOperation(userTimeline.getOperation());
        status.setLastUpdatedDate(userTimeline.getLastUpdatedDate().getTime());
        status.setCreatedDate(userTimeline.getCreatedDate().getTime());
        status.setUserIdOfOperation(userTimeline.getUserIdOfOperation());
        status.setUserId(userTimeline.getUserId());
        status.setId(userTimeline.getStatusId());
        status.setcReplies(userTimeline.getCReplies());
        status.setReplyOfId(userTimeline.getReplyOfId());
        status.setSentiment(userTimeline.getSentiment());
        status.setUserFbAndroidUId(userTimeline.getUserFbAndroidUId());
        status.setUserFbAndroidUIdOfOperation(userTimeline.getUserFbAndroidUIdOfOperation());
        return status;
    }

    public static CompanyTimeline getCompanyTimeLine(Status status, String ssSymbol, long thisUserId) {

        CompanyTimeline companyTimeline = new CompanyTimeline();
        companyTimeline.setCFavorites(status.getcFavorites());
        companyTimeline.setCLikes(status.getcLikes());
        companyTimeline.setCreatedDate(new Date(status.getCreatedDate()));
        companyTimeline.setCReplies(status.getcReplies());
        companyTimeline.setIsDeleted(status.getIsDeleted());
        companyTimeline.setIsFavourited(status.getIsFavourited());
        companyTimeline.setIsLiked(status.getIsLiked());
        companyTimeline.setLastUpdatedDate(new Date(status.getLastUpdatedDate()));
        companyTimeline.setOperation(status.getOperation());
        companyTimeline.setReplyOfId(status.getReplyOfId());
        companyTimeline.setSentiment(status.getSentiment());
        companyTimeline.setStatusId(status.getId());
        companyTimeline.setStatusText(status.getStatusText());
        companyTimeline.setUserFullName(status.getUserFullName());
        companyTimeline.setUserId(status.getUserId());
        companyTimeline.setUserIdOfOperation(status.getUserIdOfOperation());
        companyTimeline.setUserName(status.getUserName());
        companyTimeline.setUserNameOfOperation(status.getUserNameOfOperation());
        companyTimeline.setUserOfOperation(status.getUserOfOperation());
        companyTimeline.setCompanySsSymbol(ssSymbol);
        companyTimeline.setPrimaryKey(thisUserId + SSConstants.HYPHEN + status.getId() + SSConstants.HYPHEN + ssSymbol);
        companyTimeline.setUserFbAndroidUId(status.getUserFbAndroidUId());
        companyTimeline.setUserFbAndroidUIdOfOperation(status.getUserFbAndroidUIdOfOperation());
        companyTimeline.setThisUserId(thisUserId);
        return companyTimeline;
    }

    public static Status getStatus(CompanyTimeline companyTimeline) {

        Status status = new Status();
        status.setcFavorites(companyTimeline.getCFavorites());
        status.setcLikes(companyTimeline.getCLikes());
        status.setUserFullName(companyTimeline.getUserFullName());
        status.setUserOfOperation(companyTimeline.getUserOfOperation());
        status.setUserNameOfOperation(companyTimeline.getUserNameOfOperation());
        status.setUserName(companyTimeline.getUserName());
        status.setIsLiked(companyTimeline.getIsLiked());
        status.setIsDeleted(companyTimeline.getIsDeleted());
        status.setIsFavourited(companyTimeline.getIsFavourited());
        status.setStatusText(companyTimeline.getStatusText());
        status.setOperation(companyTimeline.getOperation());
        status.setLastUpdatedDate(companyTimeline.getLastUpdatedDate().getTime());
        status.setCreatedDate(companyTimeline.getCreatedDate().getTime());
        status.setUserIdOfOperation(companyTimeline.getUserIdOfOperation());
        status.setUserId(companyTimeline.getUserId());
        status.setId(companyTimeline.getStatusId());
        status.setcReplies(companyTimeline.getCReplies());
        status.setReplyOfId(companyTimeline.getReplyOfId());
        status.setSentiment(companyTimeline.getSentiment());
        status.setUserFbAndroidUId(companyTimeline.getUserFbAndroidUId());
        status.setUserFbAndroidUIdOfOperation(companyTimeline.getUserFbAndroidUIdOfOperation());
        return status;
    }

    public static StockWatchlist getStockWatchlist(Company company, long thisUserId) {

        Quote quote = company.getNseQuote();
        WatchlistNotification notification = company.getWlNotification();

        StockWatchlist stockWatchlist = new StockWatchlist();
        stockWatchlist.setSsSymbol(company.getSsSymbol());
        stockWatchlist.setName(company.getName());
        stockWatchlist.setNseSymbol(company.getNseSymbol());
        stockWatchlist.setPrimaryKey(thisUserId + SSConstants.HYPHEN + company.getSsSymbol());
        stockWatchlist.setThisUserId(thisUserId);

        if (quote != null) {
            stockWatchlist.setChangePrice(quote.getChangePrice());
            stockWatchlist.setClosingPrice(quote.getClosingPrice());
            stockWatchlist.setHighPrice(quote.getHighPrice());
            stockWatchlist.setLastMarketPrice(quote.getLastMarketPrice());
            stockWatchlist.setLowPrice(quote.getLowPrice());
            stockWatchlist.setOpenPrice(quote.getOpenPrice());
            stockWatchlist.setPercentageChange(quote.getPercentageChange());
            stockWatchlist.setPreviousClosePrice(quote.getPreviousClosePrice());
            stockWatchlist.setPrice52WkHigh(quote.getPrice52WkHigh());
            stockWatchlist.setPrice52WkLow(quote.getPrice52WkLow());
            stockWatchlist.setTradedValue(quote.getTradedValue());
            stockWatchlist.setTradedVolume(quote.getTradedVolume());
            stockWatchlist.setLastPriceUpdateDate(new Date(quote.getLastUpdatedDate()));
        }

        if (notification != null) {
            stockWatchlist.setIsPushNotifEnabled(notification.getIsPushNotifEnabled());
            stockWatchlist.setMaxPriceNotif(notification.getMaxPrice());
            stockWatchlist.setMinPriceNotif(notification.getMinPrice());
        }
        return stockWatchlist;
    }

    public static Company getCompanyForWatchlist(StockWatchlist stockWatchlist) {

        Company company = new Company();
        Quote quote = new Quote();
        WatchlistNotification notification = new WatchlistNotification();

        company.setSsSymbol(stockWatchlist.getSsSymbol());
        company.setNseSymbol(stockWatchlist.getNseSymbol());

        if (stockWatchlist.getName() != null) {
            company.setName(stockWatchlist.getName());
        } else {
            company.setName(stockWatchlist.getSsSymbol());
        }

        if (stockWatchlist.getLastPriceUpdateDate() != null) {
            quote.setLastUpdatedDate(stockWatchlist.getLastPriceUpdateDate().getTime());
        }
        if (stockWatchlist.getChangePrice() != null) {
            quote.setChangePrice(stockWatchlist.getChangePrice());
        }
        if (stockWatchlist.getClosingPrice() != null) {
            quote.setClosingPrice(stockWatchlist.getClosingPrice());
        }
        if (stockWatchlist.getHighPrice() != null) {
            quote.setHighPrice(stockWatchlist.getHighPrice());
        }
        if (stockWatchlist.getLastMarketPrice() != null) {
            quote.setLastMarketPrice(stockWatchlist.getLastMarketPrice());
        }
        if (stockWatchlist.getLowPrice() != null) {
            quote.setLowPrice(stockWatchlist.getLowPrice());
        }
        if (stockWatchlist.getOpenPrice() != null) {
            quote.setOpenPrice(stockWatchlist.getOpenPrice());
        }
        if (stockWatchlist.getPreviousClosePrice() != null) {
            quote.setPreviousClosePrice(stockWatchlist.getPreviousClosePrice());
        }
        if (stockWatchlist.getPrice52WkHigh() != null) {
            quote.setPrice52WkHigh(stockWatchlist.getPrice52WkHigh());
        }
        if (stockWatchlist.getPrice52WkLow() != null) {
            quote.setPrice52WkLow(stockWatchlist.getPrice52WkLow());
        }
        if (stockWatchlist.getTradedValue() != null) {
            quote.setTradedValue(stockWatchlist.getTradedValue());
        }
        if (stockWatchlist.getTradedVolume() != null) {
            quote.setTradedVolume(stockWatchlist.getTradedVolume());
        }
        if (stockWatchlist.getPercentageChange() != null) {
            quote.setPercentageChange(stockWatchlist.getPercentageChange());
        }

        if (stockWatchlist.getIsPushNotifEnabled() != null) {
            notification.setIsPushNotifEnabled(stockWatchlist.getIsPushNotifEnabled());
        }
        if (stockWatchlist.getMaxPriceNotif() != null) {
            notification.setMaxPrice(stockWatchlist.getMaxPriceNotif());
        }
        if (stockWatchlist.getMinPriceNotif() != null) {
            notification.setMinPrice(stockWatchlist.getMinPriceNotif());
        }

        company.setNseQuote(quote);
        company.setWlNotification(notification);

        return company;
    }
}
