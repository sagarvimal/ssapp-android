package in.stocksphere.stocksphere.data.models;

import java.util.ArrayList;

/**
 * Created by vtiwari on 7/26/15.
 */
public class CompanyList extends SSBase {

    private ArrayList<Company> companies;

    public ArrayList<Company> getCompanies() {
        return companies;
    }

    public void setCompanies(ArrayList<Company> companies) {
        this.companies = companies;
    }
}
