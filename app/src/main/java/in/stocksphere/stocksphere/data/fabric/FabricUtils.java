package in.stocksphere.stocksphere.data.fabric;

import android.util.Log;

import com.crashlytics.android.Crashlytics;

public class FabricUtils {

    public static void logError(String tag, String error) {

        Crashlytics.getInstance().core.log(Log.ERROR, tag, error);
    }

    public static void logException(Throwable t) {

        Crashlytics.getInstance().core.logException(t);
    }

    public static void log(String tag, String msg) {

        Crashlytics.getInstance().core.log(Log.INFO, tag, msg);
    }
}
