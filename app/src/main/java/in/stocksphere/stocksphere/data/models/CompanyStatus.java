package in.stocksphere.stocksphere.data.models;

import java.util.ArrayList;

public class CompanyStatus extends SSBase {
	
	private String ssSymbol;
	
	private ArrayList<Status> statuses;

	public String getSsSymbol() {
		return ssSymbol;
	}

	public void setSsSymbol(String ssSymbol) {
		this.ssSymbol = ssSymbol;
	}

	public ArrayList<Status> getStatuses() {
		return statuses;
	}

	public void setStatuses(ArrayList<Status> statuses) {
		this.statuses = statuses;
	}
}
