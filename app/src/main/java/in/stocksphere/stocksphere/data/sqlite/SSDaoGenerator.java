package in.stocksphere.stocksphere.data.sqlite;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;

/**
 * Created by vtiwari on 7/6/15.
 */
public class SSDaoGenerator {

    public static void main(String args[]) throws Exception {
        Schema schema = new Schema(2, "in.stocksphere.stocksphere.data.sqlite.greendao");

        addCompanyTimelineSchema(schema);
        addUserHomeTimelineSchema(schema);
        addUserTimelineSchema(schema);
        addWatchlistSchema(schema);
        addCompanySchema(schema);
        addOpenGraphMetadataSchema(schema);

        new DaoGenerator().generateAll(schema, "app/src/main/java");
    }

    /*
    Stores the status messages where the company is mentioned by its SS SYMBOL
     */
    private static void addCompanyTimelineSchema(Schema schema) {

        Entity status = schema.addEntity("CompanyTimeline");
        status.addStringProperty("statusId");
        status.addStringProperty("primaryKey").primaryKey().notNull(); //thisUserIdStatusIdSsSymbol
        status.addLongProperty("userId");
        status.addStringProperty("userName");
        status.addStringProperty("userFullName");
        status.addStringProperty("statusText");
        status.addStringProperty("replyOfId");
        status.addStringProperty("sentiment");
        status.addStringProperty("operation");
        status.addStringProperty("userOfOperation");//handle of user of operation
        status.addStringProperty("userNameOfOperation");
        status.addLongProperty("userIdOfOperation");
        status.addBooleanProperty("isLiked");
        status.addBooleanProperty("isFavourited");
        status.addBooleanProperty("isDeleted");
        status.addIntProperty("cLikes");
        status.addIntProperty("cFavorites");
        status.addIntProperty("cReplies");
        status.addDateProperty("createdDate");
        status.addDateProperty("lastUpdatedDate");
        status.addStringProperty("companySsSymbol");
        status.addLongProperty("userFbAndroidUId");
        status.addLongProperty("userFbAndroidUIdOfOperation");
        status.addLongProperty("thisUserId");
    }

    /*
    Stores the status messages where the which is created and liked, favourited by
    the logged in user and the user whom he follows
     */
    private static void addUserHomeTimelineSchema(Schema schema) {

        Entity status = schema.addEntity("HomeTimeline");
        status.addStringProperty("primaryKey").primaryKey().notNull();//thisUserIdStatusId
        status.addStringProperty("statusId");
        status.addLongProperty("userId");
        status.addStringProperty("userName");
        status.addStringProperty("userFullName");
        status.addStringProperty("statusText");
        status.addStringProperty("replyOfId");
        status.addStringProperty("sentiment");
        status.addStringProperty("operation");
        status.addStringProperty("userOfOperation");//handle of user of operation
        status.addStringProperty("userNameOfOperation");
        status.addLongProperty("userIdOfOperation");
        status.addBooleanProperty("isLiked");
        status.addBooleanProperty("isFavourited");
        status.addBooleanProperty("isDeleted");
        status.addIntProperty("cLikes");
        status.addIntProperty("cFavorites");
        status.addIntProperty("cReplies");
        status.addDateProperty("createdDate");
        status.addDateProperty("lastUpdatedDate");
        status.addLongProperty("userFbAndroidUId");
        status.addLongProperty("userFbAndroidUIdOfOperation");
        status.addLongProperty("thisUserId");
    }

    /*
    Stores the status messages where the which is created by the user
     */
    private static void addUserTimelineSchema(Schema schema) {

        Entity status = schema.addEntity("UserTimeline");
        status.addStringProperty("statusId");
        status.addStringProperty("primaryKey").primaryKey().notNull(); //thisUserIdStatusIdUserId
        status.addLongProperty("userId");
        status.addStringProperty("userName");
        status.addStringProperty("userFullName");
        status.addStringProperty("statusText");
        status.addStringProperty("replyOfId");
        status.addStringProperty("sentiment");
        status.addStringProperty("operation");
        status.addStringProperty("userOfOperation");//handle of user of operation
        status.addStringProperty("userNameOfOperation");
        status.addLongProperty("userIdOfOperation");
        status.addBooleanProperty("isLiked");
        status.addBooleanProperty("isFavourited");
        status.addBooleanProperty("isDeleted");
        status.addIntProperty("cLikes");
        status.addIntProperty("cFavorites");
        status.addIntProperty("cReplies");
        status.addDateProperty("createdDate");
        status.addDateProperty("lastUpdatedDate");
        status.addLongProperty("userTimelineUserId");
        status.addLongProperty("userFbAndroidUId");
        status.addLongProperty("userFbAndroidUIdOfOperation");
        status.addLongProperty("thisUserId");
    }

    /*
Stores the watchlist of user
 */
    private static void addWatchlistSchema(Schema schema) {

        Entity status = schema.addEntity("StockWatchlist");
        status.addStringProperty("name");
        status.addStringProperty("primaryKey").primaryKey().notNull();//thisUserIdSsSymbol
        status.addStringProperty("ssSymbol");
        status.addStringProperty("nseSymbol");
        status.addDoubleProperty("lastMarketPrice");
        status.addDoubleProperty("changePrice");
        status.addDoubleProperty("percentageChange");
        status.addDoubleProperty("openPrice");
        status.addDoubleProperty("highPrice");
        status.addDoubleProperty("lowPrice");
        status.addDoubleProperty("previousClosePrice");
        status.addDoubleProperty("tradedVolume");
        status.addDoubleProperty("tradedValue");
        status.addDateProperty("lastPriceUpdateDate");
        status.addDoubleProperty("closingPrice");
        status.addDoubleProperty("price52WkHigh");
        status.addDoubleProperty("price52WkLow");
        status.addBooleanProperty("isPushNotifEnabled");
        status.addDoubleProperty("maxPriceNotif");
        status.addDoubleProperty("minPriceNotif");
        status.addLongProperty("thisUserId");
    }

    /*
Stores the company names
*/
    private static void addCompanySchema(Schema schema) {

        Entity status = schema.addEntity("CompanyNames");
        status.addStringProperty("name");
        status.addStringProperty("ssSymbol").primaryKey().notNull();
        status.addStringProperty("ssSymbolName");
    }

    /*
Stores the open graph meta data - metadata of url which are there in status text
*/
    private static void addOpenGraphMetadataSchema(Schema schema) {

        Entity openGraph = schema.addEntity("OpenGraphMetadata");
        openGraph.addStringProperty("title");
        openGraph.addStringProperty("url").primaryKey().notNull();
        openGraph.addStringProperty("imageUrl");
        openGraph.addStringProperty("hostUrl");
    }
}
