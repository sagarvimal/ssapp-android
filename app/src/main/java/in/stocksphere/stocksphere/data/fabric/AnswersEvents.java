package in.stocksphere.stocksphere.data.fabric;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;

/**
 * Created by vtiwari on 12/12/15.
 */
public class AnswersEvents {

    private static final String SCREEN = "SCREEN";
    private static final String EVENT_TYPE = "E_TYPE";
    private static final String USER_ID = "ID";

    public static void sendEvent(String screenName, String eventName, String type, long userId){
        Answers.getInstance().logCustom(new CustomEvent(eventName)
                .putCustomAttribute(SCREEN, screenName)
                .putCustomAttribute(EVENT_TYPE, type)
                .putCustomAttribute(USER_ID, userId));
    }

    public static void sendEvent(String eventName, String type){
        Answers.getInstance().logCustom(new CustomEvent(eventName)
                .putCustomAttribute(EVENT_TYPE, type));
    }
}
