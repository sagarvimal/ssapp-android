package in.stocksphere.stocksphere.data.receivers;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import hugo.weaving.DebugLog;
import in.stocksphere.stocksphere.data.common.SSUtils;
import in.stocksphere.stocksphere.data.fabric.FabricUtils;
import in.stocksphere.stocksphere.data.models.SSBase;
import in.stocksphere.stocksphere.data.rest.APIHelper;
import in.stocksphere.stocksphere.data.rest.RequestObjectCreator;
import in.stocksphere.stocksphere.data.sqlite.GreenDaoUtils;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by vtiwari on 7/11/15.
 */
public class WatchlistNotifButtonClickReceiver extends BroadcastReceiver {

    public static final String INTENT_ACTION_WATCHLIST_NOTIF_BUTTON_CLICKED = "in.stocksphere.stocksphere.WATCHLIST_NOTIF_BUTTON_CLICKED";
    public static final String INTENT_ACTION_WATCHLIST_NOTIF_TRADE_BUTTON_CLICKED = "in.stocksphere.stocksphere.WATCHLIST_NOTIF_TRADE_BUTTON_CLICKED";
    public static final String NOTIFICATION_ID = "NOTIFICATION_ID";
    public static final String SS_SYMBOL = "SS_SYMBOL";
    public static final String USER_ID = "USER_ID";
    public static final String APP_PACKAGE_ID = "APP_PACKAGE_ID";

    @Override
    @DebugLog
    public void onReceive(Context context, Intent intent) {

        String intentAction = intent.getAction();

        if (intentAction != null && !intentAction.isEmpty()) {

            int notificationId = intent.getIntExtra(NOTIFICATION_ID, 0);
            String ssSymbol = intent.getStringExtra(SS_SYMBOL);
            long userId = intent.getLongExtra(USER_ID, 0);
            String appPackageId = intent.getStringExtra(APP_PACKAGE_ID);

            if (intentAction.equals(INTENT_ACTION_WATCHLIST_NOTIF_BUTTON_CLICKED)) {
                stopNotification(context, ssSymbol, userId);
            } else if (intentAction.equals(INTENT_ACTION_WATCHLIST_NOTIF_TRADE_BUTTON_CLICKED)) {

                if (appPackageId != null && !appPackageId.isEmpty() && SSUtils.isAppInstalled(context, appPackageId)) {
                    SSUtils.launchApp(context, appPackageId);
                } else {
                    Toast.makeText(context, "Your trading app is not set. Please go to StockSphere > Settings for more details.", Toast.LENGTH_LONG).show();
                }
            }

            //close notification
            String notificationService = Context.NOTIFICATION_SERVICE;
            NotificationManager notifManager = (NotificationManager) context.getSystemService(notificationService);
            notifManager.cancel(notificationId);
        }
    }

    @DebugLog
    private void stopNotification(final Context context, final String ssSymbol, long userId) {

        boolean isAddedToWatchList = true;
        final boolean isPushNotifEnabled = false;
        final double minLimit = 0;
        final double maxLimit = 0;

        APIHelper.getSsapi().updateWatchlist(RequestObjectCreator.getUpdateWatchlistObject(userId,
                ssSymbol, isAddedToWatchList, minLimit, maxLimit, isPushNotifEnabled), new Callback<SSBase>() {
            @Override
            @DebugLog
            public void success(SSBase SSBase, Response response) {

                //update sqlite
                GreenDaoUtils.updateWatchlist(context, ssSymbol, isPushNotifEnabled, minLimit, maxLimit);
                Toast.makeText(context, "Watchlist notification preference successfully updated for "
                        + ssSymbol, Toast.LENGTH_SHORT).show();
            }

            @Override
            @DebugLog
            public void failure(RetrofitError error) {
                FabricUtils.logException(error);
            }
        });
    }
}
