package in.stocksphere.stocksphere.data.aspect;

/**
 * Created by vtiwari on 9/6/15.
 */
//@Aspect
public class ScreenLoggerAspect {

//    @Around("@annotation(in.stocksphere.stocksphere.data.aspect.DisplayOnScreen)")
//    public Object logAndExecute(ProceedingJoinPoint joinPoint) throws Throwable {
//
//        if(!BuildConfig.DEBUG){
//            return joinPoint.proceed();
//        }
//
//        enterMethod(joinPoint);
//
//        long startNanos = System.nanoTime();
//        Object result = joinPoint.proceed();
//        long stopNanos = System.nanoTime();
//        long lengthMillis = TimeUnit.NANOSECONDS.toMillis(stopNanos - startNanos);
//
//        exitMethod(joinPoint, result, lengthMillis);
//
//        return result;
//    }
//
//    private static void enterMethod(JoinPoint joinPoint) {
//        CodeSignature codeSignature = (CodeSignature) joinPoint.getSignature();
//
//        Class<?> cls = codeSignature.getDeclaringType();
//        String methodName = codeSignature.getName();
//        String[] parameterNames = codeSignature.getParameterNames();
//        Object[] parameterValues = joinPoint.getArgs();
//
//        StringBuilder builder = new StringBuilder("\u21E2 ");
//        builder.append(methodName).append('(');
//        for (int i = 0; i < parameterValues.length; i++) {
//            if (i > 0) {
//                builder.append(", ");
//            }
//            builder.append(parameterNames[i]).append('=');
//            builder.append(Strings.toString(parameterValues[i]));
//        }
//        builder.append(')');
//
//        if (Looper.myLooper() != Looper.getMainLooper()) {
//            builder.append(" [Thread:\"").append(Thread.currentThread().getName()).append("\"]");
//        }
//
//        Galgo.log(asTag(cls)+ SSConstants.COLON+builder.toString());
//    }
//
//    private static void exitMethod(JoinPoint joinPoint, Object result, long lengthMillis) {
//        Signature signature = joinPoint.getSignature();
//
//        Class<?> cls = signature.getDeclaringType();
//        String methodName = signature.getName();
//        boolean hasReturnType = signature instanceof MethodSignature
//                && ((MethodSignature) signature).getReturnType() != void.class;
//
//        StringBuilder builder = new StringBuilder("\u21E0 ")
//                .append(methodName)
//                .append(" [")
//                .append(lengthMillis)
//                .append("ms]");
//
//        if (hasReturnType) {
//            builder.append(" = ");
//            builder.append(Strings.toString(result));
//        }
//
//        // Log.v(asTag(cls), builder.toString());
//        Galgo.log(asTag(cls)+ SSConstants.COLON+builder.toString());
//    }
//
//    private static String asTag(Class<?> cls) {
//        if (cls.isAnonymousClass()) {
//            return asTag(cls.getEnclosingClass());
//        }
//        return cls.getSimpleName();
//    }
}
