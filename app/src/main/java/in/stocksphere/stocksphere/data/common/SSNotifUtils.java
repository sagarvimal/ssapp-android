package in.stocksphere.stocksphere.data.common;


import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.RingtoneManager;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;

import com.parse.FunctionCallback;
import com.parse.Parse;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.SaveCallback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.HashMap;
import java.util.List;

import hugo.weaving.DebugLog;
import in.stocksphere.stocksphere.Logger;
import in.stocksphere.stocksphere.R;
import in.stocksphere.stocksphere.SSApplication;
import in.stocksphere.stocksphere.data.fabric.FabricUtils;
import in.stocksphere.stocksphere.data.ganalytics.GAnalytics;
import in.stocksphere.stocksphere.data.ganalytics.GAnalyticsConstants;
import in.stocksphere.stocksphere.data.models.AppNotification;
import in.stocksphere.stocksphere.data.receivers.WatchlistNotifButtonClickReceiver;
import in.stocksphere.stocksphere.ui.MainActivity;
import in.stocksphere.stocksphere.ui.SharedPreferencesManager;

public class SSNotifUtils {

    private static final String PARSE_APPLICATION_ID = "huHpXZ5u0Q9nSXJOktdxRwE8V7S53JqPAUcCtpYi";
    private static final String PARSE_CLIENT_KEY = "gDPmNk53FmW7tHhd177EVgpd7Y68fSH67HWm3Haw";
    private static final String CHANNEL_ALL = "";

    public static void initializeParseSDK(SSApplication application) {
        //Registering subclasses
        ParseObject.registerSubclass(AppNotification.class);

        //Initializing Parse SDK
        Parse.initialize(application, PARSE_APPLICATION_ID, PARSE_CLIENT_KEY);
    }

    public static void subscribePushNotification() {
        ParsePush.subscribeInBackground(CHANNEL_ALL, new SaveCallback() {
            public void done(com.parse.ParseException e) {
                if (e == null) {
                    Log.d("com.parse.push", "successfully subscribed to the broadcast channel.");
                } else {
                    Log.e("com.parse.push", "failed to subscribe for push", e);
                }
            }
        });
    }

    /*
     unSubscribePushNotification on log out
     */
    @DebugLog
    public static void unSubscribePushNotification() {
        ParsePush.unsubscribeInBackground(CHANNEL_ALL);
        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
        installation.put("userId", SSConstants.DEFAULT_USER_ID);
        installation.saveInBackground();
    }

    /*
     register with user id to get the user specific push notification
     */
    @DebugLog
    public static void registerUserIdForPushNotification(long userId) {
        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
        installation.put("userId", userId);
        installation.saveInBackground();
    }

    @SuppressLint("NewApi")
    @DebugLog
    public static void sendLocalNotification(Context context, String text, String ssSymbol, long userId) {

        //get random for notification id
        int id = (int) (Math.random() * 250 + 1);

        // Prepare intent which is triggered if the
        // notification is selected
        Intent intent = new Intent(context, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pIntent = PendingIntent.getActivity(context, id, intent, PendingIntent.FLAG_ONE_SHOT);

        /** sends intent on button click - this intent will be received by WatchlistNotifButtonClickReceiver
         in onReceive method. Then will be used to stop the further push notification for the company */
        Intent okButtonsIntent = new Intent(context, WatchlistNotifButtonClickReceiver.class);
        okButtonsIntent.setAction(WatchlistNotifButtonClickReceiver.INTENT_ACTION_WATCHLIST_NOTIF_BUTTON_CLICKED);
        okButtonsIntent.putExtra(WatchlistNotifButtonClickReceiver.NOTIFICATION_ID, id);
        okButtonsIntent.putExtra(WatchlistNotifButtonClickReceiver.SS_SYMBOL, ssSymbol);
        okButtonsIntent.putExtra(WatchlistNotifButtonClickReceiver.USER_ID, userId);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, okButtonsIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        /* when trade button is clicked */
        Intent tradeButtonsIntent = new Intent(context, WatchlistNotifButtonClickReceiver.class);
        tradeButtonsIntent.setAction(WatchlistNotifButtonClickReceiver.INTENT_ACTION_WATCHLIST_NOTIF_TRADE_BUTTON_CLICKED);
        tradeButtonsIntent.putExtra(WatchlistNotifButtonClickReceiver.NOTIFICATION_ID, id);
        tradeButtonsIntent.putExtra(WatchlistNotifButtonClickReceiver.SS_SYMBOL, ssSymbol);
        tradeButtonsIntent.putExtra(WatchlistNotifButtonClickReceiver.USER_ID, userId);
        tradeButtonsIntent.putExtra(WatchlistNotifButtonClickReceiver.APP_PACKAGE_ID, SharedPreferencesManager.getTradingAppPackageName(context));
        PendingIntent pendingTradeButtonIntent = PendingIntent.getBroadcast(context, 0, tradeButtonsIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        //open main activity on click on notification
        Intent openAppIntent = new Intent(context, MainActivity.class);
        PendingIntent openAppPendingIntent = PendingIntent.getActivity(context, 0, openAppIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        // Build notification
        Notification notification = new NotificationCompat.Builder(context)
                .setContentTitle(context.getResources().getString(R.string.app_name))
                .setTicker(context.getResources().getString(R.string.app_name)).setWhen(0)
                .setContentText(text)
                .setSmallIcon(R.drawable.ss_notif_small)
                .setContentIntent(openAppPendingIntent)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(text))
                .setColor(context.getResources().getColor(R.color.primary))
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher))
                .addAction(R.drawable.ic_buy_sell, "Buy/Sell", pendingTradeButtonIntent)
                .addAction(R.drawable.icon_ok, "Got it", pendingIntent)
                .build();

        //hide the notification after its selected
        notification.defaults |= Notification.DEFAULT_SOUND;
        notification.defaults |= Notification.DEFAULT_VIBRATE;
        notification.defaults |= Notification.DEFAULT_LIGHTS;
        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(id, notification);
        GAnalytics.sendEvent(GAnalytics.getTracker((SSApplication) context.getApplicationContext()), GAnalyticsConstants.PUSH_NOTIFICATION,
                GAnalyticsConstants.PUSH_NOTIFICATION_WATCHLIST, SSConstants.EMPTY_STRING, userId);

    }

    @DebugLog
    public static void sendPushNotification(long receiverId, long senderId, String senderName, long senderFbAndroidUId, String operation, String statusId) {
        if (receiverId != 0 && senderId != 0 && senderName != null && !senderName.isEmpty() && operation != null && !operation.isEmpty()) {

            HashMap<String, Object> params = new HashMap<String, Object>();

            params.put("receiverId", receiverId);
            params.put("senderId", senderId);
            params.put("senderName", senderName);
            params.put("operation", operation);
            params.put("senderFbAndroidUId", senderFbAndroidUId);
            params.put("statusId", statusId);

            Logger.log("Sending parse push notification. " + " receiverId: " + receiverId + " senderId: "
                    + senderId + " senderName: " + senderName + " operation:" + operation);

            ParseCloud.callFunctionInBackground("SaveAndSendPushNotification", params, new FunctionCallback<Object>() {

                @Override
                public void done(Object o, ParseException e) {

                    if (e == null) {
                        Logger.log("RESPONSE FROM PARSE JS CLOUD: " + o.toString());
                    } else {

                        Logger.log("ParseException from PARSE JS CLOUD: " + e);
                    }

                }
            });
        } else {
            Logger.log("Unable to send Push notification. " + " receiverId: " + receiverId + " senderId: "
                    + senderId + " senderName: " + senderName + " operation:" + operation);
        }
    }

    /*
    This will return ParseQuery<AppNotification> object which.
    Call findInBackground.findInBackground(new FindCallback<AppNotification>(){}) method from that object to get notification of the user.
     */
    public static ParseQuery<AppNotification> getAppNotificationQuery(long userId) {

        ParseQuery<AppNotification> query = ParseQuery.getQuery(AppNotification.class);
        query.whereEqualTo("receiverId", userId).orderByDescending("updatedAt").setLimit(15);

        return query;
    }

    @DebugLog
    public static void showNotificationMessage(final Context context, final String message, final long senderFbAndroidUId, final Intent intent) {

        // Check for empty push message
        if (TextUtils.isEmpty(message))
            return;

//        if (isAppIsInBackground(context)) {
        Target target = new Target() {
            @Override
            @DebugLog
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                showNotificationMessage(context, message, intent, bitmap);
            }

            @Override
            @DebugLog
            public void onBitmapFailed(Drawable errorDrawable) {
                FabricUtils.log("showNotificationMessage", "In onBitmapFailed");
                showNotificationMessage(context, message, intent,
                        BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher));
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
                //NA
            }
        };
        Picasso.with(context).load(SSUtils.getFacebookProfilePicUrl(senderFbAndroidUId)).into(target);
//        } else {
//            intent.putExtra("title", title);
//            intent.putExtra("message", message);
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//            context.startActivity(intent);
//        }
    }

    /**
     * Method checks if the app is in background or not
     *
     * @param context
     * @return
     */
    public static boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }

    /* Show parse custom push notification */
    @DebugLog
    private static void showNotificationMessage(final Context context, final String message, final Intent intent, final Bitmap bitmap) {
        int mNotificationId = (int) (Math.random() * 250 + 1);
        long CURRENT_USER_ID = SharedPreferencesManager.getCurrentlyLoggedInUserId(context);

        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        context,
                        0,
                        intent,
                        PendingIntent.FLAG_CANCEL_CURRENT
                );

        // notification icon
        final int icon = R.drawable.ss_notif_small;
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                context);
        Notification notification = mBuilder.setSmallIcon(icon)
                .setTicker(context.getResources().getString(R.string.app_name)).setWhen(0)
                .setAutoCancel(true)
                .setContentTitle(context.getResources().getString(R.string.app_name))
                .setContentIntent(resultPendingIntent)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setColor(context.getResources().getColor(R.color.primary))
                .setLargeIcon(bitmap)
                .setContentText(message)
                .build();

        //hide the notification after its selected
        notification.defaults |= Notification.DEFAULT_SOUND;
        notification.defaults |= Notification.DEFAULT_VIBRATE;
        notification.defaults |= Notification.DEFAULT_LIGHTS;
        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(mNotificationId, notification);
        GAnalytics.sendEvent(GAnalytics.getTracker((SSApplication) context.getApplicationContext()), GAnalyticsConstants.PUSH_NOTIFICATION,
                GAnalyticsConstants.PUSH_NOTIFICATION_PARSE, SSConstants.EMPTY_STRING, CURRENT_USER_ID);
    }

    @DebugLog
    public static void sendLocalNotification(Context context, String text, String title) {
        //get random for notification id
        int id = (int) (Math.random() * 250 + 1);
        long CURRENT_USER_ID = SharedPreferencesManager.getCurrentlyLoggedInUserId(context);

        //open main activity on click on notification
        Intent openAppIntent = new Intent(context, MainActivity.class);
        PendingIntent openAppPendingIntent = PendingIntent.getActivity(context, 0, openAppIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        if(title == null || title.isEmpty()){
            title = context.getResources().getString(R.string.app_name);
        }

        // Build notification
        Notification notification = new NotificationCompat.Builder(context)
                .setContentTitle(title)
                .setTicker(title).setWhen(0)
                .setContentText(text)
                .setSmallIcon(R.drawable.ss_notif_small)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher))
                .setContentIntent(openAppPendingIntent)
                .setColor(context.getResources().getColor(R.color.primary))
                .setStyle(new NotificationCompat.BigTextStyle().bigText(text))
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .build();

        //hide the notification after its selected
        notification.defaults |= Notification.DEFAULT_SOUND;
        notification.defaults |= Notification.DEFAULT_VIBRATE;
        notification.defaults |= Notification.DEFAULT_LIGHTS;
        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(id, notification);
        GAnalytics.sendEvent(GAnalytics.getTracker((SSApplication) context.getApplicationContext()), GAnalyticsConstants.PUSH_NOTIFICATION,
                GAnalyticsConstants.PUSH_NOTIFICATION_INDEX_PRICES, SSConstants.EMPTY_STRING, CURRENT_USER_ID);
    }
}
