package in.stocksphere.stocksphere.data.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import hugo.weaving.DebugLog;
import in.stocksphere.stocksphere.Logger;
import in.stocksphere.stocksphere.data.common.SSConstants;
import in.stocksphere.stocksphere.data.common.SSNotifUtils;
import in.stocksphere.stocksphere.data.common.SSUtils;
import in.stocksphere.stocksphere.data.fabric.AnswersEvents;
import in.stocksphere.stocksphere.data.fabric.FabricUtils;
import in.stocksphere.stocksphere.data.ganalytics.GAnalyticsConstants;
import in.stocksphere.stocksphere.data.models.Company;
import in.stocksphere.stocksphere.data.models.CompanyList;
import in.stocksphere.stocksphere.data.models.GFGetInfo;
import in.stocksphere.stocksphere.data.models.UserStatus;
import in.stocksphere.stocksphere.data.models.WatchList;
import in.stocksphere.stocksphere.data.rest.APIHelper;
import in.stocksphere.stocksphere.data.rest.RequestObjectCreator;
import in.stocksphere.stocksphere.data.rest.SSAPI;
import in.stocksphere.stocksphere.data.sqlite.GreenDaoUtils;
import in.stocksphere.stocksphere.ui.SharedPreferencesManager;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class RefreshWatchlistService extends IntentService {

    private static final String TAG = RefreshWatchlistService.class.getName();

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public RefreshWatchlistService(String name) {
        super(name);
    }

    public RefreshWatchlistService() {
        super("SS-RefreshWatchlistService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        Logger.log("- - - - - >>> in onHandleIntent. Refreshing watchlist");

        try {
            refreshWatchlist(this);
            sendIndexPricesNotification(this);
            downLoadCompanyNames(this);
            requestNews(this);
        } catch (Throwable t) {
            FabricUtils.logError(TAG, t.getMessage());
            FabricUtils.logException(t);
            Logger.log("- - - - - >>> Exception in onHandleIntent");
            t.printStackTrace();
        }
    }

    private synchronized void refreshWatchlist(final Context context) {
        SSAPI ssapi = APIHelper.getSsapi();

        final long userId = SharedPreferencesManager.getCurrentlyLoggedInUserId(context);

        if (userId != SSConstants.DEFAULT_USER_ID && SSUtils.isBusinessHour() && SSUtils.isNetworkAvailable(context)) {
            ssapi.getWatchlist(RequestObjectCreator.getWatchlistObject(userId), new Callback<WatchList>() {
                @Override
                @DebugLog
                public void success(WatchList watchList, Response response) {

                    if (watchList != null && watchList.getMessage() == null) {

                        ArrayList<Company> companies = watchList.getWatchlist();

                        if (companies != null && !companies.isEmpty()) {

                            if (SSUtils.isFetchDataFromGF()) {
                                fetchWatchListQuotesFromGF(context, userId, companies);
                            } else {
                                validateAndNotify(companies, context, userId);
                            }
                        } else {
                            Logger.log("No companies in watchlist for user: " + userId);
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {

                    FabricUtils.logError(TAG, error.getMessage());
                    FabricUtils.logException(error);
                    Logger.log("WatchList Callback failed in refreshWatchlist. " + error);
                }
            });
        } else {
            Logger.log("Unable to refresh watchlist. Either User Id is -1 or Network is not available or time is not business hour.");
        }
    }

    /*
    *  Save the company details locally, validate the notif push conditions and sends local push notifications
    * */
    @DebugLog
    private synchronized void validateAndNotify(ArrayList<Company> companies, Context context, long userId) {
        //update it to sqlite
        GreenDaoUtils.addStockWatchList(context, companies);

        //send notification
        for (Company company : companies) {

            if (company.getWlNotification() != null && company.getWlNotification()
                    .getIsPushNotifEnabled() && company.getNseQuote() != null) {

                if ((company.getWlNotification().getMinPrice() > 0
                        && company.getNseQuote().getLastMarketPrice()
                        < company.getWlNotification().getMinPrice())
                        || (company.getWlNotification().getMaxPrice()
                        > 0 && company.getNseQuote().getLastMarketPrice()
                        > company.getWlNotification().getMaxPrice())) {

                    String notifMsg = getNotificationMessage(company.getSsSymbol(),
                            company.getNseQuote().getLastMarketPrice(),
                            company.getNseQuote().getPercentageChange());

                    //send local notification
                    SSNotifUtils.sendLocalNotification(context, notifMsg, company.getSsSymbol(), userId);
                } else {
                    Logger.log("Notification price is out of range for ssysmbol: "
                            + company.getSsSymbol() + " userId: " + userId);
                }
            } else {
                Logger.log("No WatchlistFragment notification enabled or NSE quote is null for sssymbol: "
                        + company.getSsSymbol() + " UserId : " + userId);
            }

            /* TRENDING COMPANY NOTIF */
            //Notify if company percentage change is more than 3.9% and last price is more than 20rs
            long lastCompanyNotifTime = SharedPreferencesManager.getTrendingCompanyNotifTime(context);
            long timeDiff = TimeUnit.MILLISECONDS.toHours(new Date().getTime() - lastCompanyNotifTime);

            if (company.getNseQuote().getLastMarketPrice() >= SSConstants.STOCK_NOTIF_PRICE
                    && (company.getNseQuote().getPercentageChange() >= SSConstants.STOCK_NOTIF_PERCENTAGE
                    || company.getNseQuote().getPercentageChange() < -SSConstants.STOCK_NOTIF_PERCENTAGE)
                    && timeDiff > 15) {

                String a = "UP";

                if (company.getNseQuote().getPercentageChange() < 0) {
                    a = "DOWN";
                }

                String notifMsg = "Your watchlist company " + company.getName()
                        + " is currently trading at " + company.getNseQuote().getLastMarketPrice()
                        + ", " + a + " " + company.getNseQuote().getPercentageChange() + "%. Tap to open watchlist.";

                SSNotifUtils.sendLocalNotification(context, notifMsg, company.getSsSymbol());
                SharedPreferencesManager.saveTrendingCompanyNotifTime(context, new Date().getTime());
                AnswersEvents.sendEvent("", GAnalyticsConstants.PUSH_NOTIFICATION, GAnalyticsConstants.PUSH_NOTIFICATION_TRENDING_COMPANY, userId);
            }
        }
    }

    @DebugLog
    private void fetchWatchListQuotesFromGF(final Context context, final long userId, final ArrayList<Company> companies) {

        if (companies == null || companies.isEmpty()) {
            return;
        }

        APIHelper.getSsapiGoogle().getStockQuoteGF(SSUtils.getGFCNameQueryStr(companies), new Callback<List<GFGetInfo>>() {
            @Override
            public void success(final List<GFGetInfo> list, Response response) {

                if (list == null || list.isEmpty()) {
                    return;
                }
                //update google finance values
                updateGFWatchlistValues(list, companies);
                validateAndNotify(companies, context, userId);
            }

            @Override
            public void failure(RetrofitError error) {
                validateAndNotify(companies, context, userId);
                FabricUtils.logException(error);
                Logger.log("Exception occurred in getting watchlist quotes from GF " + error);
            }
        });
    }

    // update google finance watchlist values to local watchlist before populate to UI
    @DebugLog
    private void updateGFWatchlistValues(List<GFGetInfo> gfWatchlist, ArrayList<Company> companies) {

        for (GFGetInfo gfGetInfo : gfWatchlist) {

            // if last trading price is not null or empty
            if (gfGetInfo.getL() != null && !gfGetInfo.getL().isEmpty()) {

                for (Company company : companies) {

                    SSUtils.populateCompanyDetailsFromGF(company, gfGetInfo);
                }
            }
        }
    }

    @DebugLog
    private String getNotificationMessage(String ssSymbol, double lastMarketPrice, double percentageChange) {

        String notificationMessage;

        if (percentageChange > 0) {
            notificationMessage = ssSymbol + " is trading at " + lastMarketPrice + ", UP " + percentageChange + "%";
        } else if (percentageChange < 0) {
            //remove minus symbol and then add percentage change
            notificationMessage = ssSymbol + " is trading at " + lastMarketPrice + ", DOWN " + String.valueOf(percentageChange).substring(1) + "%";
        } else {
            notificationMessage = ssSymbol + " is trading at " + lastMarketPrice;
        }

        return notificationMessage;
    }

    @DebugLog
    private String getNotificationMessageForIndex(String indexName, double lastMarketPrice, double pointChange) {

        String notificationMessage;

        if (pointChange > 0) {
            notificationMessage = indexName + " is trading at " + lastMarketPrice + ", UP " + pointChange + " points.";
        } else if (pointChange < 0) {
            //remove minus symbol and then add points change
            notificationMessage = indexName + " is trading at " + lastMarketPrice + ", DOWN " + String.valueOf(pointChange).substring(1) + " points.";
        } else {
            notificationMessage = indexName + " is trading at " + lastMarketPrice;
        }

        return notificationMessage;
    }

    /*
     * Sends the notification about the price of NIFTY and SENSEX thrice a business day
     */
    @DebugLog
    private synchronized void sendIndexPricesNotification(final Context context) {

        if (!SSUtils.isNetworkAvailable(context)) {
            Logger.log("Network not available");
            return;
        }

        if (!SSUtils.isSenIndexPriceNotif(context)) {
            Logger.log("Time is not in range for showing INDEX notification / Last Notification was send within 30 mints");
            return;
        }

        //get nifty prices
        APIHelper.getSsapiGoogle().getStockQuoteGF(SSConstants.QUERY_NIFTY, new Callback<List<GFGetInfo>>() {
            @Override
            public void success(final List<GFGetInfo> list, Response response) {
                if (list == null || list.isEmpty()) {
                    return;
                }

                if (!list.get(0).getL().isEmpty() && !list.get(0).getCp().isEmpty()) {
                    final String notifMsgNifty = getNotificationMessageForIndex("NIFTY", SSUtils.getDouble(list.get(0).getL()), SSUtils.getDouble(list.get(0).getC()));

                    //get sensex prices
                    APIHelper.getSsapiGoogle().getStockQuoteGF(SSConstants.QUERY_SENSEX, new Callback<List<GFGetInfo>>() {
                        @Override
                        public void success(final List<GFGetInfo> list, Response response) {
                            if (list == null || list.isEmpty()) {
                                return;
                            }

                            String notifMsgSensex = null;
                            if (!list.get(0).getL().isEmpty() && !list.get(0).getCp().isEmpty()) {
                                notifMsgSensex = getNotificationMessageForIndex("SENSEX", SSUtils.getDouble(list.get(0).getL()), SSUtils.getDouble(list.get(0).getC()));
                            }

                            String notifMsg;
                            if (notifMsgSensex != null) {
                                notifMsg = notifMsgSensex + SSConstants.SPACE + notifMsgNifty;
                            } else {
                                notifMsg = notifMsgNifty;
                            }

                            notifMsg = notifMsg + SSConstants.SPACE + "Tap to open your watchlist.";

                            SSNotifUtils.sendLocalNotification(context, notifMsg, null);
                            SharedPreferencesManager.saveLastIndexNotifTime(context, new Date().getTime());
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            FabricUtils.logException(error);
                        }
                    });
                }
            }

            @Override
            public void failure(RetrofitError error) {
                FabricUtils.logException(error);
            }
        });
    }

    /*
     * Download company names on every 24 hours
     */
    @DebugLog
    private synchronized void downLoadCompanyNames(final Context context) {

        if (!SSUtils.isNetworkAvailable(context)) {
            Logger.log("Network not available");
            return;
        }

        long lastCompanyNameDownloadTime = SharedPreferencesManager.getLastCompanyNameDownloadTime(context);
        long timeDiff = TimeUnit.MILLISECONDS.toHours(new Date().getTime() - lastCompanyNameDownloadTime);

        if (timeDiff >= 24) {

            APIHelper.getSsapi().getAllCompanyNames(RequestObjectCreator.getAllCompanyNamesObject(), new Callback<CompanyList>() {
                @Override
                public void success(CompanyList companyList, Response response) {

                    if (companyList == null || companyList.getCompanies() == null || companyList.getCompanies().isEmpty()) {

                        Logger.log("null in all company names");
                        return;
                    }

                    GreenDaoUtils.addToCompanyNames(context, companyList.getCompanies());
                    if (companyList.getCompanies().size() > 1500) {
                        SharedPreferencesManager.saveLastCompanyNameDownloadTime(context, new Date().getTime());
                        Logger.log("Company names downloaded successfully. Total companies: " + companyList.getCompanies().size());
                    } else {
                        Logger.log("Less than 1500 companies downloaded. It's not the full list, will download again before saving download time");
                    }
                }

                @Override
                public void failure(RetrofitError error) {

                    Logger.log("RetrofitError in all company names");
                    FabricUtils.logException(error);
                }
            });
        } else {
            Logger.log("Not able to download company names. Time not in range. Time diff (hours): " + timeDiff);
        }
    }

    @DebugLog
    private synchronized void requestNews(final Context context) {
        Logger.log("");

        if (!SSUtils.isNetworkAvailable(context)) {
            Logger.log("Network not available");
            return;
        }

        long lastNewsDownloadTime = SharedPreferencesManager.getLastNewsDownloadTime(context);
        long timeDiff = TimeUnit.MILLISECONDS.toHours(new Date().getTime() - lastNewsDownloadTime);

        if (timeDiff >= 1) {

            final long userId = SharedPreferencesManager.getCurrentlyLoggedInUserId(context);

            APIHelper.getSsapi().getUserStatusUpdates(RequestObjectCreator.getUserStatusUpdatesObject(SSConstants.STOCK_SPHERE_USER_ID, userId), new Callback<UserStatus>() {
                @Override
                public void success(final UserStatus userStatus, Response response) {

                    Logger.log("In Success news..");

                    if (userStatus == null || userStatus.getStatuses() == null || userStatus.getStatuses().isEmpty()) {
                        return;
                    }
                    GreenDaoUtils.addToUserTimeline(context, userStatus.getStatuses(), SSConstants.STOCK_SPHERE_USER_ID);
                    SharedPreferencesManager.saveLastNewsDownloadTime(context, new Date().getTime());
                }

                @Override
                public void failure(RetrofitError error) {
                    Logger.log("Failed to get news :> " + error);
                    SharedPreferencesManager.saveLastNewsDownloadTime(context, new Date().getTime());
                }
            });
        }
        else {
            Logger.log("Not able to download news. Time not in range. Time diff (hours): " + timeDiff);
        }
    }

}
