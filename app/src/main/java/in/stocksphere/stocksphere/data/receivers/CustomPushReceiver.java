package in.stocksphere.stocksphere.data.receivers;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.parse.ParsePushBroadcastReceiver;

import org.json.JSONException;
import org.json.JSONObject;

import hugo.weaving.DebugLog;
import in.stocksphere.stocksphere.data.common.SSConstants;
import in.stocksphere.stocksphere.data.common.SSNotifUtils;
import in.stocksphere.stocksphere.ui.MainActivity;
import in.stocksphere.stocksphere.ui.StatusActivity;
import in.stocksphere.stocksphere.ui.UserActivity;
import in.stocksphere.stocksphere.ui.custom.IntentConstants;

/**
 * Created by vtiwari on 8/19/15.
 */
public class CustomPushReceiver extends ParsePushBroadcastReceiver {

    private final String TAG = CustomPushReceiver.class.getSimpleName();

    private Intent parseIntent;

    public CustomPushReceiver() {
        super();
    }

    @Override
    protected void onPushReceive(Context context, Intent intent) {
        super.onPushReceive(context, intent);

        if (intent == null)
            return;

        try {
            JSONObject json = new JSONObject(intent.getExtras().getString("com.parse.Data"));

            Log.e(TAG, "Push received: " + json);

            parseIntent = intent;

            parsePushJson(context, json);

        } catch (JSONException e) {
            Log.e(TAG, "Push message json exception: " + e.getMessage());
        }
    }

    @Override
    protected void onPushDismiss(Context context, Intent intent) {
        super.onPushDismiss(context, intent);
    }

    @Override
    protected void onPushOpen(Context context, Intent intent) {
        super.onPushOpen(context, intent);
    }

    /**
     * Parses the push notification json
     *
     * @param context
     * @param json
     */
    @DebugLog
    private void parsePushJson(Context context, JSONObject json) {
        try {
            String message = json.getString("message");
            long senderId = json.getLong("senderId");
            String senderName = json.getString("senderName");
            long senderFbAndroidUId = json.getLong("senderFbAndroidUId");
            String statusId = json.getString("statusId");
            String operation = json.getString("operation");

            Intent resultIntent;
            if (statusId != null && !statusId.isEmpty()
                    && operation != null
                    && (operation.equals(SSConstants.USER_OPERATION_LIKE_STATUS)
                    || operation.equals(SSConstants.USER_OPERATION_FAVORITE_STATUS)
                    || operation.equals(SSConstants.USER_OPERATION_MENTION_USER))) {
                resultIntent = new Intent(context, StatusActivity.class);
                resultIntent.putExtra(IntentConstants.STATUS_ID, statusId);
            } else if (operation != null && operation.equals(SSConstants.USER_OPERATION_FOLLOW_USER)) {
                resultIntent = new Intent(context, UserActivity.class);
                resultIntent.putExtra(IntentConstants.USER_ID, senderId);
            } else {
                resultIntent = new Intent(context, MainActivity.class);
            }
            showNotificationMessage(context, message, senderFbAndroidUId, resultIntent);

        } catch (JSONException e) {
            Log.e(TAG, "Push message json exception: " + e.getMessage());
        }
    }


    /**
     * Shows the notification message in the notification bar
     * If the app is in background, launches the app
     *
     * @param context
     * @param message
     * @param intent
     */
    @DebugLog
    private void showNotificationMessage(Context context, String message, long senderFbAndroidUId, Intent intent) {

        intent.putExtras(parseIntent.getExtras());
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        SSNotifUtils.showNotificationMessage(context, message, senderFbAndroidUId, intent);
    }
}
