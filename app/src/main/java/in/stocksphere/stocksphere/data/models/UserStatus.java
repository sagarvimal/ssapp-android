package in.stocksphere.stocksphere.data.models;

import java.util.ArrayList;

public class UserStatus extends SSBase {

    private long userId;

    private ArrayList<Status> statuses;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public ArrayList<Status> getStatuses() {
        return statuses;
    }

    public void setStatuses(ArrayList<Status> statuses) {
        this.statuses = statuses;
    }

    @Override
    public String toString() {
        return "UserStatus{" +
                "userId=" + userId +
                ", statuses=" + statuses +
                '}';
    }
}
