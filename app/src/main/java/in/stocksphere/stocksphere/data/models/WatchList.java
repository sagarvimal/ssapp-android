package in.stocksphere.stocksphere.data.models;

import java.util.ArrayList;

public class WatchList extends SSBase {
	
	private long userId;
	
	private ArrayList<Company> watchlist;

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public ArrayList<Company> getWatchlist() {
		return watchlist;
	}

	public void setWatchlist(ArrayList<Company> watchlist) {
		this.watchlist = watchlist;
	}
}
