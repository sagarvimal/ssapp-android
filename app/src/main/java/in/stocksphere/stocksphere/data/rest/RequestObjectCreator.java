package in.stocksphere.stocksphere.data.rest;

import com.google.gson.JsonObject;

/**
 * Created by vtiwari on 6/16/15.
 */
public class RequestObjectCreator {

    private static JsonObject getJsonObject() {
        return new JsonObject();
    }

    public static JsonObject getUserFollowerObject(long followeeId) {

        JsonObject obj = getJsonObject();
        obj.addProperty("followeeId", followeeId);

        return obj;
    }

    public static JsonObject getUserFollowingObject(long followerId) {

        JsonObject obj = getJsonObject();
        obj.addProperty("followerId", followerId);

        return obj;
    }

    public static JsonObject getUserTimelineObject(long userId) {

        JsonObject obj = getJsonObject();
        obj.addProperty("userId", userId);

        return obj;
    }

    public static JsonObject getUpdateStatusObject(long userId, String statusText, String sentiment, String replyOfId) {

        JsonObject obj = getJsonObject();
        obj.addProperty("userId", userId);
        obj.addProperty("statusText", statusText);
        obj.addProperty("sentiment", sentiment);
        obj.addProperty("replyOfId", replyOfId);
        obj.addProperty("hasAttachment", false);

        return obj;
    }

    public static JsonObject getUpdateStatusObject(long userId, String statusText, String sentiment) {

        return getUpdateStatusObject(userId, statusText, sentiment, null);
    }

    public static JsonObject getUserStatusUpdatesObject(long userId, long callingUserId) {

        JsonObject obj = getJsonObject();
        obj.addProperty("userId", userId);
        obj.addProperty("callingUserId", callingUserId);

        return obj;
    }

    public static JsonObject addUserObject(long fbAndroidUId, String name, String gender, String email, String dob) {

        JsonObject obj = getJsonObject();
        obj.addProperty("fbAndroidUId", fbAndroidUId);
        obj.addProperty("name", name);
        obj.addProperty("gender", gender);
        obj.addProperty("email", email);
        obj.addProperty("dob", dob);

        return obj;
    }

    public static JsonObject getFollowUserObject(long followeeId, long followerId, boolean isFollowing) {

        JsonObject obj = getJsonObject();
        obj.addProperty("followeeId", followeeId);
        obj.addProperty("followerId", followerId);
        obj.addProperty("isFollowing", isFollowing);

        return obj;
    }

    public static JsonObject getLikeStatusObject(String statusId, long userId, boolean isLike) {

        JsonObject obj = getJsonObject();
        obj.addProperty("statusId", statusId);
        obj.addProperty("userId", userId);
        obj.addProperty("isLike", isLike);

        return obj;
    }

    public static JsonObject getFavoriteStatusObject(String statusId, long userId, boolean isFavorite) {

        JsonObject obj = getJsonObject();
        obj.addProperty("statusId", statusId);
        obj.addProperty("userId", userId);
        obj.addProperty("isFavorite", isFavorite);

        return obj;
    }

    public static JsonObject getUpdateWatchlistObject(long userId, String ssSymbol, boolean isAdd
            , double minPrice, double maxPrice, boolean isPushNotifEnabled) {

        JsonObject obj = getJsonObject();
        obj.addProperty("userId", userId);
        obj.addProperty("ssSymbol", ssSymbol);
        obj.addProperty("isAdd", isAdd);
        obj.addProperty("minPrice", minPrice);
        obj.addProperty("maxPrice", maxPrice);
        obj.addProperty("isPushNotifEnabled", isPushNotifEnabled);
        obj.addProperty("isEmailNotifEnabled", false);

        return obj;
    }

    public static JsonObject getWatchlistObject(long userId) {

        JsonObject obj = getJsonObject();
        obj.addProperty("userId", userId);

        return obj;
    }

    public static JsonObject getFollowCompanyObject(long followerId, String ssSymbol, boolean isFollowing) {

        JsonObject obj = getJsonObject();
        obj.addProperty("followerId", followerId);
        obj.addProperty("ssSymbol", ssSymbol);
        obj.addProperty("isFollowing", isFollowing);

        return obj;
    }

    public static JsonObject getCompanyStatusUpdatesObject(String ssSymbol, long userId) {

        JsonObject obj = getJsonObject();
        obj.addProperty("userId", userId);
        obj.addProperty("ssSymbol", ssSymbol);

        return obj;
    }

    public static JsonObject getCompanyDetailsObject(String ssSymbol) {

        JsonObject obj = getJsonObject();
        obj.addProperty("ssSymbol", ssSymbol);

        return obj;
    }

    public static JsonObject getDeleteStatusObject(String statusId, long userId) {

        JsonObject obj = getJsonObject();
        obj.addProperty("userId", userId);
        obj.addProperty("statusId", statusId);

        return obj;
    }

    public static JsonObject updateUserObject(long userId, String userName, String aboutUser) {

        JsonObject obj = getJsonObject();
        obj.addProperty("userId", userId);
        obj.addProperty("userName", userName);
        obj.addProperty("aboutUser", aboutUser);

        return obj;
    }

    public static JsonObject getAllCompanyNamesObject() {
        return getJsonObject();
    }

    public static JsonObject getCompanySocialDetailsObject(String ssSymbol) {

        JsonObject obj = getJsonObject();
        obj.addProperty("ssSymbol", ssSymbol);

        return obj;
    }

    public static JsonObject getWatchingUsersObject(String ssSymbol) {

        JsonObject obj = getJsonObject();
        obj.addProperty("ssSymbol", ssSymbol);

        return obj;
    }

    public static JsonObject getStatusLikedUsersObject(String statusId) {

        JsonObject obj = getJsonObject();
        obj.addProperty("statusId", statusId);

        return obj;
    }

    public static JsonObject getStatusFavouritedUsersObject(String statusId) {

        JsonObject obj = getJsonObject();
        obj.addProperty("statusId", statusId);

        return obj;
    }
}
