package in.stocksphere.stocksphere.data.rest;

/**
 * Created by vtiwari on 9/14/15.
 */
public class SSAPIResources {

    //////////// API resource Path - start
    private static final String DOCUMENT_ROOT = "/ssapp-rest";

    public static final String PATH_GET_USER_BY_USER_ID = DOCUMENT_ROOT + "/user/get";

    public static final String PATH_GET_STATUS_BY_ID = DOCUMENT_ROOT + "/status/get/{id}";

    public static final String PATH_GET_USER_FOLLOWERS = DOCUMENT_ROOT + "/user/getfollowers";

    public static final String PATH_GET_USER_FOLLOWING = DOCUMENT_ROOT + "/user/getfollowingusers";

    public static final String PATH_GET_USER_TIMELINE = DOCUMENT_ROOT + "/status/timeline";

    public static final String PATH_UPDATE_STATUS = DOCUMENT_ROOT + "/status/update";

    public static final String PATH_GET_USER_STATUS_UPDATES = DOCUMENT_ROOT + "/status/get";

    public static final String PATH_ADD_USER = DOCUMENT_ROOT + "/user/add";

    public static final String PATH_FOLLOW_USER = DOCUMENT_ROOT + "/user/follow";

    public static final String PATH_LIKE_STATUS = DOCUMENT_ROOT + "/status/like";

    public static final String PATH_FAVORITE_STATUS = DOCUMENT_ROOT + "/status/favorite";

    public static final String PATH_UPDATE_WATCHLIST = DOCUMENT_ROOT + "/watchlist/update";

    public static final String PATH_GET_WATCHLIST = DOCUMENT_ROOT + "/watchlist/get";

    public static final String PATH_FOLLOW_COMPANY = DOCUMENT_ROOT + "/company/follow";

    public static final String PATH_GET_COMPANY_TIMELINE = DOCUMENT_ROOT + "/status/companytl";

    public static final String PATH_GET_COMPANY_DETAILS = DOCUMENT_ROOT + "/company/getdetails";

    public static final String PATH_DELETE_STATUS = DOCUMENT_ROOT + "/status/delete";

    public static final String PATH_UPDATE_USER = DOCUMENT_ROOT + "/user/update";

    public static final String PATH_GET_ALL_COMPANY_NAMES = DOCUMENT_ROOT + "/company/getallcompanynames";

    public static final String PATH_GET_COMPANY_QUOTE_FROM_G_F = "/finance/info";

    public static final String PATH_GET_USER_BY_USER_NAME = DOCUMENT_ROOT + "/user/getuser";

    public static final String PATH_GET_COMPANY_SOCIAL_DETAILS = DOCUMENT_ROOT + "/company/getsocialdetails";

    public static final String PATH_GET_WATCHING_USERS = DOCUMENT_ROOT + "/watchlist/watchingusers";

    public static final String PATH_STATUS_LIKED_USERS = DOCUMENT_ROOT + "/status/likedusers";

    public static final String PATH_STATUS_FAVOURITED_USERS = DOCUMENT_ROOT + "/status/favouritedusers";

    //////////// API resource Path - end
}
