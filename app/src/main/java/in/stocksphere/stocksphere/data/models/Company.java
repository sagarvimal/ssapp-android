package in.stocksphere.stocksphere.data.models;

public class Company extends SSBase {

	private String name;
	
	private String nseSymbol;
	
	private String ssSymbol;
	
	private String ListingDate;
	
	private double faceValue;
	
	private String isin;
	
	private String industry;
	
	private long issuedCapInShares;
	
	private double marketCapInr;
	
	private Quote nseQuote;

    private WatchlistNotification wlNotification;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNseSymbol() {
		return nseSymbol;
	}

	public void setNseSymbol(String nseSymbol) {
		this.nseSymbol = nseSymbol;
	}

	public String getSsSymbol() {
		return ssSymbol;
	}

	public void setSsSymbol(String ssSymbol) {
		this.ssSymbol = ssSymbol;
	}

	public String getListingDate() {
		return ListingDate;
	}

	public void setListingDate(String listingDate) {
		ListingDate = listingDate;
	}

	public double getFaceValue() {
		return faceValue;
	}

	public void setFaceValue(double faceValue) {
		this.faceValue = faceValue;
	}

	public String getIsin() {
		return isin;
	}

	public void setIsin(String isin) {
		this.isin = isin;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public long getIssuedCapInShares() {
		return issuedCapInShares;
	}

	public void setIssuedCapInShares(long issuedCapInShares) {
		this.issuedCapInShares = issuedCapInShares;
	}

	public double getMarketCapInr() {
		return marketCapInr;
	}

	public void setMarketCapInr(double marketCapInr) {
		this.marketCapInr = marketCapInr;
	}

	public Quote getNseQuote() {
		return nseQuote;
	}

	public void setNseQuote(Quote nseQuote) {
		this.nseQuote = nseQuote;
	}

    public WatchlistNotification getWlNotification() {
        return wlNotification;
    }

    public void setWlNotification(WatchlistNotification wlNotification) {
        this.wlNotification = wlNotification;
    }
}
