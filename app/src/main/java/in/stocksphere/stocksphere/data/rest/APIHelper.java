package in.stocksphere.stocksphere.data.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import in.stocksphere.stocksphere.data.common.SSConstants;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

/**
 * Created by vtiwari on 6/15/15.
 */
public class APIHelper {

    public static SSAPI getSsapi() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(getEndPoint())
                .setClient(new OkClient(getOkHttpClient()))
                .setConverter(new GsonConverter(getGsonInstance()))
                .build();

        SSAPI ssapi = restAdapter.create(SSAPI.class);

        return ssapi;
    }

    private static Gson getGsonInstance() {
        Gson gson = new GsonBuilder()
                //.setDateFormat(Constants.CLIENT_DATE_FORMAT)
                .create();

        return gson;
    }

    private static String getEndPoint() {
        if (SSConstants.PORT != null && !SSConstants.PORT.isEmpty()) {
            return SSConstants.HOST + SSConstants.COLON + SSConstants.PORT;
        } else {
            return SSConstants.HOST;
        }
    }

    private static String getGogoleEndPoint() {
        return SSConstants.GOOGLE_URL;
    }

    private static OkHttpClient getOkHttpClient() {
        int TIME_OUT = 120;
        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setConnectTimeout(TIME_OUT, TimeUnit.SECONDS);
        return okHttpClient;
    }

    public static SSAPI getSsapiGoogle() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(getGogoleEndPoint())
                .setClient(new OkClient(getOkHttpClient()))
                .setConverter(new StringJsonConverter())
                .build();

        SSAPI ssapi = restAdapter.create(SSAPI.class);

        return ssapi;
    }
}
