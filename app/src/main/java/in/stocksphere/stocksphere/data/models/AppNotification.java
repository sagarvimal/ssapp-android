package in.stocksphere.stocksphere.data.models;


import com.parse.ParseClassName;
import com.parse.ParseObject;

import java.io.Serializable;

@ParseClassName("SSNotification")
public class AppNotification extends ParseObject  implements Serializable{

    public Long getReceiverId() {
        return getLong("receiverId");
    }
    public void setReceiverId(Long value) {
        put("receiverId", value);
    }

    public Long getSenderId() {
        return getLong("senderId");
    }
    public void setSenderId(Long value) {
        put("senderId", value);
    }

    public String getOperation() {
        return getString("operation");
    }
    public void setOperation(String value) {
        put("operation", value);
    }

    public String getMessage() {
        return getString("message");
    }
    public void setMessage(String value) {
        put("message", value);
    }

    public Long getSenderFbAndroidUId() {
        return getLong("senderFbAndroidUId");
    }
    public void setSenderFbAndroidUId(Long value) {
        put("senderFbAndroidUId", value);
    }

    public String getStatusId() {
        return getString("statusId");
    }
    public void setStatusId(String value) {
        put("statusId", value);
    }
}
