package in.stocksphere.stocksphere.data.aspect;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.CLASS;

/**
 * Created by vtiwari on 9/6/15.
 */
@Target({METHOD, CONSTRUCTOR})
@Retention(CLASS)
public @interface DisplayOnScreen {
}
