package in.stocksphere.stocksphere.data.models;

/*
 * watch list notification details
 */
public class WatchlistNotification extends SSBase {

	private double minPrice;
	
	private double maxPrice;
	
	private boolean isPushNotifEnabled;
	
	private boolean isEmailNotifEnabled;

	public double getMinPrice() {
		return minPrice;
	}

	public void setMinPrice(double minPrice) {
		this.minPrice = minPrice;
	}

	public double getMaxPrice() {
		return maxPrice;
	}

	public void setMaxPrice(double maxPrice) {
		this.maxPrice = maxPrice;
	}

	public boolean getIsPushNotifEnabled() {
		return isPushNotifEnabled;
	}

	public void setIsPushNotifEnabled(boolean isPushNotifEnabled) {
		this.isPushNotifEnabled = isPushNotifEnabled;
	}

	public boolean getIsEmailNotifEnabled() {
		return isEmailNotifEnabled;
	}

	public void setIsEmailNotifEnabled(boolean isEmailNotifEnabled) {
		this.isEmailNotifEnabled = isEmailNotifEnabled;
	}
}
