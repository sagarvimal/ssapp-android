package in.stocksphere.stocksphere.data.jsoup;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

import hugo.weaving.DebugLog;
import in.stocksphere.stocksphere.data.fabric.FabricUtils;

/**
 * Created by vtiwari on 9/14/15.
 * <p/>
 * Retrieves the metadata of any url to show on status. It fetches image url, title and description
 * from open graph properties
 * <p/>
 * For more details >> http://ogp.me/
 */
public class MetadataRetriever {

    private static final String OG_IMAGE = "og:image";
    private static final String OG_TITLE = "og:title";
    private static final String OG_HOST_URL = "og:url";
    private static final String OG_DESCRIPTION = "og:description";
    private static final String TAG_META = "meta";
    private static final String CONTENT = "content";
    private static final String PROPERTY = "property";
    private static final String USER_AGENT = "Mozilla/5.0";
    private static final int TIME_OUT = 30000;

    @DebugLog
    public static OpenGraphMetadata getOpenGraphMetadata(String url) {

        OpenGraphMetadata openGraphMetadata = null;
        Document document = getJsoupDocument(url);
        if (document == null) {
            return null;
        }

        Elements metaTag = document.getElementsByTag(TAG_META);
        if (metaTag == null) {
            return null;
        }

        openGraphMetadata = new OpenGraphMetadata();

        for (Element og : metaTag) {

            if ((og.attr(PROPERTY)).equalsIgnoreCase(OG_IMAGE)) {
                openGraphMetadata.setImageURL(og.attr(CONTENT));
                continue;
            }

            if ((og.attr(PROPERTY)).equalsIgnoreCase(OG_TITLE)) {
                openGraphMetadata.setTitle(og.attr(CONTENT));
                continue;
            }

            if ((og.attr(PROPERTY)).equalsIgnoreCase(OG_HOST_URL)) {
                openGraphMetadata.setUrl(og.attr(CONTENT));
                continue;
            }

            if ((og.attr(PROPERTY)).equalsIgnoreCase(OG_DESCRIPTION)) {
                openGraphMetadata.setDescription(og.attr(CONTENT));
            }
        }

        return openGraphMetadata;
    }

    private static Document getJsoupDocument(String url) {

        Document document = null;
        try {
            document = Jsoup.connect(url).userAgent(USER_AGENT).timeout(TIME_OUT).get();
        } catch (IOException e) {
            e.printStackTrace();
            FabricUtils.logException(e);
        }

        return document;
    }
}
