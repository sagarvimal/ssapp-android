package in.stocksphere.stocksphere.data.common;

public class SSConstants {
	
	public static final String HOST ="http://stocksphere.in";//"http://localhost";

	public static final String PORT ="";//"80";//"8080";

	public static final int ERROR_CODE_DEFAULT = 9999;

    public static final String GOOGLE_URL ="http://google.com";

	public static final String COLON =":";

    public static final String CLIENT_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ"; //RFC 3339 complaint date time format

    public static final String PRESENTABLE_DATE_FORMAT = "hh:mm a '.' dd MMM yy";

    public static final String EMPTY_STRING = "";

    public static final String SENTIMENT_BULLISH = "BU";

    public static final String SENTIMENT_BEARISH = "BE";

    public static final String SENTIMENT_NEUTRAL = "NE";

    public static final String REG_EX_PHONE = "^\\+[9][1][-][\\d]{10}$";

    public static final String REG_EX_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    public static final String DOB_DATE_FORMAT = "dd-MM-yyyy";

    public static final String SIGN_UP_ACCOUNT_FB = "FB";

    public static final String USER_OPERATION_LIKE_STATUS = "LIKE_STATUS";

    public static final String USER_OPERATION_FAVORITE_STATUS = "FAVORITE_STATUS";

    public static final String USER_OPERATION_FOLLOW_USER = "FOLLOW_USER";

    public static final String USER_OPERATION_MENTION_USER = "MENTION_STATUS";

    public static final String NSE_DATE_FORMAT = "dd-MMM-yyyy";

    public static final String COMMA = ",";

    public static final String SYMBOL_INR = "\u20b9";

    public static final String SPACE = " ";

    public static final String AUTO_COMPLETE_SEPARATOR = " - ";

    public static final String SYMBOL_PERCENTAGE = "%";

    public static final String SYMBOL_OPEN_BRACKET = "(";

    public static final String SYMBOL_CLOSED_BRACKET = ")";

    public static final String AT_THE_RATE = "@";

    public static final String TIMELINE_OPERATION_LIKE = "LIKE";

    public static final String TIMELINE_OPERATION_REPLY = "REPLY";

    public static final String TIMELINE_OPERATION_FAVORITE = "FAVORITE";

    public static final String TIMELINE_OPERATION_MSG_LIKE = "liked";

    public static final String TIMELINE_OPERATION_MSG_REPLY = "replied";

    public static final String TIMELINE_OPERATION_MSG_USER_YOU = "You";

    public static final String TIMELINE_OPERATION_MSG_FAVOURITED = "favourited";

    public static final int STATUS_TEXT_CHAR_LIMIT = 160;

    public static final int STATUS_TEXT_CHAR_LIMIT_EDIT_BOX = 500;

    public static final String SENTIMENT_BULLISH_COMPLETE = "Bullish";

    public static final String SENTIMENT_BEARISH_COMPLETE = "Bearish";

    public static final int GREEN_DAO_MAX_FETCH_ROW_LIMIT = 300;

    public static final String HYPHEN = "-";

    public static final long DEFAULT_USER_ID = -1;

    public static final String NEW_LINE = "\n";

    public static final boolean IS_GET_DATA_FROM_GOOGLE_FINANCE = true;

    public static final String NSE_STRING = "NSE";

    public static final String DATE_FORMAT_G_FINANCE = "yyyy MMM dd, hh:mma z";

    public static final String REG_EX_START_WITH_$ = "\\$(\\w+)";

    public static final String REG_EX_START_WITH_AT_THE_RATE = "\\@(\\w+)";

    public static final String NIFTY = "NIFTY";

    public static final String SENSEX = "SENSEX";

    public static final String DOLLAR = "$";

    public static final String SYMBOL_UP = "\u25B2";

    public static final String SYMBOL_DOWN = "\u25BC";

    public static final String QUERY_NIFTY = "NSE:NIFTY";

    public static final String QUERY_SENSEX = "INDEXBOM:SENSEX";

    public static final String POINT =".";

    public static final String TEXT_PLAIN = "text/plain";

    public static final String IST_TIME_ZONE_ID = "Asia/Kolkata";

    public static final String AMPERSAND = "&";

    public static final String AMPERSAND_URL_CODE ="%26";

    public static final double STOCK_NOTIF_PERCENTAGE = 2.6;

    public static final int STOCK_NOTIF_PRICE = 20;

    public static final int STOCK_SPHERE_USER_ID = 26;
}
