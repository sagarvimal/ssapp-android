package in.stocksphere.stocksphere.data.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by vtiwari on 10/4/15.
 */
public class TradingAppConstants {

    public static final String APP_ID_HDFC_SEC = "com.snapwork.hdfcsec";

    public static final String APP_ID_ZERODHA = "com.openstream.cueme.zerodha.mobiletrading";

    public static final String APP_ID_KOTAK = "com.msf.kotak";

    public static final String APP_ID_NSE = "NSE";

    public static final String APP_ID_ICICI_DIRECT = "com.icicisecurity";

    public static final String APP_ID_EDELWEISS = "com.msf.emt.mobile";

    public static final String APP_ID_MOTILAL_OSWAL = "com.msf.mosl.mobile";

    public static final String APP_ID_SHARE_KHAN = "com.snapwork.sharekhan";

    public static final String APP_ID_ANGEL_SWIFT = "air.com.angelbroking.angelswiftmobile";

    public static final String APP_ID_NIRMAL_BANG = "air.com.nirmalbang.nirmalbangmobile";

    public static final String APP_ID_AXIS_DIRECT_MOBILE = "com.axis.login";

    public static final String APP_ID_SBI_SMART = "com.sbi.trade";

    public static final String APP_ID_RELIGARE = "air.com.religare.iPhone";

    public static final String APP_ID_KARVY_ONLINE = "com.omnesys.KarvyOnlineTrading";

    public static final String APP_ID_HDFC_SEC_MULTILINGUAL = "com.snapwork.hdfcsecml";

    public static final String APP_ID_RKSV = "com.openstream.cueme.rksv.mobiletrading";

    public static final String APP_ID_ADITYA_BIRLA = "com.aditybirlamoney.cueme.mobileInvestPro";

    public static final List<String> TRADING_APPS_LIST = new ArrayList<>(Arrays.asList(APP_ID_HDFC_SEC,
            APP_ID_ZERODHA, APP_ID_KOTAK, APP_ID_NSE, APP_ID_ICICI_DIRECT, APP_ID_EDELWEISS, APP_ID_MOTILAL_OSWAL,
            APP_ID_SHARE_KHAN, APP_ID_ANGEL_SWIFT, APP_ID_NIRMAL_BANG, APP_ID_AXIS_DIRECT_MOBILE, APP_ID_SBI_SMART,
            APP_ID_RELIGARE, APP_ID_KARVY_ONLINE, APP_ID_HDFC_SEC_MULTILINGUAL, APP_ID_RKSV, APP_ID_ADITYA_BIRLA));
}
