package in.stocksphere.stocksphere.ui.fragment;


import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.analytics.Tracker;

import java.util.List;

import hugo.weaving.DebugLog;
import in.stocksphere.stocksphere.Logger;
import in.stocksphere.stocksphere.R;
import in.stocksphere.stocksphere.data.common.SSConstants;
import in.stocksphere.stocksphere.data.common.SSUtils;
import in.stocksphere.stocksphere.data.ganalytics.GAnalytics;
import in.stocksphere.stocksphere.data.ganalytics.GAnalyticsConstants;
import in.stocksphere.stocksphere.data.loaders.StatusLoader;
import in.stocksphere.stocksphere.data.models.Status;
import in.stocksphere.stocksphere.data.models.UserStatus;
import in.stocksphere.stocksphere.data.rest.APIHelper;
import in.stocksphere.stocksphere.data.rest.RequestObjectCreator;
import in.stocksphere.stocksphere.data.sqlite.GreenDaoUtils;
import in.stocksphere.stocksphere.ui.SharedPreferencesManager;
import in.stocksphere.stocksphere.ui.custom.SSAdapter;
import in.stocksphere.stocksphere.ui.custom.SSSwipeRefreshLayout;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewsFragment extends android.support.v4.app.Fragment {

    private List<Status> tStatusData;
    private Context context;
    private SSSwipeRefreshLayout mSwipeRefreshLayout = null;
    long USER_ID;
    private RecyclerView mRecyclerView;
    private Activity activity;
    private TextView tlNoDataView;
    private Tracker mTracker;
    private static final String TAG = NewsFragment.class.getSimpleName();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fragRootView = inflater.inflate(R.layout.fragment_news, container, false);
        context = getActivity();
        USER_ID = SharedPreferencesManager.getCurrentlyLoggedInUserId(this.getActivity());
        activity = this.getActivity();

        tlNoDataView = (TextView) fragRootView.findViewById(R.id.tlNoDataView);

        mTracker = GAnalytics.getTracker(getActivity().getApplication());

        mRecyclerView = (RecyclerView) fragRootView.findViewById(R.id.newsStatusView);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        mRecyclerView.setLayoutManager(mLayoutManager);

        //swipe layout
        mSwipeRefreshLayout = (SSSwipeRefreshLayout) fragRootView.findViewById(R.id.newsSwipeLayout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                mSwipeRefreshLayout.setRefreshing(true);
                requestNews();
                GAnalytics.sendEvent(mTracker, TAG, GAnalyticsConstants.SWIPE_REFRESH, GAnalyticsConstants.NEWS_INFO, USER_ID);
            }
        });

        loadDataFromSQLite(savedInstanceState);
        showRefreshOnUIThread();
        requestNews();

        return fragRootView;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onStop() {
        Logger.log("");
        super.onStop();
        dismissLoadingOnUIThread();
    }

    @Override
    public void onPause() {
        super.onPause();
        dismissLoadingOnUIThread();
    }

    @Override
    @DebugLog
    public void onStart() {
        super.onStart();
        GAnalytics.sendScreenView(mTracker, TAG);
    }

    @Override
    @DebugLog
    public void onResume() {
        super.onResume();
        GAnalytics.sendScreenView(mTracker, TAG);
    }

    @DebugLog
    void showRefreshOnUIThread() {
        mSwipeRefreshLayout.post(new Runnable() {
            public void run() {
                mSwipeRefreshLayout.setRefreshing(true);
            }
        });
    }

    @DebugLog
    private void requestNews() {
        Logger.log("");

        if (!SSUtils.isNetworkAvailable(activity)) {
            SSUtils.displayNoNetworkMsg(activity);
            dismissLoadingOnUIThread();
            return;
        }

        APIHelper.getSsapi().getUserStatusUpdates(RequestObjectCreator.getUserStatusUpdatesObject(SSConstants.STOCK_SPHERE_USER_ID, USER_ID), new Callback<UserStatus>() {
            @Override
            public void success(final UserStatus userStatus, Response response) {
                Logger.log("In Success..");
                dismissLoadingOnUIThread();

                if (userStatus == null || userStatus.getStatuses() == null || userStatus.getStatuses().isEmpty()) {
                    return;
                }

                //in background thread
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        // store and populate data
                        GreenDaoUtils.addToUserTimeline(context, userStatus.getStatuses(), SSConstants.STOCK_SPHERE_USER_ID);

                        // load data only if fragment is attached to activity
                        if (isAdded() && activity != null) {
                            loadDataFromSQLite(null);
                        }
                    }
                }).start();
            }

            @Override
            public void failure(RetrofitError error) {
                Logger.log("UserStatus is null :> " + error);
                dismissLoadingOnUIThread();
            }
        });
    }

    @DebugLog
    void populateNewsOnUIThread() {
        /// Execute the UI stuff on the main thread
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            public void run() {
                populateNews();
            }
        });
    }

    void dismissLoadingOnUIThread() {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            public void run() {
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @DebugLog
    private void populateNews() {
        Logger.log("");
        if (null == tStatusData || tStatusData.isEmpty()) {
            Logger.log("WARNING :: tStatusData empty !!");
            tlNoDataView.setVisibility(View.VISIBLE);
            return;
        }
        tlNoDataView.setVisibility(View.GONE);
        SSAdapter adapter = new SSAdapter(activity, R.layout.status_row_item, tStatusData, mTracker);
        mRecyclerView.setAdapter(adapter);
    }

    private void loadDataFromSQLite(Bundle savedInstanceState) {
        //savedInstanceState can be null if no loading required from saved bundle
        getLoaderManager().initLoader(0, savedInstanceState,
                new LoaderManager.LoaderCallbacks<List<Status>>() {
                    @Override
                    @DebugLog
                    public Loader<List<Status>> onCreateLoader(int id, Bundle args) {
                        return new StatusLoader(context, StatusLoader.NEWS_INFO);
                    }

                    @Override
                    @DebugLog
                    public void onLoadFinished(Loader<List<Status>> loader, List<Status> data) {
                        tStatusData = data;
                        populateNewsOnUIThread();
                    }

                    @Override
                    @DebugLog
                    public void onLoaderReset(Loader<List<Status>> loader) {
                        //NA
                    }
                }).forceLoad();
    }
}
