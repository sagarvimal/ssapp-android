package in.stocksphere.stocksphere.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.Tracker;

import java.util.List;

import hugo.weaving.DebugLog;
import in.stocksphere.stocksphere.Logger;
import in.stocksphere.stocksphere.R;
import in.stocksphere.stocksphere.data.aspect.DisplayOnScreen;
import in.stocksphere.stocksphere.data.common.SSConstants;
import in.stocksphere.stocksphere.data.common.SSUtils;
import in.stocksphere.stocksphere.data.fabric.FabricUtils;
import in.stocksphere.stocksphere.data.ftue.FTUEDisplayer;
import in.stocksphere.stocksphere.data.ganalytics.GAnalytics;
import in.stocksphere.stocksphere.data.ganalytics.GAnalyticsConstants;
import in.stocksphere.stocksphere.data.models.Company;
import in.stocksphere.stocksphere.data.models.CompanySocialDetails;
import in.stocksphere.stocksphere.data.models.CompanyStatus;
import in.stocksphere.stocksphere.data.models.GFGetInfo;
import in.stocksphere.stocksphere.data.models.Quote;
import in.stocksphere.stocksphere.data.models.Status;
import in.stocksphere.stocksphere.data.rest.APIHelper;
import in.stocksphere.stocksphere.data.rest.RequestObjectCreator;
import in.stocksphere.stocksphere.data.sqlite.GreenDaoUtils;
import in.stocksphere.stocksphere.ui.custom.IntentConstants;
import in.stocksphere.stocksphere.ui.custom.SSAdapter;
import in.stocksphere.stocksphere.ui.custom.SSProgressViewer;
import in.stocksphere.stocksphere.ui.custom.SSSwipeRefreshLayout;
import in.stocksphere.stocksphere.ui.fragment.WatchlistOptionsDialogFragment;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class CompanyActivity extends ActionBarActivity {

    public Company companyData;
    private SSProgressViewer progressViewer;
    private List<Status> tStatusData;
    private long USER_ID;
    private RecyclerView mRecyclerView;
    private SSSwipeRefreshLayout mSwipeRefreshLayout = null;
    private Context context;
    private String ssSymbol;
    private TextView cNoDataView;
    private long watchingUserCount;
    private Button cWatching;
    private Tracker mTracker;
    private static final String TAG = CompanyActivity.class.getSimpleName();
    private Activity activity;
    private ImageView cWlNotification;
    Button cSentiment;
    Button cMsgVol;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company);
        USER_ID = SharedPreferencesManager.getCurrentlyLoggedInUserId(this);
        context = this;
        activity = this;

        cNoDataView = (TextView)this.findViewById(R.id.cNoDataView);
        cWlNotification = (ImageView)this.findViewById(R.id.cWlNotification);
        cMsgVol = (Button) findViewById(R.id.cMsgVol);
        cWatching = (Button) findViewById(R.id.cWatching);
        cSentiment = (Button) findViewById(R.id.cSentiment);

        mTracker = GAnalytics.getTracker(getApplication());

        mRecyclerView = (RecyclerView) this.findViewById(R.id.cTimeline);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        //swipe layout
        mSwipeRefreshLayout = (SSSwipeRefreshLayout) this.findViewById(R.id.cSwipeLayout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                mSwipeRefreshLayout.setRefreshing(true);
                requestCompanyStatusUpdates(ssSymbol);
                GAnalytics.sendEvent(mTracker, TAG, GAnalyticsConstants.SWIPE_REFRESH, ssSymbol, USER_ID);
            }
        });

        Intent intent = getIntent();
        ssSymbol = intent.getStringExtra(IntentConstants.COMPANY_SS_SYMBOL);
        progressViewer = new SSProgressViewer();

        cWatching = (Button) findViewById(R.id.cWatching);
        cWatching.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewWatchingUsers();
            }
        });

        if (ssSymbol != null) {
            requestCompanyDetails(ssSymbol);
            getCompanySocialDetails(ssSymbol);
            requestCompanyStatusUpdates(ssSymbol);
        }

        showFTUE();
    }

    @Override
    public void onResume() {
        super.onResume();
        Logger.log("");

        ///populate data
        populateCompanyData(false);
        GAnalytics.sendScreenView(mTracker, TAG);
    }

    @Override
    @DebugLog
    public void onStart() {
        super.onStart();
        GAnalytics.sendScreenView(mTracker, TAG);
    }

    @DisplayOnScreen
    private void populateCompanyData(boolean isFetchedFromServer) {
        Logger.log("");

        if (null == companyData) {
            Logger.log("WARNING : companyData null !");
            return;
        }

        LinearLayout cLinearLayout = (LinearLayout) findViewById(R.id.cLinearLayout);
        TextView cName = (TextView) findViewById(R.id.cName);
        TextView cSSymbol = (TextView) findViewById(R.id.cSsymbol);

        cLinearLayout.setVisibility(View.VISIBLE);
        ///setting text details
        cName.setText(companyData.getName());
        cSSymbol.setText(companyData.getSsSymbol());

        ///setting appropriate background
        RelativeLayout cDetailsLayout = (RelativeLayout) findViewById(R.id.cDetailsLayout);
        LinearLayout cSocialLayout = (LinearLayout) findViewById(R.id.cSocialLayout);
        LinearLayout cQuoteLayout = (LinearLayout) findViewById(R.id.cQuoteLayout);

        int cDetailsLayoutBgColor = this.getResources().getColor(R.color.red);
        int cSocialLayoutBgColor = this.getResources().getColor(R.color.red_dark);
        if (companyData.getNseQuote().getPercentageChange() > 0) {
            cDetailsLayoutBgColor = this.getResources().getColor(R.color.green);
            cSocialLayoutBgColor = this.getResources().getColor(R.color.green_dark);
        }
        cDetailsLayout.setBackgroundColor(cDetailsLayoutBgColor);
        cSocialLayout.setBackgroundColor(cSocialLayoutBgColor);
        cQuoteLayout.setBackgroundColor(cDetailsLayoutBgColor);

        if (isFetchedFromServer) {
            if (companyData.getNseQuote() == null) {
                return;
            }
            TextView cLastMarketPrice = (TextView) findViewById(R.id.cLastMarketPrice);
            TextView cChange = (TextView) findViewById(R.id.cChange);
            TextView cPChange = (TextView) findViewById(R.id.cPChange);
            TextView cTime = (TextView) findViewById(R.id.cTime);


            Quote quote = companyData.getNseQuote();

            cLastMarketPrice.setText(SSConstants.SYMBOL_INR + SSConstants.SPACE + String.valueOf(quote.getLastMarketPrice()));
            cChange.setText(SSConstants.SYMBOL_INR + SSConstants.SPACE + String.valueOf(quote.getChangePrice()));
            cPChange.setText(SSConstants.SYMBOL_OPEN_BRACKET + String.valueOf(quote.getPercentageChange())
                    + SSConstants.SYMBOL_PERCENTAGE + SSConstants.SYMBOL_CLOSED_BRACKET);
            cTime.setText(SSUtils.getPresentableDate(quote.getLastPriceUpdateDate()));

            //populate quote
            Button cHigh = (Button) findViewById(R.id.cHigh);
            Button cLow = (Button) findViewById(R.id.cLow);
            Button cVolume = (Button) findViewById(R.id.cVolume);

            cHigh.setText("HIGH" + SSConstants.COLON+ SSConstants.SPACE + SSConstants.SYMBOL_INR + SSConstants.SPACE + String.valueOf(quote.getHighPrice()));
            cLow.setText("LOW" + SSConstants.COLON+ SSConstants.SPACE + SSConstants.SYMBOL_INR + SSConstants.SPACE + String.valueOf(quote.getLowPrice()));
            cVolume.setText("VOLUME" + SSConstants.COLON+ SSConstants.SPACE + String.valueOf((int)quote.getTradedVolume()));

            new Thread(new Runnable() {
                @Override
                public void run() {
                    tStatusData = GreenDaoUtils.getCompanyTimeLineStatus(context, companyData.getSsSymbol());
                    populateCompanyTLOnUIThread();
                }
            }).start();
        }
    }

    /**
     * Called when the user clicks the Options button
     */
    public void moreActionOptions(View view) {
        Logger.log("");

        Bundle bundle = new Bundle();
        bundle.putString(IntentConstants.COMPANY_SS_SYMBOL, ssSymbol);
        bundle.putLong(IntentConstants.USER_ID, USER_ID);

        WatchlistOptionsDialogFragment moreOptionsDialog = new WatchlistOptionsDialogFragment();
        moreOptionsDialog.setArguments(bundle);

        moreOptionsDialog.show(getSupportFragmentManager(), "tagWatchlistActions");
    }

    public void goBack(View view) {
        finish();
    }

    public void showPostDialog(View view) {

        Intent intent = new Intent(context, StatusUpdateActivity.class);
        intent.putExtra(IntentConstants.MENTION_SS_SYMBOL, ssSymbol);
        context.startActivity(intent);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Logger.log("");
        dismissLoadingOnUIThread();
    }

    @Override
    public void onPause() {
        super.onPause();
        dismissLoadingOnUIThread();
    }

    @DebugLog
    @DisplayOnScreen
    private void requestCompanyDetails(String ssSymbol) {

        Logger.log("");
        progressViewer.showLoading(getResources().getString(R.string.loading_message), this);

        //check if internet connected
        if (!SSUtils.isNetworkAvailable(this)) {

            SSUtils.displayNoNetworkMsg(this);
            finish();
            return;
        }

        APIHelper.getSsapi().getCompanyDetails(RequestObjectCreator.getCompanyDetailsObject(ssSymbol), new Callback<Company>() {
            @Override
            public void success(Company company, Response response) {

                if(company == null){
                    Logger.log("Company null in requestCompanyDetails");
                    dismissLoadingOnUIThread();
                    Toast.makeText(context, "Unable to get details right now", Toast.LENGTH_SHORT).show();
                    finish();
                }

                Logger.log("Success requestCompanyDetails");
                companyData = company;

                if (SSUtils.isFetchDataFromGF()) {
                    fetchQuoteFromGF();
                } else {
                    populateCompanyDetailsOnUIThread();
                }
            }

            @Override
            public void failure(RetrofitError error) {

                Logger.log("Failed requestCompanyDetails");
                dismissLoadingOnUIThread();
                Toast.makeText(context, "Details not available", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

    void dismissLoadingOnUIThread() {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            public void run() {
                progressViewer.dismissLoading();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @DebugLog
    void populateCompanyDetailsOnUIThread() {
        Logger.log("");
        /// Execute the UI stuff on the main thread
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            public void run() {

                populateCompanyData(true);
                /// remove loading
                dismissLoadingOnUIThread();
            }
        });
    }

    @DebugLog
    @DisplayOnScreen
    private void requestCompanyStatusUpdates(final String ssSymbol) {
        //check if internet connected
        if (!SSUtils.isNetworkAvailable(this)) {

            SSUtils.displayNoNetworkMsg(this);
            return;
        }

        APIHelper.getSsapi().getCompanyTimeline(RequestObjectCreator.getCompanyStatusUpdatesObject(ssSymbol, USER_ID), new Callback<CompanyStatus>() {
            @Override
            public void success(CompanyStatus companyStatus, Response response) {

                Logger.log("Success requestCompanyStatusUpdates");
                if (companyStatus == null || companyStatus.getStatuses() == null) {
                    Logger.log("companyStatus or companyStatus.getStatuses() is null");
                }else {
                    GreenDaoUtils.addToCompanyTimeline(context, companyStatus.getStatuses(), ssSymbol);
                }

                //populate company timeline
                populateCompanyDetailsOnUIThread();
            }

            @Override
            public void failure(RetrofitError error) {

                Logger.log("Failed requestCompanyStatusUpdates");
                dismissLoadingOnUIThread();
            }
        });
    }

    private void populateCompanyTLOnUIThread() {
        Logger.log("");
        /// Execute the UI stuff on the main thread
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            public void run() {
                /// populate the data fetched
                populateCompanyTimeline();
            }
        });
    }

    @DebugLog
    private void populateCompanyTimeline() {
        Logger.log("");
        dismissLoadingOnUIThread();
        /// populate company timeline
        if (null == tStatusData || tStatusData.isEmpty()) {
            Logger.log("WARNING :: tStatusData empty !!");
            cNoDataView.setVisibility(View.VISIBLE);
            return;
        }
        cNoDataView.setVisibility(View.GONE);
        SSAdapter adapter = new SSAdapter(this, R.layout.status_row_item, tStatusData, mTracker);
        mRecyclerView.setAdapter(adapter);
    }

    // update google finance watchlist values to local watchlist before populate to UI
    @DebugLog
    private void updateGFWatchlistValues(List<GFGetInfo> gfWatchlist) {

        GFGetInfo gfGetInfo = gfWatchlist.get(0);

        // if last trading price is not null or empty
        if (gfGetInfo.getL() != null && !gfGetInfo.getL().isEmpty()) {

            SSUtils.populateCompanyDetailsFromGF(companyData, gfGetInfo);
        }
    }

    @DebugLog
    void fetchQuoteFromGF() {

        if (companyData == null) {
            return;
        }

        String q = SSConstants.NSE_STRING + SSConstants.COLON + companyData.getNseSymbol();
        APIHelper.getSsapiGoogle().getStockQuoteGF(q, new Callback<List<GFGetInfo>>() {
            @Override
            public void success(final List<GFGetInfo> list, Response response) {

                if (list == null || list.isEmpty()) {
                    dismissLoadingOnUIThread();
                    return;
                }
                //in background thread
                new Thread(new Runnable() {
                    @Override
                    public void run() {

                        //update google finance values
                        updateGFWatchlistValues(list);
                        populateCompanyDetailsOnUIThread();
                    }
                }).start();
            }

            @Override
            public void failure(RetrofitError error) {
                FabricUtils.logException(error);
                dismissLoadingOnUIThread();
            }
        });
    }

    @DebugLog
    private void getCompanySocialDetails(String ssSymbol){

        if(ssSymbol == null || ssSymbol.isEmpty() || !SSUtils.isNetworkAvailable(context)){
            return;
        }

        APIHelper.getSsapi().getCompanySocialDetails(RequestObjectCreator.getCompanySocialDetailsObject(ssSymbol), new Callback<CompanySocialDetails>() {
            @Override
            public void success(CompanySocialDetails companySocialDetails, Response response) {

                populateCompanySocialDetailsOnUIThread(companySocialDetails);
            }

            @Override
            public void failure(RetrofitError error) {
                FabricUtils.logException(error);
            }
        });
    }

    @DebugLog
    void populateCompanySocialDetailsOnUIThread(final CompanySocialDetails companySocialDetails) {
        /// Execute the UI stuff on the main thread
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            public void run() {
                cMsgVol.setText("Updates (" + String.valueOf(companySocialDetails.getStatusCount()) + ")");
                cWatching.setText("Watching (" + String.valueOf(companySocialDetails.getWatchingUserCount()) + ")");

                watchingUserCount = companySocialDetails.getWatchingUserCount();

                if (companySocialDetails.getSentimentPositive() > 0 || companySocialDetails.getSentimentNegative() > 0) {
                    if (companySocialDetails.getSentimentPositive() >= companySocialDetails.getSentimentNegative()) {
                        cSentiment.setText("Sentiment (" + SSConstants.SYMBOL_UP + SSConstants.SPACE + String.valueOf(companySocialDetails.getSentimentPositive()) + SSConstants.SYMBOL_PERCENTAGE + ")");
                    } else {
                        cSentiment.setText("Sentiment (" + SSConstants.SYMBOL_DOWN + SSConstants.SPACE + String.valueOf(companySocialDetails.getSentimentNegative()) + SSConstants.SYMBOL_PERCENTAGE + ")");
                    }
                }
            }
        });
    }

    private void  viewWatchingUsers(){

        if(watchingUserCount <= 0){
            Toast.makeText(context, "No one is watching this company",Toast.LENGTH_SHORT).show();
        }

        Intent intent = new Intent(context, UserListActivity.class);
        intent.putExtra(IntentConstants.OPERATION, IntentConstants.SHOW_WATCHING_USERS);
        intent.putExtra(IntentConstants.COMPANY_SS_SYMBOL, ssSymbol);
        context.startActivity(intent);
    }

    private void showFTUE() {
        FTUEDisplayer.showFTUEOnCompanyScreen(activity, cWlNotification, cWatching);
    }
}
