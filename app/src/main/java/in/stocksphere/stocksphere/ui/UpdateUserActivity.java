package in.stocksphere.stocksphere.ui;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.Tracker;

import hugo.weaving.DebugLog;
import in.stocksphere.stocksphere.Logger;
import in.stocksphere.stocksphere.R;
import in.stocksphere.stocksphere.data.common.SSConstants;
import in.stocksphere.stocksphere.data.common.SSUtils;
import in.stocksphere.stocksphere.data.ganalytics.GAnalytics;
import in.stocksphere.stocksphere.data.ganalytics.GAnalyticsConstants;
import in.stocksphere.stocksphere.data.models.User;
import in.stocksphere.stocksphere.data.rest.APIHelper;
import in.stocksphere.stocksphere.data.rest.RequestObjectCreator;
import in.stocksphere.stocksphere.ui.custom.SSProgressViewer;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class UpdateUserActivity extends ActionBarActivity {

    private EditText uuName;
    private EditText uuUserName;
    private EditText uuAboutUser;
    private TextView uuErrorMsgView;
    private SSProgressViewer progressViewer;
    private Context context;
    private User user;
    private Toolbar toolbar;
    private Tracker mTracker;
    private static final String TAG = UpdateUserActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_user);
        uuName = (EditText) this.findViewById(R.id.uuName);
        uuUserName = (EditText) this.findViewById(R.id.uuUserName);
        uuAboutUser = (EditText) this.findViewById(R.id.uuAboutUser);
        uuErrorMsgView = (TextView) this.findViewById(R.id.uuErrorMsgView);
        context = this;

        mTracker = GAnalytics.getTracker(getApplication());

        // Set a Toolbar to replace the ActionBar.
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_back));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        progressViewer = new SSProgressViewer();
        user = SharedPreferencesManager.getCurrentlyLoggedInUser(this);
        populateUserDetails();

        Button uuCancel = (Button) this.findViewById(R.id.uuCancel);
        uuCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLastActivity();
                GAnalytics.sendEvent(mTracker, TAG, GAnalyticsConstants.CANCEL, SSConstants.EMPTY_STRING, user.getUserId());
            }
        });

        Button uuUpdate = (Button) this.findViewById(R.id.uuUpdate);
        uuUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateUserDetails();
                GAnalytics.sendEvent(mTracker, TAG, GAnalyticsConstants.PROFILE_UPDATE, SSConstants.EMPTY_STRING, user.getUserId());
            }
        });
    }

    private void populateUserDetails() {

        if (user == null) {
            return;
        }

        uuName.setText(user.getName());
        uuUserName.setText(user.getUserName());
        uuAboutUser.setText(user.getAboutUser());
    }

    @Override
    public void onResume() {
        super.onResume();
        Logger.log("");
        GAnalytics.sendScreenView(mTracker, TAG);
    }

    @Override
    @DebugLog
    public void onStart() {
        super.onStart();
        GAnalytics.sendScreenView(mTracker, TAG);
    }

    private void updateUserDetails(){

        if(uuUserName.getText().length() <=0 ){
            Toast.makeText(context, "User name should not be blank", Toast.LENGTH_SHORT).show();
            return;
        }

        showProgressDialogOnUI();
        APIHelper.getSsapi().updateUser(RequestObjectCreator.updateUserObject(user.getUserId()
                , uuUserName.getText().toString(), uuAboutUser.getText().toString()), new Callback<User>() {
            @Override
            public void success(User user, Response response) {

                hideProgressDialogOnUI();
                SharedPreferencesManager.setCurrentlyLoggedInUser(context, user);
                showLastActivity();
                SSUtils.SendShowSnackBarMsgBroadcast(context, "Profile updated successfully");
            }

            @Override
            public void failure(RetrofitError error) {
                uuErrorMsgView.setVisibility(View.VISIBLE);
                hideProgressDialogOnUI();
            }
        });
    }

    private void showLastActivity(){
        finish();
    }

    @DebugLog
    void showProgressDialogOnUI() {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            public void run() {
                progressViewer.showLoading("Updating...",context);
            }
        });
    }

    @DebugLog
    void hideProgressDialogOnUI() {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            public void run() {
                progressViewer.dismissLoading();
            }
        });
    }
}
