package in.stocksphere.stocksphere.ui.custom;

/**
 * Created by vtiwari on 7/11/15.
 */
public class IntentConstants {

    public static final String COMPANY_WATCHLIST_NOTIF = "CompanyWatchlistNotif";

    public static final String COMPANY_SS_SYMBOL = "CompanySSSymbol";

    public static final String USER_ID = "USER_ID";

    public static final String IS_ADDED_TO_WATCHLIST = "IsAddedToWatchList";

    public static final String SHOW_FOLLOWERS = "SHOW_FOLLOWERS";

    public static final String OPERATION = "OPERATION";

    public static final String SHOW_FOLLOWING = "SHOW_FOLLOWING";

    public static final String REPLY_OF_ID = "replyOfId";

    public static final String MENTION_HANDLE = "mentionHandle";

    public static final String MENTION_SS_SYMBOL = "mentionSSSymbol";

    public static final String SHOW_WATCHING_USERS = "SHOW_WATCHING_USERS";

    public static final String SHOW_STATUS_LIKED_USERS = "SHOW_STATUS_LIKED_USERS";

    public static final String SHOW_STATUS_FAVOURITED_USERS = "SHOW_STATUS_FAVOURITED_USERS";

    public static final String STATUS_ID = "STATUS_ID";

    public static final String SNACK_BAR_MESSAGE = "SNACK_BAR_MESSAGE";

    public static final String INTENT_ACTION_SHOW_SNACK_BAR = "in.stocksphere.stocksphere.SHOW_SNACK_BAR";

    public static final String SHARED_STATUS_TEXT = "SHARED_STATUS_TEXT";
}
