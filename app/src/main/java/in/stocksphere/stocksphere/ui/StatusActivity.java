package in.stocksphere.stocksphere.ui;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.text.util.Linkify;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.Tracker;
import com.squareup.picasso.Picasso;

import java.util.List;

import hugo.weaving.DebugLog;
import in.stocksphere.stocksphere.Logger;
import in.stocksphere.stocksphere.R;
import in.stocksphere.stocksphere.data.common.SSConstants;
import in.stocksphere.stocksphere.data.common.SSUtils;
import in.stocksphere.stocksphere.data.fabric.FabricUtils;
import in.stocksphere.stocksphere.data.ganalytics.GAnalytics;
import in.stocksphere.stocksphere.data.jsoup.MetadataRetriever;
import in.stocksphere.stocksphere.data.jsoup.OpenGraphMetadata;
import in.stocksphere.stocksphere.data.models.Status;
import in.stocksphere.stocksphere.data.models.User;
import in.stocksphere.stocksphere.data.rest.APIHelper;
import in.stocksphere.stocksphere.data.sqlite.GreenDaoUtils;
import in.stocksphere.stocksphere.ui.custom.IntentConstants;
import in.stocksphere.stocksphere.ui.custom.SSAdapter;
import in.stocksphere.stocksphere.ui.custom.SSProgressViewer;
import in.stocksphere.stocksphere.ui.custom.StatusPostHelper;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class StatusActivity extends ActionBarActivity {

    private Status status = null;
    private List<Status> replyData = null;
    private StatusPostHelper postHelper = null;
    Activity activity = null;
    RecyclerView mRecyclerView;
    private Toolbar toolbar;
    private long USER_ID;
    private SSProgressViewer ssProgressViewer;
    private Tracker mTracker;
    private static final String TAG = StatusActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status);
        USER_ID = SharedPreferencesManager.getCurrentlyLoggedInUserId(this);
        activity = this;

        mTracker = GAnalytics.getTracker(getApplication());
        postHelper = new StatusPostHelper(mTracker);

        // Set a Toolbar to replace the ActionBar.
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_back));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mRecyclerView = (RecyclerView) this.findViewById(R.id.ssListView);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(activity);
        mRecyclerView.setLayoutManager(mLayoutManager);

        Intent intent = getIntent();
        String statusId = intent.getStringExtra(IntentConstants.STATUS_ID);

        ssProgressViewer = new SSProgressViewer();
        ssProgressViewer.showLoading(getResources().getString(R.string.loading_message), this);

        //request reply data from server
        if (statusId != null) {
            requestStatusAndReplyData(statusId);
        } else {
            finish();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Logger.log("");
        GAnalytics.sendScreenView(mTracker, TAG);
    }

    @Override
    @DebugLog
    public void onStart() {
        super.onStart();
        GAnalytics.sendScreenView(mTracker, TAG);
    }

    @DebugLog
    private void populateStatusData() {

        if (status == null) {
            return;
        }

        CardView statusCardView = (CardView) findViewById(R.id.statusCardView);
        TextView ssUserName = (TextView) findViewById(R.id.ssUserName);
        TextView ssUserHandle = (TextView) findViewById(R.id.ssUserHandle);
        TextView ssText = (TextView) findViewById(R.id.ssText);
        TextView ssDate = (TextView) findViewById(R.id.ssDate);
        TextView ssLikeCount = (TextView) findViewById(R.id.ssLikeCount);
        TextView ssFavCount = (TextView) findViewById(R.id.ssFavCount);
        TextView ssLikeText = (TextView) findViewById(R.id.ssLikeText);
        TextView ssFavText = (TextView) findViewById(R.id.ssFavText);
        Button ssFavoriteButton = (Button) findViewById(R.id.ssFavoriteButton);
        Button ssLikeButton = (Button) findViewById(R.id.ssLikeButton);
        Button ssDeleteButton = (Button) findViewById(R.id.ssDeleteButton);
        Button ssReplyButton = (Button) findViewById(R.id.ssReplyButton);
        ImageView ssUserPic = (ImageView) findViewById(R.id.ssUserPic);
        ImageView ssSentimentImg = (ImageView) findViewById(R.id.ssSentimentImg);
        RelativeLayout likeLayout = (RelativeLayout) findViewById(R.id.likeLayout);

        statusCardView.setVisibility(View.VISIBLE);

        Picasso.with(activity)
                .load(SSUtils.getFacebookProfilePicUrl(status.getUserFbAndroidUId()))
                .into(ssUserPic);
        ssUserName.setText(status.getUserFullName());
        ssUserHandle.setText(SSConstants.AT_THE_RATE + status.getUserName());
        this.createClickableTextView(ssText);
        ssDate.setText(SSUtils.getPresentableDate(status.getCreatedDate()));

        if (status.getcLikes() > 0 || status.getcFavorites() > 0) {
            likeLayout.setVisibility(View.VISIBLE);
        } else {
            likeLayout.setVisibility(View.GONE);
        }

        if (status.getcLikes() > 0) {
            ssLikeCount.setVisibility(View.VISIBLE);
            ssLikeText.setVisibility(View.VISIBLE);
            ssLikeCount.setText(String.valueOf(status.getcLikes()));
            if (status.getcLikes() > 1) {
                ssLikeText.setText("LIKES");
            } else {
                ssLikeText.setText("LIKE");
            }
        } else {
            ssLikeText.setText("LIKE");
            ssLikeCount.setVisibility(View.INVISIBLE);
            ssLikeText.setVisibility(View.INVISIBLE);
        }

        if (status.getcFavorites() > 0) {
            ssFavCount.setVisibility(View.VISIBLE);
            ssFavText.setVisibility(View.VISIBLE);
            ssFavCount.setText(String.valueOf(status.getcFavorites()));
            if (status.getcLikes() > 1) {
                ssFavText.setText("FAVOURITES");
            } else {
                ssFavText.setText("FAVOURITE");
            }
        } else {
            ssFavText.setText("FAVOURITE");
            ssFavCount.setVisibility(View.INVISIBLE);
            ssFavText.setVisibility(View.INVISIBLE);
        }

        if (status.getIsLiked()) {
            ssLikeButton.setBackgroundResource(R.drawable.like_done);
        } else {
            ssLikeButton.setBackgroundResource(R.drawable.like);
        }

        if (status.getIsFavourited()) {
            ssFavoriteButton.setBackgroundResource(R.drawable.favorite_done);
        } else {
            ssFavoriteButton.setBackgroundResource(R.drawable.favorite);
        }

        //hide delete button
        if (status.getUserId() != USER_ID) {
            ssDeleteButton.setVisibility(View.GONE);
        }

        //set sentiment image
        if (status.getSentiment().equals(SSConstants.SENTIMENT_BULLISH)) {
            ssSentimentImg.setBackgroundResource(R.drawable.bull_dark);
            ssSentimentImg.setVisibility(View.VISIBLE);
        } else if (status.getSentiment().equals(SSConstants.SENTIMENT_BEARISH)) {
            ssSentimentImg.setBackgroundResource(R.drawable.bear_dark);
            ssSentimentImg.setVisibility(View.VISIBLE);
        } else {
            ssSentimentImg.setVisibility(View.GONE);
        }

        //set onclick listeners
        ssLikeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postHelper.onLike(status, v, activity, false);
            }
        });

        ssFavoriteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postHelper.onFavorite(status, v, activity, false);
            }
        });

        ssReplyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postHelper.onReply(status, v, activity, false);
            }
        });

        ssDeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postHelper.onDelete(status, v, activity);
            }
        });
        ssUserPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postHelper.onDisplayPicClick(status.getUserId(), v, activity, false);
            }
        });

        //copy text to clip board on long press
        ssText.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                ClipData clipdata = ClipData.newPlainText("text", status.getStatusText() + "\n\n - Posted by "
                        + SSConstants.AT_THE_RATE + status.getUserName() + " on StockSphere");

                ClipboardManager clipboard = (ClipboardManager) activity.getSystemService(CLIPBOARD_SERVICE);
                clipboard.setPrimaryClip(clipdata);
                SSUtils.showSnackBarMsg(activity, "Status text copied to clipboard");
                return true;
            }
        });

        ssLikeText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewStatusLikedUsers();
            }
        });

        ssFavText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewStatusFavouritedUsers();
            }
        });
    }

    /*
 * Populate metadata from web page (url) mentioned in status text
 */
    public void populateWebPageMetadata(final URLSpan spans[]) {

        final CardView urlMetadataCardView = (CardView) findViewById(R.id.ssURLMetadataCardView);
        final ImageView urlMetadataImage = (ImageView) findViewById(R.id.ssUrlMetadataImage);
        final TextView urlMetadataTitle = (TextView) findViewById(R.id.ssUrlMetadataTitle);
        final TextView urlMetadataHost = (TextView) findViewById(R.id.ssUrlMetadataHost);

        //default visibility gone
        urlMetadataCardView.setVisibility(View.GONE);
        if (spans == null || spans.length == 0) {
            return;
        }

        class URLMetadataRetriever extends AsyncTask<String, Integer, OpenGraphMetadata> {
            protected OpenGraphMetadata doInBackground(String... urls) {
                OpenGraphMetadata openGraphMetadata = null;
                String url = urls[0];
                openGraphMetadata = GreenDaoUtils.getOpenGraphMetadata(activity, url);

                if (openGraphMetadata == null) {
                    openGraphMetadata = MetadataRetriever.getOpenGraphMetadata(url);
                    if (openGraphMetadata != null && openGraphMetadata.getImageURL() != null
                            && openGraphMetadata.getTitle() != null && openGraphMetadata.getUrl() != null) {
                        openGraphMetadata.setWebPageUrl(url);
                        GreenDaoUtils.updateOpenGraphMetadata(activity, openGraphMetadata);
                    } else {
                        openGraphMetadata = null;
                    }
                }
                return openGraphMetadata;
            }

            protected void onProgressUpdate(Integer... progress) {
            }

            @DebugLog
            protected void onPostExecute(final OpenGraphMetadata ogMetadata) {

                if (ogMetadata == null) {
                    return;
                }
                urlMetadataCardView.setVisibility(View.VISIBLE);
                Picasso.with(activity)
                        .load(ogMetadata.getImageURL()).into(urlMetadataImage);
                urlMetadataTitle.setText(ogMetadata.getTitle());
                urlMetadataHost.setText(SSUtils.getHost(ogMetadata.getUrl()));

                //set onclick listener
                urlMetadataCardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse(ogMetadata.getWebPageUrl()));
                        activity.startActivity(intent);
                    }
                });
            }
        }

        new URLMetadataRetriever().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, spans[0].getURL());
    }

    @DebugLog
    private void requestStatusAndReplyData(String statusId) {

        APIHelper.getSsapi().getStatusById(statusId, USER_ID, new Callback<Status>() {
            @Override
            public void success(Status status1, Response response) {
                Logger.log("Success requestReplyData");

                ssProgressViewer.dismissLoading();
                if (status1 == null) {
                    return;
                }

                //populate status data - used to update likes and favourite updates
                status = status1;
                populateStatusData();
                GreenDaoUtils.updateStatus(activity, status); //save status to db

                if (status1.getStatusReplies() != null && !status1.getStatusReplies().isEmpty()) {

                    //populate reply data
                    replyData = status.getStatusReplies();
                    populateRepliesOnUIThread();
                }
            }

            @Override
            public void failure(RetrofitError error) {

                ssProgressViewer.dismissLoading();
                Logger.log("Fail requestReplyData");
                FabricUtils.logException(error);
                Toast.makeText(activity, "Unable to load details. Please try again.", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

    void populateRepliesOnUIThread() {
        /// Execute the UI stuff on the main thread
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            public void run() {
                /// populate the data fetched
                populateTimeline();
            }
        });
    }

    private void populateTimeline() {
        Logger.log("");
        if (null == replyData) {
            Logger.log("WARNING :: replyData empty !!");
            return;
        }
        SSAdapter adapter = new SSAdapter(this,
                R.layout.status_row_item, replyData, mTracker);
        mRecyclerView.setAdapter(adapter);
    }

    private void createClickableTextView(TextView textView) {

        if (status == null || status.getStatusText() == null || status.getStatusText().isEmpty()) {
            return;
        }

        User thisUser = SharedPreferencesManager.getCurrentlyLoggedInUser(activity);
        String statusText = status.getStatusText();

        SpannableString ss = new SpannableString(statusText);
        String[] words = statusText.split(SSConstants.SPACE);
        for (final String word : words) {

            //do not add the word to SpannableString if it is current user name
            if (word.startsWith(SSConstants.AT_THE_RATE)
                    && word.trim().substring(1).equalsIgnoreCase(thisUser.getUserName())) {
                continue;
            }

            if (word.startsWith(SSConstants.AT_THE_RATE) || word.startsWith(SSConstants.DOLLAR)) {
                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void onClick(final View textView) {
                        if (word.startsWith(SSConstants.AT_THE_RATE)) {

                            String userName = word.trim().substring(1);
                            APIHelper.getSsapi().getUserByUserName(userName, new Callback<User>() {
                                @Override
                                public void success(User user, Response response) {
                                    new StatusPostHelper(mTracker).onDisplayPicClick(user.getUserId(), textView, activity, false);
                                }

                                @Override
                                public void failure(RetrofitError error) {
                                    Toast.makeText(activity, "User does not exist", Toast.LENGTH_SHORT).show();
                                    FabricUtils.logException(error);
                                }
                            });

                        } else {
                            Intent intent = new Intent(activity, CompanyActivity.class);
                            intent.putExtra(IntentConstants.COMPANY_SS_SYMBOL, word.trim().toUpperCase());
                            activity.startActivity(intent);
                        }
                    }

                    public void updateDrawState(TextPaint ds) {
                        ds.setUnderlineText(false); // set to false to remove underline
                        ds.setColor(getResources().getColor(R.color.primary));
                    }
                };
                ss.setSpan(clickableSpan, statusText.indexOf(word), statusText.indexOf(word) + word.length(),
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
        }

        textView.setText(ss);
        textView.setMovementMethod(LinkMovementMethod.getInstance());

        //Show all web urls in text as clickable
        Linkify.addLinks(textView, Linkify.WEB_URLS);
        populateWebPageMetadata(textView.getUrls());
    }

    private void viewStatusLikedUsers() {

        if (status == null || status.getcLikes() <= 0) {
            Toast.makeText(activity, "No likes", Toast.LENGTH_SHORT).show();
            return;
        }

        Intent intent = new Intent(activity, UserListActivity.class);
        intent.putExtra(IntentConstants.OPERATION, IntentConstants.SHOW_STATUS_LIKED_USERS);
        intent.putExtra(IntentConstants.STATUS_ID, status.getId());
        activity.startActivity(intent);
    }

    private void viewStatusFavouritedUsers() {

        if (status == null || status.getcFavorites() <= 0) {
            Toast.makeText(activity, "No favourites", Toast.LENGTH_SHORT).show();
            return;
        }

        Intent intent = new Intent(activity, UserListActivity.class);
        intent.putExtra(IntentConstants.OPERATION, IntentConstants.SHOW_STATUS_FAVOURITED_USERS);
        intent.putExtra(IntentConstants.STATUS_ID, status.getId());
        activity.startActivity(intent);
    }
}
