package in.stocksphere.stocksphere.ui;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.Tracker;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import hugo.weaving.DebugLog;
import in.stocksphere.stocksphere.Logger;
import in.stocksphere.stocksphere.R;
import in.stocksphere.stocksphere.data.common.SSConstants;
import in.stocksphere.stocksphere.data.common.SSNotifUtils;
import in.stocksphere.stocksphere.data.common.SSUtils;
import in.stocksphere.stocksphere.data.fabric.FabricUtils;
import in.stocksphere.stocksphere.data.ftue.FTUEDisplayer;
import in.stocksphere.stocksphere.data.ganalytics.GAnalytics;
import in.stocksphere.stocksphere.data.ganalytics.GAnalyticsConstants;
import in.stocksphere.stocksphere.data.models.Status;
import in.stocksphere.stocksphere.data.models.User;
import in.stocksphere.stocksphere.data.rest.APIHelper;
import in.stocksphere.stocksphere.data.rest.RequestObjectCreator;
import in.stocksphere.stocksphere.data.rest.SSAPI;
import in.stocksphere.stocksphere.data.sqlite.GreenDaoUtils;
import in.stocksphere.stocksphere.ui.custom.IntentConstants;
import in.stocksphere.stocksphere.ui.custom.SpaceTokenizer;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class StatusUpdateActivity extends ActionBarActivity {

    private static Context context;
    private static long USER_ID;
    private Activity activity;

    private String replyOfId = null;
    private MultiAutoCompleteTextView statusPostInput;
    private Tracker mTracker;
    private static final String TAG = StatusUpdateActivity.class.getSimpleName();
    private Button bullishBtn;
    private Button bearishBtn;
    private Button postBtn;
    private Button bDollarSymbol;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status_update);
        context = this;
        activity = this;
        USER_ID = SharedPreferencesManager.getCurrentlyLoggedInUserId(this);

        mTracker = GAnalytics.getTracker(getApplication());

        Intent intent = getIntent();
        replyOfId = intent.getStringExtra(IntentConstants.REPLY_OF_ID);
        String mentionHandle = intent.getStringExtra(IntentConstants.MENTION_HANDLE);
        String mentionSSSymbol = intent.getStringExtra(IntentConstants.MENTION_SS_SYMBOL);
        String sharedStatusText = intent.getStringExtra(IntentConstants.SHARED_STATUS_TEXT);

        Button cancelBtn = (Button) findViewById(R.id.bCancelPost);
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Logger.log("cancelBtn");
                onCancelClick();
                GAnalytics.sendEvent(mTracker, TAG, GAnalyticsConstants.CANCEL, SSConstants.EMPTY_STRING, USER_ID);
            }
        });

        postBtn = (Button) findViewById(R.id.bStatusPost);
        postBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //check internet
                if (SSUtils.isNetworkAvailable(context)) {
                    onPostStatusClick(replyOfId);
                } else {
                    SSUtils.displayNoNetworkMsg(context);
                }
                GAnalytics.sendEvent(mTracker, TAG, GAnalyticsConstants.STATUS_UPDATE, SSConstants.EMPTY_STRING, USER_ID);
            }
        });

        bullishBtn = (Button) findViewById(R.id.bBullish);
        bullishBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBullishClick(view);
                GAnalytics.sendEvent(mTracker, TAG, GAnalyticsConstants.CLICK_BULLISH_ICON, SSConstants.EMPTY_STRING, USER_ID);
            }
        });

        bearishBtn = (Button) findViewById(R.id.bBearish);
        bearishBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBearishClick(view);
                GAnalytics.sendEvent(mTracker, TAG, GAnalyticsConstants.CLICK_BEARISH_ICON, SSConstants.EMPTY_STRING, USER_ID);
            }
        });

        statusPostInput = (MultiAutoCompleteTextView) findViewById(R.id.StatusPostInput);
        populateAutocompleteSSSymbols(statusPostInput);
        //add text change listener for edit text
        statusPostInput.addTextChangedListener(new StatusTextWatcher(this));

        //If it is a reply to a post
        if (mentionSSSymbol != null || mentionHandle != null || sharedStatusText != null) {

            String text;

            if (mentionHandle != null) {
                text = SSConstants.AT_THE_RATE + mentionHandle + SSConstants.SPACE;
            } else if (sharedStatusText != null) {
                text = sharedStatusText;
            } else {
                text = mentionSSSymbol + SSConstants.SPACE;
            }

            // set text size to 160 if it is more than that
            if (text.length() > SSConstants.STATUS_TEXT_CHAR_LIMIT_EDIT_BOX) {
                text = text.substring(0, SSConstants.STATUS_TEXT_CHAR_LIMIT_EDIT_BOX);
            }

            //sent the reply to handle in text
            statusPostInput.setText(text);
            setRemainingCharCount(text);

            //set cursor position after text
            statusPostInput.setSelection(text.length());
        }

        bDollarSymbol = (Button) findViewById(R.id.bDollarSymbol);
        bDollarSymbol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appendDollarSymbol();
            }
        });

        //show display pic
        User thisUser = SharedPreferencesManager.getCurrentlyLoggedInUser(this);
        ImageView suDisplayPic = (ImageView) findViewById(R.id.suDisplayPic);
        Picasso.with(this)
                .load(SSUtils.getFacebookProfilePicUrl(thisUser.getFbAndroidUId()))
                .into(suDisplayPic);

        showFTUE();
    }

    /* Appends dollar symbol to status text */
    private void appendDollarSymbol() {
        String text = statusPostInput.getText().toString();
        if (text.length() >= SSConstants.STATUS_TEXT_CHAR_LIMIT_EDIT_BOX) {
            return;
        }
        text = text + SSConstants.DOLLAR;
        statusPostInput.setText(text);
        statusPostInput.setSelection(text.length());
        setRemainingCharCount(text);
    }

    /* Change the text count & change post button color and enable the button */
    private void setRemainingCharCount(String text) {

        TextView charCounter = (TextView) findViewById(R.id.StatusCharCount);
        if (text == null || text.trim().isEmpty()) {
            charCounter.setText(getResources().getString(R.string.status_text_limit));
            postBtn.setEnabled(false);
            return;
        }

        int remainingChar = SSConstants.STATUS_TEXT_CHAR_LIMIT - text.trim().length();
        charCounter.setText(String.valueOf(remainingChar));
        if (remainingChar < 0) {
            charCounter.setTextColor(getResources().getColor(R.color.red));
            postBtn.setEnabled(false);
        } else {
            charCounter.setTextColor(getResources().getColor(R.color.grey_dark));
            postBtn.setEnabled(true);
        }
    }

    private void onBullishClick(View view) {

        Logger.log("");
        TextView displaySentiment = (TextView) ((ViewGroup) view.getParent()).findViewById(R.id.displaySentiment);
        String tag = (String) view.getTag();

        if (displaySentiment == null) {
            return;
        }

        if (tag == null) {
            view.setBackgroundResource(R.drawable.bull_dark);
            view.setTag(SSConstants.SENTIMENT_BULLISH);
            bearishBtn.setBackgroundResource(R.drawable.bear_light);
            bearishBtn.setTag(null);
            displaySentiment.setText(SSConstants.SENTIMENT_BULLISH_COMPLETE);
            displaySentiment.setTextColor(context.getResources().getColor(R.color.green));
        } else {
            view.setBackgroundResource(R.drawable.bull_light);
            view.setTag(null);
            displaySentiment.setText(SSConstants.EMPTY_STRING);
        }
    }

    private void onBearishClick(View view) {
        Logger.log("");
        TextView displaySentiment = (TextView) ((ViewGroup) view.getParent()).findViewById(R.id.displaySentiment);
        String tag = (String) view.getTag();

        if (displaySentiment == null) {
            return;
        }

        if (tag == null) {
            view.setBackgroundResource(R.drawable.bear_dark);
            view.setTag(SSConstants.SENTIMENT_BEARISH);
            bullishBtn.setBackgroundResource(R.drawable.bull_light);
            bullishBtn.setTag(null);
            displaySentiment.setText(SSConstants.SENTIMENT_BEARISH_COMPLETE);
            displaySentiment.setTextColor(context.getResources().getColor(R.color.red));
        } else {
            view.setBackgroundResource(R.drawable.bear_light);
            view.setTag(null);
            displaySentiment.setText(SSConstants.EMPTY_STRING);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Logger.log("");
        GAnalytics.sendScreenView(mTracker, TAG);
    }

    @Override
    @DebugLog
    public void onStart() {
        super.onStart();
        GAnalytics.sendScreenView(mTracker, TAG);
    }

    private void onPostStatusClick(String replyOfId) {
        Logger.log("");
        EditText editText = (EditText) findViewById(R.id.StatusPostInput);
        String sentiment;

        if (editText == null || editText.getText() == null || editText.getText().toString().trim().isEmpty()) {

            Logger.log("status text is null");
            return;
        }

        //get sentiment
        if ((bearishBtn == null || bearishBtn.getTag() == null) &&
                (bullishBtn == null || bullishBtn.getTag() == null)) {
            sentiment = SSConstants.SENTIMENT_NEUTRAL;
        } else if (bearishBtn != null && bearishBtn.getTag() != null) {
            sentiment = (String) bearishBtn.getTag();
        } else if (bullishBtn != null && bullishBtn.getTag() != null) {
            sentiment = (String) bullishBtn.getTag();
        } else {
            sentiment = SSConstants.SENTIMENT_NEUTRAL;
        }

        SSAPI ssapi = APIHelper.getSsapi();

        ssapi.updateStatus(RequestObjectCreator.getUpdateStatusObject(USER_ID, editText.getText().toString().trim(), sentiment, replyOfId),
                new Callback<Status>() {
                    @Override
                    public void success(Status status, Response response) {
                        SSUtils.SendShowSnackBarMsgBroadcast(context, "Status update successful");
                        sendPushNotifToMentionedUsers(status);
                    }

                    @Override
                    public void failure(RetrofitError error) {

                        Toast.makeText(context, "Update status fail",
                                Toast.LENGTH_SHORT).show();
                    }
                });
        //close post screen
        finish();
    }

    private class StatusTextWatcher implements TextWatcher {

        Activity activity;

        StatusTextWatcher(Activity activity) {
            this.activity = activity;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            //Implementation not required
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            //Implementation not required
        }

        @Override
        public void afterTextChanged(Editable s) {
            setRemainingCharCount(s.toString());
        }
    }

    private void populateAutocompleteSSSymbols(final MultiAutoCompleteTextView view) {

        ArrayAdapter adapter = new ArrayAdapter(context, android.R.layout.simple_list_item_1, GreenDaoUtils.getSSSymbolsArray(context));
        view.setAdapter(adapter);
        view.setTokenizer(new SpaceTokenizer());
        view.setLines(3);
        view.setDropDownBackgroundDrawable(getResources().getDrawable(R.drawable.cust_rect_border));
    }

    private void sendPushNotifToMentionedUsers(final Status status) {

        new Thread(new Runnable() {
            @Override
            public void run() {

                ArrayList<String> userNameList = SSUtils.getUserNameList(status.getStatusText());

                if (userNameList != null && !userNameList.isEmpty()) {

                    final User thisUser = SharedPreferencesManager.getCurrentlyLoggedInUser(context);

                    for (String userName : userNameList) {

                        //remove @ from the userName
                        userName = userName.trim().substring(1);

                        APIHelper.getSsapi().getUserByUserName(userName, new Callback<User>() {
                            @Override
                            @DebugLog
                            public void success(User user, Response response) {
                                //send push notification on success
                                if (thisUser.getUserId() != user.getUserId()) {
                                    SSNotifUtils.sendPushNotification(user.getUserId(), thisUser.getUserId(),
                                            thisUser.getName(), thisUser.getFbAndroidUId(), SSConstants.USER_OPERATION_MENTION_USER, status.getId());
                                }
                            }

                            @Override
                            @DebugLog
                            public void failure(RetrofitError error) {
                                FabricUtils.logException(error);
                            }
                        });
                    }
                }
            }
        }).start();
    }

    private void onCancelClick() {

        if (statusPostInput == null || statusPostInput.getText() == null || statusPostInput.getText().toString().trim().isEmpty()) {
            finish();
            return;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Status update")
                .setMessage("Discard changes?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .show();
    }

    private void showFTUE() {
        FTUEDisplayer.showFTUEOnUpdateStatus(activity, bullishBtn, bearishBtn, postBtn, bDollarSymbol);
    }
}
