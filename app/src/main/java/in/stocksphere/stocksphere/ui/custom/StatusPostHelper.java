package in.stocksphere.stocksphere.ui.custom;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.Tracker;

import hugo.weaving.DebugLog;
import in.stocksphere.stocksphere.R;
import in.stocksphere.stocksphere.data.common.SSConstants;
import in.stocksphere.stocksphere.data.common.SSNotifUtils;
import in.stocksphere.stocksphere.data.common.SSUtils;
import in.stocksphere.stocksphere.data.fabric.FabricUtils;
import in.stocksphere.stocksphere.data.ganalytics.GAnalytics;
import in.stocksphere.stocksphere.data.ganalytics.GAnalyticsConstants;
import in.stocksphere.stocksphere.data.models.SSBase;
import in.stocksphere.stocksphere.data.models.Status;
import in.stocksphere.stocksphere.data.models.User;
import in.stocksphere.stocksphere.data.rest.APIHelper;
import in.stocksphere.stocksphere.data.rest.RequestObjectCreator;
import in.stocksphere.stocksphere.data.sqlite.GreenDaoUtils;
import in.stocksphere.stocksphere.ui.SharedPreferencesManager;
import in.stocksphere.stocksphere.ui.StatusUpdateActivity;
import in.stocksphere.stocksphere.ui.UserActivity;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by vtiwari on 7/2/15.
 * <p/>
 * All Fragments which displays user status updates (timeline, user page) should will use this class
 */
public class StatusPostHelper {

    private User thisUser;
    private long USER_ID;
    private Tracker mTracker;

    public StatusPostHelper(Tracker mTracker) {
        this.mTracker = mTracker;
    }

    @DebugLog
    public void onFavorite(final Status status, View view, final Activity activity, boolean isCalledFromAdaptor) {

        if (!SSUtils.isNetworkAvailable(activity)) {
            SSUtils.displayNoNetworkMsg(activity);
            return;
        }

        thisUser = SharedPreferencesManager.getCurrentlyLoggedInUser(activity);
        TextView favouriteCountView;
        TextView favText = null;
        RelativeLayout likeLayout = null;

        if (isCalledFromAdaptor) {
            favouriteCountView = (TextView) ((ViewGroup) view.getParent()).findViewById(R.id.sFavoriteCount);
        } else {
            favouriteCountView = (TextView) ((ViewGroup) view.getParent()).findViewById(R.id.ssFavCount);
            favText = (TextView) ((ViewGroup) view.getParent()).findViewById(R.id.ssFavText); //shown on status activity
            likeLayout = (RelativeLayout) ((ViewGroup) view.getParent()).findViewById(R.id.likeLayout);
        }

        if (status.getIsFavourited()) {
            status.setIsFavourited(false);
            //change favourite image
            view.setBackgroundResource(R.drawable.favorite);

            //decrease favourite count
            status.setcFavorites(status.getcFavorites() - 1);
        } else {
            status.setIsFavourited(true);
            view.setBackgroundResource(R.drawable.favorite_done);

            //increase favourite count
            status.setcFavorites(status.getcFavorites() + 1);
        }

        //set values
        favouriteCountView.setText(String.valueOf(status.getcFavorites()));
        if (status.getcFavorites() > 0) {
            favouriteCountView.setVisibility(View.VISIBLE);
            if(favText != null){
                favText.setVisibility(View.VISIBLE);
            }
        } else {
            favouriteCountView.setVisibility(View.INVISIBLE);
            if(favText != null){
                favText.setVisibility(View.INVISIBLE);
            }
        }

        if(status.getcLikes() > 0 || status.getcFavorites() > 0){
            if(likeLayout != null) {
                likeLayout.setVisibility(View.VISIBLE);
            }
        }else{
            if(likeLayout != null) {
                likeLayout.setVisibility(View.GONE);
            }
        }

        APIHelper.getSsapi().favoriteStatus(RequestObjectCreator.getFavoriteStatusObject(status.getId().toString(), thisUser.getUserId()
                , status.getIsFavourited()), new Callback<SSBase>() {
            @Override
            public void success(SSBase SSBase, Response response) {

                if(status.getIsFavourited()) {
                    SSUtils.showSnackBarMsg(activity, "Favorite successful");
                }

                //send push notification on success
                if (thisUser.getUserId() != status.getUserId() && status.getIsFavourited()) {
                    SSNotifUtils.sendPushNotification(status.getUserId(), thisUser.getUserId(), thisUser.getName(), thisUser.getFbAndroidUId(), SSConstants.USER_OPERATION_FAVORITE_STATUS, status.getId());
                }
            }

            @Override @DebugLog
            public void failure(RetrofitError error) {

               // Toast.makeText(activity, "Favorite fail",Toast.LENGTH_SHORT).show();
                FabricUtils.logException(error);
            }
        });

        GAnalytics.sendEvent(mTracker, activity.getLocalClassName(),
                GAnalyticsConstants.FAVOURITE_STATUS, status.getId()+SSConstants.HYPHEN+status.getIsFavourited(),
                thisUser.getUserId());
    }

    @DebugLog
    public void onLike(final Status status, View view, final Activity activity, boolean isCalledFromAdaptor) {

        if (!SSUtils.isNetworkAvailable(activity)) {
            SSUtils.displayNoNetworkMsg(activity);
            return;
        }

        thisUser = SharedPreferencesManager.getCurrentlyLoggedInUser(activity);
        TextView likeCountView;
        TextView likeText = null;
        RelativeLayout likeLayout = null;
        if (isCalledFromAdaptor) {
            likeCountView = (TextView) ((ViewGroup) view.getParent()).findViewById(R.id.sLikeCount);
        } else {
            likeCountView = (TextView) ((ViewGroup) view.getParent()).findViewById(R.id.ssLikeCount);
            likeText = (TextView) ((ViewGroup) view.getParent()).findViewById(R.id.ssLikeText); //shown on status activity
            likeLayout = (RelativeLayout) ((ViewGroup) view.getParent()).findViewById(R.id.likeLayout);
        }

        if (status.getIsLiked()) {
            status.setIsLiked(false);
            //change like image
            view.setBackgroundResource(R.drawable.like);

            //decrease like count
            status.setcLikes(status.getcLikes() - 1);
        } else {
            status.setIsLiked(true);
            view.setBackgroundResource(R.drawable.like_done);

            //increase like count
            status.setcLikes(status.getcLikes() + 1);
        }
        //set values
        likeCountView.setText(String.valueOf(status.getcLikes()));
        if (status.getcLikes() > 0) {
            likeCountView.setVisibility(View.VISIBLE);
            if(likeText != null){
                likeText.setVisibility(View.VISIBLE);
            }
        } else {
            likeCountView.setVisibility(View.INVISIBLE);
            if(likeText != null){
                likeText.setVisibility(View.INVISIBLE);
            }
        }

        if(status.getcLikes() > 0 || status.getcFavorites() > 0){
            if(likeLayout != null) {
                likeLayout.setVisibility(View.VISIBLE);
            }
        }else{
            if(likeLayout != null) {
                likeLayout.setVisibility(View.GONE);
            }
        }

        APIHelper.getSsapi().likeStatus(RequestObjectCreator.getLikeStatusObject(status.getId().toString(), thisUser.getUserId()
                , status.getIsLiked()), new Callback<SSBase>() {
            @Override
            public void success(SSBase SSBase, Response response) {

                if(status.getIsLiked()) {
                    SSUtils.showSnackBarMsg(activity, "Like successful");
                }

                //send push notification on success
                if (thisUser.getUserId() != status.getUserId() && status.getIsLiked()) {
                    SSNotifUtils.sendPushNotification(status.getUserId(), thisUser.getUserId(), thisUser.getName(), thisUser.getFbAndroidUId(), SSConstants.USER_OPERATION_LIKE_STATUS, status.getId());
                }
            }

            @Override @DebugLog
            public void failure(RetrofitError error) {

              //  Toast.makeText(activity, "Like fail",Toast.LENGTH_SHORT).show();
                FabricUtils.logException(error);
            }
        });

        GAnalytics.sendEvent(mTracker, activity.getLocalClassName(),
                GAnalyticsConstants.LIKE_STATUS, status.getId()+SSConstants.HYPHEN+status.getIsLiked(),
                thisUser.getUserId());
    }

    @DebugLog
    public void onReply(final Status status, View view, final Activity activity, boolean isCalledFromAdaptor) {

        if (!SSUtils.isNetworkAvailable(activity)) {

            SSUtils.displayNoNetworkMsg(activity);
            return;
        }

        thisUser = SharedPreferencesManager.getCurrentlyLoggedInUser(activity);
        Intent intent = new Intent(activity, StatusUpdateActivity.class);
        intent.putExtra(IntentConstants.MENTION_HANDLE, status.getUserName());
        intent.putExtra(IntentConstants.REPLY_OF_ID, status.getId());
        activity.startActivity(intent);
        GAnalytics.sendEvent(mTracker, activity.getLocalClassName(),
                GAnalyticsConstants.REPLY_STATUS, status.getId(), thisUser.getUserId());
    }

    @DebugLog
    public void onDelete(final Status status, View view, final Activity activity) {

        if (!SSUtils.isNetworkAvailable(activity)) {

            SSUtils.displayNoNetworkMsg(activity);
            return;
        }

        USER_ID = SharedPreferencesManager.getCurrentlyLoggedInUserId(activity);
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        builder.setTitle("Delete")
                .setMessage("Do you want to delete this post?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        APIHelper.getSsapi().deleteStatus(RequestObjectCreator.getDeleteStatusObject(status.getId(), USER_ID), new Callback<SSBase>() {
                            @Override
                            public void success(SSBase SSBase, Response response) {
                                SSUtils.showSnackBarMsg(activity, "Status deleted successfully");
                                GreenDaoUtils.deleteStatus(activity, status.getId(), status.getStatusText());
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                Toast.makeText(activity, "Unable to delete status", Toast.LENGTH_SHORT).show();
                            }
                        });

                        GAnalytics.sendEvent(mTracker, activity.getLocalClassName(),
                                GAnalyticsConstants.DELETE_STATUS, status.getId(), USER_ID);
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .show();
    }

    @DebugLog
    public void onDisplayPicClick(long userId, View view, final Activity activity, boolean isCalledFromAdaptor) {

        USER_ID = SharedPreferencesManager.getCurrentlyLoggedInUserId(activity);
        //do not open logged in user's profile on display image click
        if(USER_ID == userId){
            return;
        }

        if (!SSUtils.isNetworkAvailable(activity)) {
            SSUtils.displayNoNetworkMsg(activity);
            return;
        }

        Intent intent = new Intent(activity, UserActivity.class);
        intent.putExtra(IntentConstants.USER_ID, userId);
        activity.startActivity(intent);
        GAnalytics.sendEvent(mTracker, activity.getLocalClassName(),
                GAnalyticsConstants.DISPLAY_PIC_CLICK, String.valueOf(userId), USER_ID);
    }
}
