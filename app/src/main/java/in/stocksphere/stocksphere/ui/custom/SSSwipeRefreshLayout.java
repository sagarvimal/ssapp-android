package in.stocksphere.stocksphere.ui.custom;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.AttributeSet;

import in.stocksphere.stocksphere.R;

/**
 * Created by vtiwari on 7/5/15.
 */
public class SSSwipeRefreshLayout extends SwipeRefreshLayout {

    public SSSwipeRefreshLayout(Context context) {
        super(context);
        this.setColorSchemeResources(R.color.red,R.color.green);
    }

    public SSSwipeRefreshLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setColorSchemeResources(R.color.red,R.color.green);
    }
}
