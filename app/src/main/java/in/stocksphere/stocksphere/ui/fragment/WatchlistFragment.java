package in.stocksphere.stocksphere.ui.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.analytics.Tracker;
import com.kobakei.ratethisapp.RateThisApp;

import java.util.List;

import hugo.weaving.DebugLog;
import in.stocksphere.stocksphere.Logger;
import in.stocksphere.stocksphere.R;
import in.stocksphere.stocksphere.data.common.SSConstants;
import in.stocksphere.stocksphere.data.common.SSUtils;
import in.stocksphere.stocksphere.data.fabric.FabricUtils;
import in.stocksphere.stocksphere.data.ftue.FTUEDisplayer;
import in.stocksphere.stocksphere.data.ganalytics.GAnalytics;
import in.stocksphere.stocksphere.data.ganalytics.GAnalyticsConstants;
import in.stocksphere.stocksphere.data.models.Company;
import in.stocksphere.stocksphere.data.models.GFGetInfo;
import in.stocksphere.stocksphere.data.models.WatchList;
import in.stocksphere.stocksphere.data.rest.APIHelper;
import in.stocksphere.stocksphere.data.rest.RequestObjectCreator;
import in.stocksphere.stocksphere.data.sqlite.GreenDaoUtils;
import in.stocksphere.stocksphere.ui.SharedPreferencesManager;
import in.stocksphere.stocksphere.ui.custom.SSAdapter;
import in.stocksphere.stocksphere.ui.custom.SSProgressViewer;
import in.stocksphere.stocksphere.ui.custom.SSSwipeRefreshLayout;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class WatchlistFragment extends android.support.v4.app.Fragment {

    private SSProgressViewer progressViewer;
    private List<Company> wCompanyData;
    private View fragRootView;
    private SSSwipeRefreshLayout mSwipeRefreshLayout = null;
    private long USER_ID;
    private RecyclerView mRecyclerView;
    private Activity activity;
    private ImageView wlNoDataView;
    private Tracker mTracker;
    private static final String TAG = WatchlistFragment.class.getSimpleName();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        fragRootView = inflater.inflate(R.layout.fragment_watchlist, container, false);
        activity = getActivity();
        USER_ID = SharedPreferencesManager.getCurrentlyLoggedInUserId(this.getActivity());
        activity = this.getActivity();

        mTracker = GAnalytics.getTracker(getActivity().getApplication());

        wlNoDataView = (ImageView) fragRootView.findViewById(R.id.wlNoDataView);

        mRecyclerView = (RecyclerView) fragRootView.findViewById(R.id.wList);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(activity);
        mRecyclerView.setLayoutManager(mLayoutManager);

        progressViewer = new SSProgressViewer();

        //swipe layout
        mSwipeRefreshLayout = (SSSwipeRefreshLayout) fragRootView.findViewById(R.id.watchlistSwipeLayout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSwipeRefreshLayout.setRefreshing(true);
                requestWatchlistForUser(true);
                populateExchangePrices();
                GAnalytics.sendEvent(mTracker, TAG, GAnalyticsConstants.SWIPE_REFRESH, GAnalyticsConstants.WATCHLIST, USER_ID);
            }
        });

        populateWatchlistOnUIThread();
        showRefreshOnUIThread();
        requestWatchlistForUser(true);
        populateExchangePrices();
        showFTUE();
        setRateThisAppPopUpConfig();
        return fragRootView;
    }

    @Override
    @DebugLog
    public void onStart() {
        super.onStart();
        GAnalytics.sendScreenView(mTracker, TAG);

        //Check Rate this app popup
        checkForRateThisAppPopUp();
    }

    @Override
    @DebugLog
    public void onResume() {
        super.onResume();
        requestWatchlistForUser(false);
        populateExchangePrices();
        GAnalytics.sendScreenView(mTracker, TAG);
    }

    @Override
    @DebugLog
    public void onStop() {
        progressViewer.dismissLoading();
        super.onStop();
    }

    @DebugLog
    private void populateWatchlist() {
        /// populate the watchlist data
        if (null == wCompanyData) {
            Logger.log("WARNING :: wCompanyData empty !!");
            wlNoDataView.setVisibility(View.VISIBLE);
            return;
        }

        wlNoDataView.setVisibility(View.GONE);
        SSAdapter adapter = new SSAdapter(activity,
                R.layout.company_row_item, wCompanyData, mTracker);

        mRecyclerView.setAdapter(adapter);
    }

    @DebugLog
    private void requestWatchlistForUser(boolean showMessage) {

        if (!SSUtils.isNetworkAvailable(activity)) {

            if (showMessage) {
                SSUtils.displayNoNetworkMsg(activity);
            }
            dismissLoadingOnUIThread();
            return;
        }
        fetchWatchlistQuotesFromNSEAPI();
    }

    @DebugLog
    void populateWatchlistOnUIThread() {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            public void run() {

                /// remove loading
                dismissLoadingOnUIThread();

                wCompanyData = GreenDaoUtils.getStockWatchlist(activity);
                /// populate the data fetched
                populateWatchlist();
            }
        });
    }

    @DebugLog
    void dismissLoadingOnUIThread() {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            public void run() {
                mSwipeRefreshLayout.setRefreshing(false);
                progressViewer.dismissLoading();
            }
        });
    }

    @DebugLog
    void showRefreshOnUIThread() {
        mSwipeRefreshLayout.post(new Runnable() {
            public void run() {
                mSwipeRefreshLayout.setRefreshing(true);
            }
        });
    }

    @Override
    @DebugLog
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @DebugLog
    private void fetchWatchlistQuotesFromNSEAPI() {
        APIHelper.getSsapi().getWatchlist(RequestObjectCreator.getWatchlistObject(USER_ID), new Callback<WatchList>() {
            @Override
            @DebugLog
            public void success(final WatchList watchList, Response response) {

                if (watchList == null || watchList.getWatchlist() == null) {
                    dismissLoadingOnUIThread();
                    return;
                }

                wCompanyData = watchList.getWatchlist();

                if (SSUtils.isFetchDataFromGF()) {
                    fetchWatchListQuotesFromGF();
                } else {
                    //in background thread
                    new Thread(new Runnable() {
                        @Override
                        public void run() {

                            // store and populate data
                            GreenDaoUtils.addStockWatchList(activity, wCompanyData);
                            populateWatchlistOnUIThread();
                        }
                    }).start();
                }
            }

            @Override
            @DebugLog
            public void failure(RetrofitError error) {

                FabricUtils.logException(error);
                dismissLoadingOnUIThread();
            }
        });
    }

    @DebugLog
    void fetchWatchListQuotesFromGF() {

        if (wCompanyData == null || wCompanyData.isEmpty()) {
            return;
        }

        APIHelper.getSsapiGoogle().getStockQuoteGF(SSUtils.getGFCNameQueryStr(wCompanyData), new Callback<List<GFGetInfo>>() {
            @Override
            public void success(final List<GFGetInfo> list, Response response) {

                if (list == null || list.isEmpty()) {
                    dismissLoadingOnUIThread();
                    return;
                }
                //in background thread
                new Thread(new Runnable() {
                    @Override
                    public void run() {

                        //update google finance values
                        updateGFWatchlistValues(list);
                        // store and populate data
                        GreenDaoUtils.addStockWatchList(activity, wCompanyData);
                        populateWatchlistOnUIThread();
                    }
                }).start();
            }

            @Override
            public void failure(RetrofitError error) {
                FabricUtils.logException(error);
                dismissLoadingOnUIThread();
            }
        });
    }

    // update google finance watchlist values to local watchlist before populate to UI
    private void updateGFWatchlistValues(List<GFGetInfo> gfWatchlist) {

        for (GFGetInfo gfGetInfo : gfWatchlist) {

            // if last trading price is not null or empty
            if (gfGetInfo.getL() != null && !gfGetInfo.getL().isEmpty()) {

                for (Company company : wCompanyData) {

                    SSUtils.populateCompanyDetailsFromGF(company, gfGetInfo);
                }
            }
        }
    }

    private void populateExchangePrices() {

        //get nifty prices
        APIHelper.getSsapiGoogle().getStockQuoteGF(SSConstants.QUERY_NIFTY, new Callback<List<GFGetInfo>>() {
            @Override
            public void success(final List<GFGetInfo> list, Response response) {

                if (list == null || list.isEmpty()) {
                    return;
                }

                populateNiftyPricesOnUI(list.get(0));
            }

            @Override
            public void failure(RetrofitError error) {
                FabricUtils.logException(error);
            }
        });

        //get sensex prices
        APIHelper.getSsapiGoogle().getStockQuoteGF(SSConstants.QUERY_SENSEX, new Callback<List<GFGetInfo>>() {
            @Override
            public void success(final List<GFGetInfo> list, Response response) {

                if (list == null || list.isEmpty()) {
                    return;
                }

                populateSensexPricesOnUI(list.get(0));
            }

            @Override
            public void failure(RetrofitError error) {
                FabricUtils.logException(error);
            }
        });
    }

    private void populateNiftyPricesOnUI(final GFGetInfo gFGetInfo) {

        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            public void run() {

                RelativeLayout nifty = (RelativeLayout) fragRootView.findViewById(R.id.nifty);
                TextView wPriceNifty = (TextView) fragRootView.findViewById(R.id.wPriceNifty);
                TextView wPerChangeNifty = (TextView) fragRootView.findViewById(R.id.wPerChangeNifty);
                TextView wChangeNifty = (TextView) fragRootView.findViewById(R.id.wChangeNifty);
                TextView wSymbolNifty = (TextView) fragRootView.findViewById(R.id.wSymbolNifty);

                if (SSUtils.getDouble(gFGetInfo.getCp()) > 0) {
                    nifty.setBackgroundColor(activity.getResources().getColor(R.color.green));
                } else {
                    nifty.setBackgroundColor(activity.getResources().getColor(R.color.red));
                }

                wSymbolNifty.setText(SSConstants.NIFTY);
                wPriceNifty.setText(String.valueOf(SSUtils.getDouble(gFGetInfo.getL())));
                wPerChangeNifty.setText(SSConstants.SYMBOL_OPEN_BRACKET + String.valueOf(gFGetInfo.getCp())
                        + SSConstants.SYMBOL_PERCENTAGE + SSConstants.SYMBOL_CLOSED_BRACKET);
                wChangeNifty.setText(String.valueOf(SSUtils.getDouble(gFGetInfo.getC())));
            }
        });
    }

    private void populateSensexPricesOnUI(final GFGetInfo gFGetInfo) {

        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            public void run() {

                RelativeLayout sensex = (RelativeLayout) fragRootView.findViewById(R.id.sensex);
                TextView wPriceSensex = (TextView) fragRootView.findViewById(R.id.wPriceSensex);
                TextView wPerChangeSensex = (TextView) fragRootView.findViewById(R.id.wPerChangeSensex);
                TextView wChangeSensex = (TextView) fragRootView.findViewById(R.id.wChangeSensex);
                TextView wSymbolSensex = (TextView) fragRootView.findViewById(R.id.wSymbolSensex);

                if (SSUtils.getDouble(gFGetInfo.getCp()) > 0) {
                    sensex.setBackgroundColor(activity.getResources().getColor(R.color.green));
                } else {
                    sensex.setBackgroundColor(activity.getResources().getColor(R.color.red));
                }

                wSymbolSensex.setText(SSConstants.SENSEX);
                wPriceSensex.setText(String.valueOf(SSUtils.getDouble(gFGetInfo.getL())));
                wPerChangeSensex.setText(SSConstants.SYMBOL_OPEN_BRACKET + String.valueOf(SSUtils.getDouble(gFGetInfo.getCp()))
                        + SSConstants.SYMBOL_PERCENTAGE + SSConstants.SYMBOL_CLOSED_BRACKET);
                wChangeSensex.setText(String.valueOf(SSUtils.getDouble(gFGetInfo.getC())));
            }
        });
    }

    private void showFTUE() {

        long USER_ID = SharedPreferencesManager.getCurrentlyLoggedInUserId(activity);
        if (USER_ID == SSConstants.DEFAULT_USER_ID) {
            return; // return if user is not logged in
        }

        Handler myHandler = new Handler();
        myHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                FTUEDisplayer.showFTUEOnWatchlist(activity);
            }
        }, FTUEDisplayer.DELAY_MS);
    }

    /* Setup Rate this App pop up
    * For more info visit: https://github.com/kobakei/Android-RateThisApp
    */
    private void setRateThisAppPopUpConfig() {

        // Custom criteria: (#days, #launches)
        RateThisApp.Config config = new RateThisApp.Config(8, 40);
        config.setTitle(R.string.app_rating_popup_title);
        config.setMessage(R.string.app_rating_popup_msg);
        RateThisApp.init(config);
    }

    private void checkForRateThisAppPopUp() {
        // Monitor launch times and interval from installation
        RateThisApp.onStart(activity);
        // If the criteria is satisfied, "Rate this app" dialog will be shown
        RateThisApp.showRateDialogIfNeeded(activity);
    }
}
