package in.stocksphere.stocksphere.ui;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.google.android.gms.analytics.Tracker;
import com.squareup.picasso.Picasso;

import java.util.List;

import hugo.weaving.DebugLog;
import in.stocksphere.stocksphere.Logger;
import in.stocksphere.stocksphere.R;
import in.stocksphere.stocksphere.data.common.CircleTransform;
import in.stocksphere.stocksphere.data.common.SSConstants;
import in.stocksphere.stocksphere.data.common.SSNotifUtils;
import in.stocksphere.stocksphere.data.common.SSUtils;
import in.stocksphere.stocksphere.data.fabric.FabricUtils;
import in.stocksphere.stocksphere.data.ganalytics.GAnalytics;
import in.stocksphere.stocksphere.data.ganalytics.GAnalyticsConstants;
import in.stocksphere.stocksphere.data.models.Company;
import in.stocksphere.stocksphere.data.models.CompanyList;
import in.stocksphere.stocksphere.data.models.User;
import in.stocksphere.stocksphere.data.rest.APIHelper;
import in.stocksphere.stocksphere.data.rest.RequestObjectCreator;
import in.stocksphere.stocksphere.data.sqlite.GreenDaoUtils;
import in.stocksphere.stocksphere.ui.custom.FilterWithSpaceAdapter;
import in.stocksphere.stocksphere.ui.custom.IntentConstants;
import in.stocksphere.stocksphere.ui.fragment.MyPageFragment;
import in.stocksphere.stocksphere.ui.fragment.NewsFragment;
import in.stocksphere.stocksphere.ui.fragment.NotificationFragment;
import in.stocksphere.stocksphere.ui.fragment.SettingsFragment;
import in.stocksphere.stocksphere.ui.fragment.TimelineFragment;
import in.stocksphere.stocksphere.ui.fragment.WatchlistFragment;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class MainActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private DrawerLayout dlDrawer;
    private ActionBarDrawerToggle drawerToggle;
    private Context context;
    private Activity activity;
    private MenuItem menuSearch;
    private BroadcastReceiver broadcastReceiver;
    private Tracker mTracker;
    private static final String TAG = MainActivity.class.getSimpleName();
    private long USER_ID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;
        activity = this;
        USER_ID = SharedPreferencesManager.getCurrentlyLoggedInUserId(this);

        // add some customization to the log messages
//        GalgoOptions options = new GalgoOptions.Builder()
//                .numberOfLines(15)
//                .backgroundColor(Color.parseColor("#D9d6d6d6"))
//                .textColor(Color.BLACK)
//                .textSize(12)
//                .build();
//        Galgo.enable(this, options);

        //if user id is default then show login screen
        if (USER_ID == SSConstants.DEFAULT_USER_ID) {
            showLogin();
        }

        mTracker = GAnalytics.getTracker(getApplication());

        // Set a Toolbar to replace the ActionBar.
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Find our drawer view
        dlDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerToggle = this.setupDrawerToggle();

        // Tie DrawerLayout events to the ActionBarToggle
        dlDrawer.setDrawerListener(drawerToggle);

        // Set the menu icon instead of the launcher icon.
        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setHomeAsUpIndicator(R.drawable.ic_menu);
            ab.setDisplayHomeAsUpEnabled(true);
        }

        // Find our drawer view
        NavigationView nvDrawer = (NavigationView) findViewById(R.id.nvView);
        // Setup drawer view
        setupDrawerContent(nvDrawer);
        createInitialFragment();
        populateUserProfileInDrawer();
        downLoadCompanyNames();
        createBroadcastReceiver();
        receiveDataFromOtherApp();
    }

    @Override
    protected void onResume() {
        registerReceiver(broadcastReceiver, new IntentFilter(IntentConstants.INTENT_ACTION_SHOW_SNACK_BAR));
        super.onResume();
        if (menuSearch != null) {
            //collapse the action bar search view
            menuSearch.collapseActionView();
        }
        GAnalytics.sendScreenView(mTracker, TAG);
    }

    @Override
    @DebugLog
    public void onStart() {
        super.onStart();
        GAnalytics.sendScreenView(mTracker, TAG);
    }

    @Override
    protected void onDestroy() {
        if (broadcastReceiver != null) {
            try {
                unregisterReceiver(broadcastReceiver);
            } catch (Exception e) {
                FabricUtils.logException(e);
            }
        }
        super.onDestroy();
        // always call disable to avoid memory leaks
//        Galgo.disable(this);
    }

    private ActionBarDrawerToggle setupDrawerToggle() {
        return new ActionBarDrawerToggle(this, dlDrawer, toolbar, R.string.drawer_open, R.string.drawer_close);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);
        setSearchViewHintText(menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // The action bar home/up action should open or close the drawer.
        switch (item.getItemId()) {
            case android.R.id.home:
                dlDrawer.openDrawer(GravityCompat.START);
                return true;
            case R.id.toolbar_search:
                return true;
            case R.id.toolbar_post:
                showPostDialog();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setupDrawerContent(NavigationView navigationView) {

        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        return true;
                    }
                });
    }

    public void selectDrawerItem(MenuItem menuItem) {
        // Create a new fragment and specify the planet to show based on
        // position
        Fragment fragment = null;
        Class fragmentClass = null;
        boolean isChangeFragment = true;

        switch (menuItem.getItemId()) {
            case R.id.nav_watchlist:
                fragmentClass = WatchlistFragment.class;
                break;
            case R.id.nav_news:
                fragmentClass = NewsFragment.class;
                break;
            case R.id.nav_timeline:
                fragmentClass = TimelineFragment.class;
                break;
            case R.id.nav_notif:
                fragmentClass = NotificationFragment.class;
                break;
            case R.id.nav_settings:
                fragmentClass = SettingsFragment.class;
                break;
            case R.id.nav_logout:
                isChangeFragment = false;
                doLogout();
                break;
            default:
                fragmentClass = WatchlistFragment.class;
        }

        if (isChangeFragment) {
            try {
                fragment = (Fragment) fragmentClass.newInstance();
            } catch (Exception e) {
                FabricUtils.logException(e);
            }

            // Insert the fragment by replacing any existing fragment
            FragmentManager fragmentManager = this.getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();
        }

        // Highlight the selected item, update the title, and close the drawer
        menuItem.setChecked(true);
        setTitle(menuItem.getTitle());
        dlDrawer.closeDrawers();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        drawerToggle.onConfigurationChanged(newConfig);
    }

    private void createInitialFragment() {
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flContent, new WatchlistFragment()).commit();
    }

    private void showPostDialog() {
        Intent intent = new Intent(activity, StatusUpdateActivity.class);
        activity.startActivity(intent);
    }

    private void setSearchViewHintText(Menu menu) {
        menuSearch = menu.findItem(R.id.toolbar_search);
        SearchView searchView = (SearchView) menuSearch.getActionView();
        searchView.setQueryHint("Search companies..");

        SearchView.SearchAutoComplete autoCompleteTextView = (SearchView.SearchAutoComplete) searchView.findViewById(R.id.search_src_text);

        if (autoCompleteTextView != null) {
            List<Company> wCompanyData;
            wCompanyData = GreenDaoUtils.getStockWatchlist(context);
            populateAutoCompleteCompanyNames(autoCompleteTextView, wCompanyData);
        } else {
            FabricUtils.log("MainActivity", "SearchView.SearchAutoComplete autoCompleteTextView is null");
        }
    }

    @DebugLog
    private void populateAutoCompleteCompanyNames(final AutoCompleteTextView autoCompleteTextView, final List<Company> wCompanyData) {

        FilterWithSpaceAdapter adapter = new FilterWithSpaceAdapter(this, android.R.layout.simple_list_item_1, GreenDaoUtils.getSSSymbolNamesArray(this));
        autoCompleteTextView.setAdapter(adapter);
        autoCompleteTextView.setLines(8);
        autoCompleteTextView.setDropDownBackgroundDrawable(getResources().getDrawable(R.drawable.cust_rect_border));
        autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> parent, final View view, final int position, long id) {
                Handler handler = new Handler(Looper.getMainLooper());
                handler.post(new Runnable() {
                    public void run() {
                        try {
                            String selectedValue = ((TextView) view).getText().toString();
                            String ssSymbol = (selectedValue.split(SSConstants.AUTO_COMPLETE_SEPARATOR))[0];
                            SSUtils.hideKeyboard(activity);

                            //display company page
                            Intent intent = new Intent(context, CompanyActivity.class);

                            //send watchlist details in intent
                            if (wCompanyData != null && !wCompanyData.isEmpty()) {
                                for (Company company : wCompanyData) {

                                    if (company.getSsSymbol().equalsIgnoreCase(ssSymbol.trim())) {
                                        intent.putExtra(IntentConstants.IS_ADDED_TO_WATCHLIST, true);
                                        if (company.getWlNotification() != null) {
                                            intent.putExtra(IntentConstants.COMPANY_WATCHLIST_NOTIF, company.getWlNotification());
                                        }
                                        break;
                                    }
                                }
                            }
                            GAnalytics.sendEvent(mTracker, TAG, GAnalyticsConstants.SEARCH_COMPANY, ssSymbol.trim(), USER_ID);
                            intent.putExtra(IntentConstants.COMPANY_SS_SYMBOL, ssSymbol.trim());
                            context.startActivity(intent);
                        } catch (Exception e) {
                            Toast.makeText(context, "Unable to load company screen", Toast.LENGTH_SHORT).show();
                            FabricUtils.logException(e);
                        }
                    }
                });
            }
        });
    }

    private void populateUserProfileInDrawer() {

        final User user = SharedPreferencesManager.getCurrentlyLoggedInUser(this);

        if (user == null) {
            return;
        }

        ImageView imageView = (ImageView) this.findViewById(R.id.headerIcon);
        Picasso.with(activity)
                .load(SSUtils.getFacebookProfilePicUrl(user.getFbAndroidUId()))
                .transform(new CircleTransform())
                .into(imageView);

        TextView nameView = (TextView) this.findViewById(R.id.headerName);
        nameView.setText(user.getName());

        TextView userNameView = (TextView) this.findViewById(R.id.headerUserName);
        userNameView.setText(SSConstants.AT_THE_RATE + user.getUserName());

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoCurrentUserScreen();
            }
        });

        nameView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoCurrentUserScreen();
            }
        });

        userNameView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoCurrentUserScreen();
            }
        });
    }

    private void gotoCurrentUserScreen() {
        //close opened drawer
        dlDrawer.closeDrawers();
        //remove title
        setTitle("Me");
        //switch to current user fragment
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flContent, new MyPageFragment()).commit();
    }

    private void doLogout() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Log Out")
                .setMessage("Are you sure you want to log out?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //clear user data
                        SharedPreferencesManager.clearUserData(activity);
                        //unSubscribePushNotification
                        SSNotifUtils.unSubscribePushNotification();
                        //log out from facebook
                        LoginManager.getInstance().logOut();
                        //back to login screen
                        showLogin();
                        GAnalytics.sendEvent(mTracker, TAG, GAnalyticsConstants.LOGOUT, GAnalyticsConstants.OK, USER_ID);
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        GAnalytics.sendEvent(mTracker, TAG, GAnalyticsConstants.LOGOUT, GAnalyticsConstants.CANCEL, USER_ID);
                    }
                })
                .show();
    }

    private void showLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    /*
    It first checks if company names are already present otherwise download it
     */
    @DebugLog
    private void downLoadCompanyNames() {

        if (!GreenDaoUtils.isCompanyNamesPresent(activity)) {

            APIHelper.getSsapi().getAllCompanyNames(RequestObjectCreator.getAllCompanyNamesObject(), new Callback<CompanyList>() {
                @Override
                public void success(CompanyList companyList, Response response) {

                    if (companyList == null || companyList.getCompanies() == null || companyList.getCompanies().isEmpty()) {

                        Logger.log("null in all company names");
                        return;
                    }

                    GreenDaoUtils.addToCompanyNames(activity, companyList.getCompanies());
                }

                @Override
                public void failure(RetrofitError error) {

                    Logger.log("RetrofitError in all company names");
                    FabricUtils.logException(error);
                }
            });
        }
    }

    /*
    create broadcast receiver to show snack bar messages in case of status and profile update etc
     */
    private void createBroadcastReceiver() {
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String message = intent.getStringExtra(IntentConstants.SNACK_BAR_MESSAGE);

                if (!intent.getAction().equals(IntentConstants.INTENT_ACTION_SHOW_SNACK_BAR)
                        || message == null || message.isEmpty()) {
                    return;
                }

                SSUtils.showSnackBarMsg(activity, message);
            }
        };
    }

    /*
     * This will be called to check if some other app has shared data with this app and opens the
     * StatusUpdateActivity
     */
    private void receiveDataFromOtherApp() {
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        if (action != null && Intent.ACTION_SEND.equals(action) && type != null
                && SSConstants.TEXT_PLAIN.equals(type)) {

            String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
            if (sharedText != null && !sharedText.isEmpty()) {
                Intent newIntent = new Intent(activity, StatusUpdateActivity.class);
                newIntent.putExtra(IntentConstants.SHARED_STATUS_TEXT, sharedText);
                activity.startActivity(newIntent);
            }
        }
    }
}