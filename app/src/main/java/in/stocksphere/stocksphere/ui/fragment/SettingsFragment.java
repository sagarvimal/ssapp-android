package in.stocksphere.stocksphere.ui.fragment;


import android.app.Activity;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.analytics.Tracker;

import java.util.ArrayList;

import hugo.weaving.DebugLog;
import in.stocksphere.stocksphere.R;
import in.stocksphere.stocksphere.data.common.SSConstants;
import in.stocksphere.stocksphere.data.common.SSUtils;
import in.stocksphere.stocksphere.data.ganalytics.GAnalytics;
import in.stocksphere.stocksphere.data.ganalytics.GAnalyticsConstants;
import in.stocksphere.stocksphere.data.models.TradingApp;
import in.stocksphere.stocksphere.ui.SharedPreferencesManager;
import in.stocksphere.stocksphere.ui.custom.TradingAppsAdaptor;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends android.support.v4.app.Fragment {

    private View fragRootView;
    private long USER_ID;
    private Activity activity;
    private Tracker mTracker;
    private static final String TAG = SettingsFragment.class.getSimpleName();
    private ImageView sSelectedAppIcon;
    private TextView sSelectedAppName;
    private Button sSelectedAppLaunchButton;
    private TradingApp tradingApp = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        fragRootView = inflater.inflate(R.layout.fragment_settings, container, false);

        USER_ID = SharedPreferencesManager.getCurrentlyLoggedInUserId(this.getActivity());
        activity = this.getActivity();
        mTracker = GAnalytics.getTracker(getActivity().getApplication());

        Button sShowTradingApps = (Button) fragRootView.findViewById(R.id.sShowTradingApps);
        sSelectedAppIcon = (ImageView) fragRootView.findViewById(R.id.sSelectedAppIcon);
        sSelectedAppName = (TextView) fragRootView.findViewById(R.id.sSelectedAppName);
        sSelectedAppLaunchButton = (Button) fragRootView.findViewById(R.id.sSelectedAppLaunchButton);

        //populate trading app
        populateSelectedTradingApp();

        sShowTradingApps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowTradingApps();
            }
        });

        return fragRootView;
    }

    @Override
    @DebugLog
    public void onStart() {
        super.onStart();
        GAnalytics.sendScreenView(mTracker, TAG);
    }

    @Override
    @DebugLog
    public void onResume() {
        super.onResume();
        GAnalytics.sendScreenView(mTracker, TAG);
    }

    private void ShowTradingApps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        final ArrayList<TradingApp> installTradingApps = (ArrayList<TradingApp>) SSUtils.getInstalledTradingApps(activity);
        ArrayAdapter<TradingApp> adapter = new TradingAppsAdaptor(activity, R.layout.trading_apps, installTradingApps);

        ArrayList<TradingApp> installedApps = (ArrayList<TradingApp>) SSUtils.getInstalledTradingApps(activity);
        if (installedApps == null) {
            builder.setMessage("No trading app is installed");
        } else {
            builder.setSingleChoiceItems(adapter, -1, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    tradingApp = installTradingApps.get(item);
                    populateSelectedTradingApp();
                    SharedPreferencesManager.saveTradingAppPackageName(activity, tradingApp.getAppPackageName());
                    dialog.dismiss();
                }
            });
        }
        builder.setTitle("Select your trading app")
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .show();

        GAnalytics.sendEvent(mTracker, TAG, GAnalyticsConstants.SHOW_TRADING_APPS, SSConstants.EMPTY_STRING, USER_ID);
    }

    private void populateSelectedTradingApp() {

        if (tradingApp == null) {
            String appPackageName = SharedPreferencesManager.getTradingAppPackageName(activity);
            if (!appPackageName.isEmpty() && SSUtils.isAppInstalled(activity, appPackageName)) {
                tradingApp = SSUtils.getApplicationInfo(activity, appPackageName);
            } else {
                return;
            }
        }
        sSelectedAppIcon.setVisibility(View.VISIBLE);
        sSelectedAppIcon.setImageDrawable(tradingApp.getAppIcon());
        sSelectedAppName.setText(tradingApp.getAppName());
        sSelectedAppLaunchButton.setVisibility(View.VISIBLE);
        sSelectedAppLaunchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SSUtils.launchApp(activity, tradingApp.getAppPackageName());
                GAnalytics.sendEvent(mTracker, TAG, GAnalyticsConstants.LAUNCH_TRADING_APP, tradingApp.getAppPackageName(), USER_ID);
            }
        });
    }
}
