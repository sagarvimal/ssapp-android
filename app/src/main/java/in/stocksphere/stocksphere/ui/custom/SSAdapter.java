package in.stocksphere.stocksphere.ui.custom;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.style.URLSpan;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.analytics.Tracker;
import com.squareup.picasso.Picasso;

import java.util.List;

import hugo.weaving.DebugLog;
import in.stocksphere.stocksphere.Logger;
import in.stocksphere.stocksphere.R;
import in.stocksphere.stocksphere.data.common.SSConstants;
import in.stocksphere.stocksphere.data.common.SSUtils;
import in.stocksphere.stocksphere.data.ganalytics.GAnalytics;
import in.stocksphere.stocksphere.data.ganalytics.GAnalyticsConstants;
import in.stocksphere.stocksphere.data.jsoup.MetadataRetriever;
import in.stocksphere.stocksphere.data.jsoup.OpenGraphMetadata;
import in.stocksphere.stocksphere.data.models.AppNotification;
import in.stocksphere.stocksphere.data.models.Company;
import in.stocksphere.stocksphere.data.models.Status;
import in.stocksphere.stocksphere.data.models.User;
import in.stocksphere.stocksphere.data.sqlite.GreenDaoUtils;
import in.stocksphere.stocksphere.ui.CompanyActivity;
import in.stocksphere.stocksphere.ui.SharedPreferencesManager;
import in.stocksphere.stocksphere.ui.StatusActivity;

/**
 * Created by srishti on 17/06/15.
 */
public class SSAdapter<S, T extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<T> {

    private int layoutResourceId;
    private List<S> data = null;
    private Activity activity = null;
    private StatusPostHelper postHelper = null;
    private long userId;
    private Tracker mTracker;

    public SSAdapter(Activity activity, int layoutResourceId, List<S> data, Tracker mTracker) {

        if (data == null) {
            throw new IllegalArgumentException("data in adapter must not be null");
        }

        this.layoutResourceId = layoutResourceId;
        this.data = data;
        this.activity = activity;
        this.mTracker = mTracker;
        postHelper = new StatusPostHelper(mTracker);
        userId = SharedPreferencesManager.getCurrentlyLoggedInUserId(activity);

    }

    @Override
    public T onCreateViewHolder(ViewGroup viewGroup, int itemType) {

        T viewHolder = null;
        View v;
        switch (layoutResourceId) {

            case R.layout.company_row_item: {
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.company_row_item, viewGroup, false);
                CompanyHolder companyHolder = new CompanyHolder(v);
                viewHolder = (T) companyHolder;
                break;
            }
            case R.layout.user_row_item: {
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.user_row_item, viewGroup, false);
                UserHolder userHolder = new UserHolder(v);
                viewHolder = (T) userHolder;
                break;
            }
            case R.layout.status_row_item: {
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.status_row_item, viewGroup, false);
                StatusHolder statusHolder = new StatusHolder(v);
                viewHolder = (T) statusHolder;
                break;
            }
            case R.layout.notif_status_row_item: {
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.notif_status_row_item, viewGroup, false);
                UserNotificationHolder notificationHolder = new UserNotificationHolder(v);
                viewHolder = (T) notificationHolder;
                break;
            }
            default:
                Logger.log("Unhandled (ROW no show)!!");

        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(T t, int position) {

        CompanyHolder companyHolder;
        UserHolder userHolder;
        StatusHolder statusHolder;
        UserNotificationHolder userNotificationHolder;

        /**
         * The row details gets populated here, based on type...
         */
        switch (layoutResourceId) {

            case R.layout.company_row_item: {
                companyHolder = (CompanyHolder) t;
                Company company = (Company) data.get(position);
                companyHolder.txtStockSymbol.setText(company.getSsSymbol());

                // show only first 18 letters of company name
                if (company.getName() != null && !company.getName().isEmpty()) {

                    int SHOW_NAME_CHAR_LIMIT = 18;

                    if (company.getName().length() <= SHOW_NAME_CHAR_LIMIT) {
                        companyHolder.txtStockName.setText(company.getName());
                    } else {
                        companyHolder.txtStockName.setText(company.getName().substring(0, SHOW_NAME_CHAR_LIMIT - 1) + "..");
                    }
                } else {
                    companyHolder.txtStockName.setText(company.getSsSymbol());
                }

                if (layoutResourceId == R.layout.company_row_item) {
                    companyHolder.txtStockChange.setText(SSConstants.SYMBOL_INR + SSConstants.SPACE + String.valueOf(company.getNseQuote().getChangePrice()));
                    companyHolder.txtStockPrice.setText(SSConstants.SYMBOL_INR + SSConstants.SPACE + String.valueOf(company.getNseQuote().getLastMarketPrice()));
                    companyHolder.txtStockPerChange.setText(SSConstants.SYMBOL_OPEN_BRACKET + String.valueOf(company.getNseQuote()
                            .getPercentageChange()) + SSConstants.SYMBOL_PERCENTAGE + SSConstants.SYMBOL_CLOSED_BRACKET);

                    if (company.getNseQuote().getPercentageChange() > 0.0) {
                        companyHolder.imgStockChangeShow.setImageResource(R.drawable.wl_green_stripe);
                        companyHolder.txtStockChange.setTextColor(activity.getResources().getColor(R.color.green));
                        companyHolder.txtStockPerChange.setTextColor(activity.getResources().getColor(R.color.green));
                    } else {
                        companyHolder.imgStockChangeShow.setImageResource(R.drawable.wl_red_stripe);
                        companyHolder.txtStockChange.setTextColor(activity.getResources().getColor(R.color.red));
                        companyHolder.txtStockPerChange.setTextColor(activity.getResources().getColor(R.color.red));
                    }
                }

                break;
            }
            case R.layout.user_row_item: {
                userHolder = (UserHolder) t;
                final User user = (User) data.get(position);
                userHolder.txtName.setText(user.getName());
                userHolder.txtUserName.setText(SSConstants.AT_THE_RATE + user.getUserName());
                userHolder.txtUserAboutUser.setText(user.getAboutUser());
                Picasso.with(activity)
                        .load(SSUtils.getFacebookProfilePicUrl(user.getFbAndroidUId()))
                        .into(userHolder.imgUserDP);

                userHolder.imgUserDP.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View vw) {
                        postHelper.onDisplayPicClick(user.getUserId(), vw, activity, true);
                    }
                });
                break;
            }
            case R.layout.status_row_item: {
                statusHolder = (StatusHolder) t;
                final Status status = (Status) data.get(position);
                statusHolder.txtUserUpdateMessage.setText(status.getStatusText());
                //Show all urls in text as clickable
                Linkify.addLinks(statusHolder.txtUserUpdateMessage, Linkify.WEB_URLS);

                //populate metadata from linkified website
                statusHolder.populateWebPageMetadata(statusHolder.txtUserUpdateMessage.getUrls());

                statusHolder.txtUserFullName.setText(status.getUserFullName());
                statusHolder.txtUserName.setText(SSConstants.AT_THE_RATE + status.getUserName());
                statusHolder.txtElapsedTime.setText(SSUtils.getTimeElapsed(status.getLastUpdatedDate()));
                Picasso.with(activity)
                        .load(SSUtils.getFacebookProfilePicUrl(status.getUserFbAndroidUId()))
                        .into(statusHolder.imgUserDisplayPic);

                if (status.getcLikes() > 0) {
                    statusHolder.txtLikeCount.setText(String.valueOf(status.getcLikes()));
                } else {
                    statusHolder.txtLikeCount.setText(SSConstants.EMPTY_STRING);
                }

                if (status.getcReplies() > 0) {
                    statusHolder.txtReplyCount.setText(String.valueOf(status.getcReplies()));
                } else {
                    statusHolder.txtReplyCount.setText(SSConstants.EMPTY_STRING);
                }

                if (status.getcFavorites() > 0) {
                    statusHolder.txtFavoriteCount.setText(String.valueOf(status.getcFavorites()));
                } else {
                    statusHolder.txtFavoriteCount.setText(SSConstants.EMPTY_STRING);
                }

                if (status.getOperation() != null && !status.getOperation().isEmpty()
                        && status.getUserId() != status.getUserIdOfOperation()) {

                    if (status.getOperation().equals(SSConstants.TIMELINE_OPERATION_LIKE)) {

                        if (userId == status.getUserIdOfOperation()) {
                            statusHolder.txtOperationMsg.setText(SSConstants.TIMELINE_OPERATION_MSG_USER_YOU
                                    + SSConstants.SPACE + SSConstants.TIMELINE_OPERATION_MSG_LIKE);
                        } else {
                            statusHolder.txtOperationMsg.setText(status.getUserOfOperation()
                                    + SSConstants.SPACE + SSConstants.TIMELINE_OPERATION_MSG_LIKE);
                        }
                        statusHolder.imgOperation.setImageResource(R.drawable.like);
                        statusHolder.imgOperation.setVisibility(View.VISIBLE);
                        statusHolder.txtOperationMsg.setVisibility(View.VISIBLE);
                    } else if (status.getOperation().equals(SSConstants.TIMELINE_OPERATION_REPLY)) {

                        if (userId == status.getUserIdOfOperation()) {
                            statusHolder.txtOperationMsg.setText(SSConstants.TIMELINE_OPERATION_MSG_USER_YOU
                                    + SSConstants.SPACE + SSConstants.TIMELINE_OPERATION_MSG_REPLY);
                        } else {
                            statusHolder.txtOperationMsg.setText(status.getUserOfOperation()
                                    + SSConstants.SPACE + SSConstants.TIMELINE_OPERATION_MSG_REPLY);
                        }
                        statusHolder.imgOperation.setImageResource(R.drawable.reply);
                        statusHolder.imgOperation.setVisibility(View.VISIBLE);
                        statusHolder.txtOperationMsg.setVisibility(View.VISIBLE);
                    } else if (status.getOperation().equals(SSConstants.TIMELINE_OPERATION_FAVORITE)) {

                        if (userId == status.getUserIdOfOperation()) {
                            statusHolder.txtOperationMsg.setText(SSConstants.TIMELINE_OPERATION_MSG_USER_YOU
                                    + SSConstants.SPACE + SSConstants.TIMELINE_OPERATION_MSG_FAVOURITED);
                        } else {
                            statusHolder.txtOperationMsg.setText(status.getUserOfOperation()
                                    + SSConstants.SPACE + SSConstants.TIMELINE_OPERATION_MSG_FAVOURITED);
                        }
                        statusHolder.imgOperation.setImageResource(R.drawable.favorite);
                        statusHolder.imgOperation.setVisibility(View.VISIBLE);
                        statusHolder.txtOperationMsg.setVisibility(View.VISIBLE);
                    } else {

                        statusHolder.imgOperation.setVisibility(View.GONE);
                        statusHolder.txtOperationMsg.setVisibility(View.GONE);
                    }
                } else {

                    statusHolder.imgOperation.setVisibility(View.GONE);
                    statusHolder.txtOperationMsg.setVisibility(View.GONE);
                }

                if (status.getIsFavourited()) {

                    statusHolder.btnFavourite.setBackgroundResource(R.drawable.favorite_done);
                } else {
                    statusHolder.btnFavourite.setBackgroundResource(R.drawable.favorite);
                }

                if (status.getIsLiked()) {

                    statusHolder.btnLike.setBackgroundResource(R.drawable.like_done);
                } else {
                    statusHolder.btnLike.setBackgroundResource(R.drawable.like);
                }

                //set sentiment image
                if (status.getSentiment().equals(SSConstants.SENTIMENT_BULLISH)) {
                    statusHolder.imgSentiment.setBackgroundResource(R.drawable.bull_dark);
                    statusHolder.imgSentiment.setVisibility(View.VISIBLE);
                } else if (status.getSentiment().equals(SSConstants.SENTIMENT_BEARISH)) {
                    statusHolder.imgSentiment.setBackgroundResource(R.drawable.bear_dark);
                    statusHolder.imgSentiment.setVisibility(View.VISIBLE);
                } else {
                    statusHolder.imgSentiment.setVisibility(View.GONE);
                }

                //set status onclick listeners
                statusHolder.btnFavourite.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View vw) {

                        postHelper.onFavorite(status, vw, activity, true);
                    }
                });
                statusHolder.btnLike.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View vw) {
                        postHelper.onLike(status, vw, activity, true);
                    }
                });
                statusHolder.btnReply.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View vw) {
                        postHelper.onReply(status, vw, activity, true);
                    }
                });
                statusHolder.imgUserDisplayPic.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View vw) {
                        postHelper.onDisplayPicClick(status.getUserId(), vw, activity, true);
                    }
                });
                break;
            }
            case R.layout.notif_status_row_item: {
                userNotificationHolder = (UserNotificationHolder) t;
                final AppNotification userNotification = (AppNotification) data.get(position);
                userNotificationHolder.txtNotificationMessage.setText(userNotification.getMessage());
                userNotificationHolder.txtTimelapsed.setText(SSUtils.getTimeElapsed(userNotification.getUpdatedAt().getTime()))
                ;
                Picasso.with(activity)
                        .load(SSUtils.getFacebookProfilePicUrl(userNotification.getSenderFbAndroidUId()))
                        .into(userNotificationHolder.imgUserDisplayPic);
                userNotificationHolder.imgUserDisplayPic.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View vw) {
                        postHelper.onDisplayPicClick(userNotification.getSenderId(), vw, activity, true);
                    }
                });
                userNotificationHolder.txtNotificationMessage.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (userNotification.getStatusId() != null && !userNotification.getStatusId().isEmpty()) {
                            Intent intent = new Intent(activity, StatusActivity.class);
                            intent.putExtra(IntentConstants.STATUS_ID, userNotification.getStatusId());
                            activity.startActivity(intent);
                        } else {
                            postHelper.onDisplayPicClick(userNotification.getSenderId(), v, activity, true);
                        }
                    }
                });
                break;
            }
            default:
                Logger.log("Unhandled (ROW no show)!!");
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void add(int position, S item) {
        data.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(S item) {
        int position = data.indexOf(item);
        data.remove(position);
        notifyItemRemoved(position);
    }

    class CompanyHolder extends RecyclerView.ViewHolder implements View.OnClickListener    /* WatchlistFragment Company Row Holder */ {
        TextView txtStockSymbol;
        TextView txtStockName;
        TextView txtStockChange;
        TextView txtStockPrice;
        TextView txtStockPerChange;
        ImageView imgStockChangeShow;

        public CompanyHolder(View itemView) {
            super(itemView);

            txtStockChange = (TextView) itemView.findViewById(R.id.wChange);
            txtStockPrice = (TextView) itemView.findViewById(R.id.wPrice);
            txtStockPerChange = (TextView) itemView.findViewById(R.id.wPerChange);
            imgStockChangeShow = (ImageView) itemView.findViewById(R.id.wlStripe);
            txtStockSymbol = (TextView) itemView.findViewById(R.id.wSymbol);
            txtStockName = (TextView) itemView.findViewById(R.id.wName);

            //set onclick listener
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Company company = (Company) data.get(getAdapterPosition());
            Intent intent = new Intent(activity, CompanyActivity.class);
            intent.putExtra(IntentConstants.COMPANY_SS_SYMBOL, company.getSsSymbol());
            activity.startActivity(intent);
            GAnalytics.sendEvent(mTracker, activity.getLocalClassName(),
                    GAnalyticsConstants.CLICK_COMPANY_ROW, company.getSsSymbol(), userId);
        }
    }

    class StatusHolder extends RecyclerView.ViewHolder implements View.OnClickListener    /* Timeline Status Row Holder */ {
        TextView txtUserFullName;
        TextView txtUserName;
        TextView txtUserUpdateMessage;
        TextView txtElapsedTime;
        ImageView imgUserDisplayPic;
        TextView txtLikeCount;
        TextView txtReplyCount;
        TextView txtFavoriteCount;
        TextView txtOperationMsg;
        ImageView imgOperation;
        Button btnFavourite;
        Button btnLike;
        Button btnReply;
        ImageView imgSentiment;
        CardView urlMetadataCardView;
        ImageView urlMetadataImage;
        TextView urlMetadataTitle;
        TextView urlMetadataHost;

        public StatusHolder(View itemView) {
            super(itemView);

            txtUserFullName = (TextView) itemView.findViewById(R.id.sUserFullName);
            txtUserName = (TextView) itemView.findViewById(R.id.sUserName);
            txtUserUpdateMessage = (TextView) itemView.findViewById(R.id.sStatusMessage);
            txtElapsedTime = (TextView) itemView.findViewById(R.id.sTimeElapsed);
            imgUserDisplayPic = (ImageView) itemView.findViewById(R.id.sDisplayPic);
            txtLikeCount = (TextView) itemView.findViewById(R.id.sLikeCount);
            txtReplyCount = (TextView) itemView.findViewById(R.id.sReplyCount);
            txtFavoriteCount = (TextView) itemView.findViewById(R.id.sFavoriteCount);
            txtOperationMsg = (TextView) itemView.findViewById(R.id.sOperationMsg);
            imgOperation = (ImageView) itemView.findViewById(R.id.sOperationImage);
            btnFavourite = (Button) itemView.findViewById(R.id.sFavoriteButton);
            btnLike = (Button) itemView.findViewById(R.id.sLikeButton);
            btnReply = (Button) itemView.findViewById(R.id.sReplyButton);
            imgSentiment = (ImageView) itemView.findViewById(R.id.sSentimentImg);
            urlMetadataCardView = (CardView) itemView.findViewById(R.id.sURLMetadataCardView);
            urlMetadataImage = (ImageView) itemView.findViewById(R.id.urlMetadataImage);
            urlMetadataTitle = (TextView) itemView.findViewById(R.id.urlMetadataTitle);
            urlMetadataHost = (TextView) itemView.findViewById(R.id.urlMetadataHost);

            //set onclick listener
            itemView.setOnClickListener(this);
            txtUserUpdateMessage.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Status status = (Status) data.get(getAdapterPosition());
            Intent intent = new Intent(activity, StatusActivity.class);
            intent.putExtra(IntentConstants.STATUS_ID, status.getId());
            activity.startActivity(intent);
            GAnalytics.sendEvent(mTracker, activity.getLocalClassName(),
                    GAnalyticsConstants.CLICK_STATUS_ROW, status.getId(), userId);
        }

        /*
     * Populate metadata from web page (url) mentioned in status text
     */
        public void populateWebPageMetadata(final URLSpan spans[]) {
            //default visibility gone
            urlMetadataCardView.setVisibility(View.GONE);
            if (spans == null || spans.length == 0) {
                return;
            }

            class URLMetadataRetriever extends AsyncTask<String, Integer, OpenGraphMetadata> {
                protected OpenGraphMetadata doInBackground(String... urls) {
                    OpenGraphMetadata openGraphMetadata = null;
                    String url = urls[0];
                    openGraphMetadata = GreenDaoUtils.getOpenGraphMetadata(activity, url);

                    if (openGraphMetadata == null) {
                        openGraphMetadata = MetadataRetriever.getOpenGraphMetadata(url);
                        if (openGraphMetadata != null && openGraphMetadata.getImageURL() != null
                                && openGraphMetadata.getTitle() != null && openGraphMetadata.getUrl() != null) {
                            openGraphMetadata.setWebPageUrl(url);
                            GreenDaoUtils.updateOpenGraphMetadata(activity, openGraphMetadata);
                        } else {
                            openGraphMetadata = null;
                        }
                    }
                    return openGraphMetadata;
                }

                protected void onProgressUpdate(Integer... progress) {
                }

                @DebugLog
                protected void onPostExecute(final OpenGraphMetadata ogMetadata) {

                    if (ogMetadata == null) {
                        return;
                    }

                    urlMetadataCardView.setVisibility(View.VISIBLE);
                    Picasso.with(activity)
                            .load(ogMetadata.getImageURL()).into(urlMetadataImage);
                    urlMetadataTitle.setText(ogMetadata.getTitle());
                    urlMetadataHost.setText(SSUtils.getHost(ogMetadata.getUrl()));

                    //set onclick listener
                    urlMetadataCardView.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse(ogMetadata.getWebPageUrl()));
                            activity.startActivity(intent);
                        }
                    });
                }
            }

            new URLMetadataRetriever().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, spans[0].getURL());
        }
    }

    class UserHolder extends RecyclerView.ViewHolder       /* User Row Holder */ {
        TextView txtUserName;
        TextView txtName;
        ImageView imgUserDP;
        TextView txtUserAboutUser;

        public UserHolder(View itemView) {
            super(itemView);

            txtUserName = (TextView) itemView.findViewById(R.id.ulUserName);
            txtName = (TextView) itemView.findViewById(R.id.ulName);
            imgUserDP = (ImageView) itemView.findViewById(R.id.ulDisplayPic);
            txtUserAboutUser = (TextView) itemView.findViewById(R.id.ulAboutUser);
        }
    }

    class UserNotificationHolder extends RecyclerView.ViewHolder /* Notification Row Holder */ {
        TextView txtNotificationMessage;
        ImageView imgUserDisplayPic;
        TextView txtTimelapsed;

        public UserNotificationHolder(View itemView) {
            super(itemView);

            txtNotificationMessage = (TextView) itemView.findViewById(R.id.nsStatusMessage);
            imgUserDisplayPic = (ImageView) itemView.findViewById(R.id.nsDisplayPic);
            txtTimelapsed = (TextView) itemView.findViewById(R.id.nsTimeElapsed);
        }
    }
}
