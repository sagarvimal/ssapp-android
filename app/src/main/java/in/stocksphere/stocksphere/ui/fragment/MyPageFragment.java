package in.stocksphere.stocksphere.ui.fragment;


import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.analytics.Tracker;
import com.squareup.picasso.Picasso;

import java.util.List;

import hugo.weaving.DebugLog;
import in.stocksphere.stocksphere.Logger;
import in.stocksphere.stocksphere.R;
import in.stocksphere.stocksphere.data.common.SSConstants;
import in.stocksphere.stocksphere.data.common.SSUtils;
import in.stocksphere.stocksphere.data.ftue.FTUEDisplayer;
import in.stocksphere.stocksphere.data.ganalytics.GAnalytics;
import in.stocksphere.stocksphere.data.ganalytics.GAnalyticsConstants;
import in.stocksphere.stocksphere.data.models.Status;
import in.stocksphere.stocksphere.data.models.User;
import in.stocksphere.stocksphere.data.models.UserStatus;
import in.stocksphere.stocksphere.data.rest.APIHelper;
import in.stocksphere.stocksphere.data.rest.RequestObjectCreator;
import in.stocksphere.stocksphere.data.sqlite.GreenDaoUtils;
import in.stocksphere.stocksphere.ui.SharedPreferencesManager;
import in.stocksphere.stocksphere.ui.UpdateUserActivity;
import in.stocksphere.stocksphere.ui.UserListActivity;
import in.stocksphere.stocksphere.ui.custom.IntentConstants;
import in.stocksphere.stocksphere.ui.custom.SSAdapter;
import in.stocksphere.stocksphere.ui.custom.SSProgressViewer;
import in.stocksphere.stocksphere.ui.custom.SSSwipeRefreshLayout;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyPageFragment extends android.support.v4.app.Fragment {

    private SSProgressViewer mpProgressViewer;
    public long USER_ID;
    private User myData;
    private List<Status> statusData;
    private View fragRootView;
    private Context context;
    private RecyclerView mRecyclerView;
    private SSSwipeRefreshLayout mSwipeRefreshLayout = null;
    private Activity activity;
    private TextView mpNoDataView;
    private Tracker mTracker;
    private static final String TAG = MyPageFragment.class.getSimpleName();
    ImageView mpEdit;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        fragRootView = inflater.inflate(R.layout.fragment_my_page, container, false);
        context = getActivity();
        USER_ID = SharedPreferencesManager.getCurrentlyLoggedInUserId(this.getActivity());
        activity = this.getActivity();

        mTracker = GAnalytics.getTracker(getActivity().getApplication());

        mpNoDataView = (TextView)fragRootView.findViewById(R.id.mpNoDataView);

        mRecyclerView = (RecyclerView) fragRootView.findViewById(R.id.mpListView);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        mRecyclerView.setLayoutManager(mLayoutManager);

        //swipe layout
        mSwipeRefreshLayout = (SSSwipeRefreshLayout) fragRootView.findViewById(R.id.mpSwipeLayout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                mSwipeRefreshLayout.setRefreshing(true);
                requestUserStatus(myData.getUserId());
                GAnalytics.sendEvent(mTracker, TAG, GAnalyticsConstants.SWIPE_REFRESH, SSConstants.EMPTY_STRING, USER_ID);
            }
        });

        mpProgressViewer = new SSProgressViewer();

        ///request to get the timeline data for the user
        requestUserInfoByUserId(USER_ID);

        //set onclick listener on edit button
        mpEdit = (ImageView)fragRootView.findViewById(R.id.mpEdit);
        mpEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent updateUserActivity = new Intent(getActivity(), UpdateUserActivity.class);
                startActivity(updateUserActivity);
            }
        });

        //followers button listener
        Button mpFollowers = (Button) fragRootView.findViewById(R.id.mpFollowers);
        mpFollowers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewFollowers();
            }
        });

        //following button listener
        Button mpFollowing = (Button) fragRootView.findViewById(R.id.mpFollowing);
        mpFollowing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewFollowing();
            }
        });

        showFTUE();

        return fragRootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        Logger.log("");
        GAnalytics.sendScreenView(mTracker, TAG);

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onResume() {
        super.onResume();
        Logger.log("");
        GAnalytics.sendScreenView(mTracker, TAG);

    }

    @Override
    public void onPause() {
        super.onPause();
        dismissLoadingOnUIThread();
    }

    @Override
    public void onStop() {
        Logger.log("");
        super.onStop();
        dismissLoadingOnUIThread();
    }

    private void requestUserInfoByUserId(long userId) {
        Logger.log("");
        mpProgressViewer.showLoading(getResources().getString(R.string.loading_message), context);
        APIHelper.getSsapi().getUserByUserId(userId, userId, new Callback<User>() {
            @Override
            public void success(User user, Response response) {
                Logger.log("in Success");
                Logger.log("user.toString() = " + user.toString());

                ///fetched data stored locally
                myData = user;

                ///populate data
                populateUserOnUIThread();
            }

            @Override
            public void failure(RetrofitError error) {
                Logger.log("in failure");
                dismissLoadingOnUIThread();
            }
        });
    }

    void populateUserOnUIThread() {
        /// Execute the UI stuff on the main thread
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            public void run() {
                // UI code goes here...

                /// populate the data fetched
                populateUser();
                /// remove loading
                mpProgressViewer.dismissLoading();
            }
        });
    }

    void dismissLoadingOnUIThread() {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            public void run() {
                mpProgressViewer.dismissLoading();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @DebugLog
    private void populateUser() {
        Logger.log("");
        /// populate the watchlist data
        if (null == myData) {
            Logger.log("WARNING :: userData empty !!");
            return;
        }

        LinearLayout mpLinearLayout = (LinearLayout) fragRootView.findViewById(R.id.mpLinearLayout);
        TextView uName = (TextView) fragRootView.findViewById(R.id.mpName);
        TextView uHandle = (TextView) fragRootView.findViewById(R.id.mpHandle);
        TextView uAboutMe = (TextView) fragRootView.findViewById(R.id.mpAboutMe);
        Button uUpdates = (Button) fragRootView.findViewById(R.id.mpUpdates);
        Button uFollowing = (Button) fragRootView.findViewById(R.id.mpFollowing);
        Button uFollowers = (Button) fragRootView.findViewById(R.id.mpFollowers);
        ImageView mpPic = (ImageView) fragRootView.findViewById(R.id.mpPic);

        mpLinearLayout.setVisibility(View.VISIBLE);
        Picasso.with(context)
                .load(SSUtils.getFacebookProfilePicUrl(myData.getFbAndroidUId()))
                .into(mpPic);
        ///setting text details
        uName.setText(myData.getName());
        uHandle.setText(SSConstants.AT_THE_RATE + myData.getUserName());
        uAboutMe.setText(myData.getAboutUser());
        //uUpdates.setText(String.valueOf(myData.get));///todo: updates-count ???
        uFollowing.setText("Following (" + String.valueOf(myData.getcFollowingUsers()) + ")");
        uFollowers.setText("Followers (" + String.valueOf(myData.getcFollowsers()) + ")");
        uUpdates.setText("Updates (" + String.valueOf(myData.getcStatus()) + ")");

        //populate the status list
        populateUserStatusOnUIThread();
        requestUserStatus(myData.getUserId());
    }

    @DebugLog
    private void requestUserStatus(long userId) {
        Logger.log("");
        long currentlyLoggedInUserId = SharedPreferencesManager.getCurrentlyLoggedInUserId(activity);
        APIHelper.getSsapi().getUserStatusUpdates(RequestObjectCreator.getUserStatusUpdatesObject(userId,currentlyLoggedInUserId), new Callback<UserStatus>() {
            @Override
            public void success(UserStatus userStatus, Response response) {

                Logger.log("in Success of requestUserStatus");

                if (userStatus == null || userStatus.getStatuses() == null) {
                    Logger.log("userStatus or userStatus.getStatuses() null");
                    return;
                }

                mSwipeRefreshLayout.setRefreshing(false);
                GreenDaoUtils.addToUserTimeline(context, userStatus.getStatuses(), myData.getUserId());

                //populate data
                populateUserStatusOnUIThread();
            }

            @Override
            public void failure(RetrofitError error) {
                Logger.log("in Fail of requestUserStatus");
                dismissLoadingOnUIThread();
            }
        });
    }

    private void populateUserStatusOnUIThread() {
        /// Execute the UI stuff on the main thread
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            public void run() {

                statusData = GreenDaoUtils.getUserTimeLineStatus(context, myData.getUserId());
                populateStatus();
            }
        });
    }

    private void populateStatus() {
        Logger.log("");

        if (statusData == null || statusData.isEmpty()) {

            Logger.log("statusData is null");
            mpNoDataView.setVisibility(View.VISIBLE);
            return;
        }

        mpNoDataView.setVisibility(View.GONE);
        SSAdapter adapter = new SSAdapter(activity, R.layout.status_row_item, statusData, mTracker);
        mRecyclerView.setAdapter(adapter);
    }

    private void  viewFollowers(){
        Intent intent = new Intent(context, UserListActivity.class);
        intent.putExtra(IntentConstants.OPERATION, IntentConstants.SHOW_FOLLOWERS);
        intent.putExtra(IntentConstants.USER_ID, myData.getUserId());
        context.startActivity(intent);
    }

    private void  viewFollowing(){
        Intent intent = new Intent(context, UserListActivity.class);
        intent.putExtra(IntentConstants.OPERATION, IntentConstants.SHOW_FOLLOWING);
        intent.putExtra(IntentConstants.USER_ID, myData.getUserId());
        context.startActivity(intent);
    }

    private void showFTUE() {
        FTUEDisplayer.showFTUEOnMyPage(activity, mpEdit);
    }
}
