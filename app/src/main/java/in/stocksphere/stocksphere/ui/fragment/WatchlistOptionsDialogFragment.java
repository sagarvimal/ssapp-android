package in.stocksphere.stocksphere.ui.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.analytics.Tracker;

import hugo.weaving.DebugLog;
import in.stocksphere.stocksphere.Logger;
import in.stocksphere.stocksphere.R;
import in.stocksphere.stocksphere.data.common.SSUtils;
import in.stocksphere.stocksphere.data.ganalytics.GAnalytics;
import in.stocksphere.stocksphere.data.models.Company;
import in.stocksphere.stocksphere.data.models.SSBase;
import in.stocksphere.stocksphere.data.models.WatchlistNotification;
import in.stocksphere.stocksphere.data.rest.APIHelper;
import in.stocksphere.stocksphere.data.rest.RequestObjectCreator;
import in.stocksphere.stocksphere.data.sqlite.GreenDaoUtils;
import in.stocksphere.stocksphere.ui.custom.IntentConstants;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class WatchlistOptionsDialogFragment extends DialogFragment {

    private boolean[] mSelectedItems;
    private String[] mCheckBoxItems;
    private EditText minLimit;
    private EditText maxLimit;
    private LinearLayout linearLayout;
    private View textEntryView;
    long userId;
    String ssSymbol;
    Context context;
    private Tracker mTracker;
    private static final String TAG = WatchlistOptionsDialogFragment.class.getSimpleName();

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        if (getArguments() != null && !getArguments().isEmpty()) {

            ssSymbol = getArguments().getString(IntentConstants.COMPANY_SS_SYMBOL);
            userId = getArguments().getLong(IntentConstants.USER_ID);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        context = getActivity();

        mTracker = GAnalytics.getTracker(getActivity().getApplication());

        textEntryView = getActivity().getLayoutInflater().inflate(R.layout.fragment_watchlist_options, null);
        minLimit = (EditText) textEntryView.findViewById(R.id.opMinLimit);
        maxLimit = (EditText) textEntryView.findViewById(R.id.opMaxLimit);
        linearLayout = (LinearLayout) textEntryView.findViewById(R.id.opLayout);

        mCheckBoxItems = getResources().getStringArray(R.array.WatchlistActionOptions);
        mSelectedItems = new boolean[getResources().getStringArray(R.array.WatchlistActionOptions).length];

        populateWatchlistValues();

        //set checkboxes
        builder.setMultiChoiceItems(mCheckBoxItems, mSelectedItems,
                new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which,
                                        boolean isChecked) {
                        if (isChecked) {
                            Logger.log("Item Selected:" + mCheckBoxItems[which]);

                            //if get notification checkbox is selected then show min max
                            // edit box and check add to watchlist check box
                            if (which == 1) {
                                mSelectedItems[0] = true;
                                ((AlertDialog) dialog).getListView().setItemChecked(0, true);
                                linearLayout.setVisibility(View.VISIBLE);
                            }

                        } else {
                            Logger.log("Item UnSelected:" + mCheckBoxItems[which]);

                            //if add to watchlist checkbox is deselected then hide min max
                            // edit box and deselect get notification checkbox
                            if (which == 0) {
                                mSelectedItems[1] = false;
                                ((AlertDialog) dialog).getListView().setItemChecked(1, false);
                            }

                            //if any checkbox is deselected then hide text min max edit box
                            linearLayout.setVisibility(View.GONE);
                            maxLimit.getText().clear();
                            minLimit.getText().clear();
                        }
                    }
                })
                //set view
                .setView(textEntryView)
                        // Set the action buttons
                .setPositiveButton("DONE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        Logger.log("Done");

                        updateWatchList();
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        Logger.log("CANCELLed");
                    }
                });

        // Create the AlertDialog object and return it
        return builder.create();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getDialog().setCanceledOnTouchOutside(false);

        return getView();
    }

    @Override
    @DebugLog
    public void onStart() {
        super.onStart();
        GAnalytics.sendScreenView(mTracker, TAG);
    }

    @Override
    @DebugLog
    public void onResume() {
        super.onResume();
        GAnalytics.sendScreenView(mTracker, TAG);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @DebugLog
    private void updateWatchList() {

        if (!SSUtils.isNetworkAvailable(context)) {

            SSUtils.displayNoNetworkMsg(context);
            return;
        }

        final boolean isAdd = mSelectedItems[0];
        final boolean isPushNotifEnabled = mSelectedItems[1];
        double minLimitValue = 0;
        double maxLimitValue = 0;

        if (isPushNotifEnabled) {

            if (minLimit.getText().toString().length() > 0) {
                minLimitValue = Double.parseDouble(minLimit.getText().toString());
            } else {
                minLimitValue = 0;
            }
            if (maxLimit.getText().toString().length() > 0) {
                maxLimitValue = Double.parseDouble(maxLimit.getText().toString());
            } else {
                maxLimitValue = 0;
            }
        }

        final double minL = minLimitValue;
        final double maxL = maxLimitValue;

        APIHelper.getSsapi().updateWatchlist(RequestObjectCreator.getUpdateWatchlistObject(userId, ssSymbol, isAdd, minLimitValue, maxLimitValue, isPushNotifEnabled), new Callback<SSBase>() {
            @Override
            @DebugLog
            public void success(SSBase SSBase, Response response) {

                SSUtils.showSnackBarMsg((Activity)context, "Watchlist is updated successfully");

                if (isAdd) {
                    GreenDaoUtils.updateWatchlist(context, ssSymbol, isPushNotifEnabled, minL, maxL);
                } else {
                    //remove from sqlite watchlist
                    GreenDaoUtils.removeStockFromWatchList(context, ssSymbol);
                }
            }

            @Override
            @DebugLog
            public void failure(RetrofitError error) {

                Toast.makeText(context, "Unable to update watchlist for " + ssSymbol, Toast.LENGTH_SHORT).show();
            }
        });
    }

    /*
    populate the watchlist related details to pop up
     */
    @DebugLog
    private void populateWatchlistValues() {

        Company company = GreenDaoUtils.getCompanyFromWatchList(context, ssSymbol);

        if (company == null) {
            return; // company is not added to watchlist
        }

        mSelectedItems[0] = true; // if company is added to watchlist then check the checkbox

        WatchlistNotification watchlistNotification = company.getWlNotification();

        if (watchlistNotification != null && watchlistNotification.getIsPushNotifEnabled()) {

            mSelectedItems[1] = true; // push notification is already enabled
            minLimit.setText(String.valueOf(watchlistNotification.getMinPrice()));
            maxLimit.setText(String.valueOf(watchlistNotification.getMaxPrice()));

            //make layout visible to display min max values fields
            linearLayout.setVisibility(View.VISIBLE);
        }
    }
}
