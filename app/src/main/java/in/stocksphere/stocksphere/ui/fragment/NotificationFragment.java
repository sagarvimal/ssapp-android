package in.stocksphere.stocksphere.ui.fragment;


import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.analytics.Tracker;
import com.parse.FindCallback;
import com.parse.ParseException;

import java.util.List;

import hugo.weaving.DebugLog;
import in.stocksphere.stocksphere.Logger;
import in.stocksphere.stocksphere.R;
import in.stocksphere.stocksphere.data.common.SSNotifUtils;
import in.stocksphere.stocksphere.data.common.SSUtils;
import in.stocksphere.stocksphere.data.ganalytics.GAnalytics;
import in.stocksphere.stocksphere.data.models.AppNotification;
import in.stocksphere.stocksphere.ui.SharedPreferencesManager;
import in.stocksphere.stocksphere.ui.custom.SSAdapter;
import in.stocksphere.stocksphere.ui.custom.SSProgressViewer;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationFragment extends android.support.v4.app.Fragment {

    private SSProgressViewer nProgressViewer;
    private List<AppNotification> nAppNotifData;
    private Context nContext;
    private View fragRootView;
    private long USER_ID;
    private RecyclerView mRecyclerView;
    private Activity activity;
    private TextView nNoDataView;
    private Tracker mTracker;
    private static final String TAG = NotificationFragment.class.getSimpleName();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        fragRootView = inflater.inflate(R.layout.fragment_notifications, container, false);
        nContext = getActivity();
        USER_ID = SharedPreferencesManager.getCurrentlyLoggedInUserId(this.getActivity());
        activity = this.getActivity();

        mTracker = GAnalytics.getTracker(getActivity().getApplication());

        nNoDataView = (TextView)fragRootView.findViewById(R.id.nNoDataView);

        mRecyclerView = (RecyclerView) fragRootView.findViewById(R.id.nListView);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(nContext);
        mRecyclerView.setLayoutManager(mLayoutManager);

        nProgressViewer = new SSProgressViewer();

        ///fetch notification
        requestNotificationForUser(USER_ID);

        return fragRootView;
    }

    @Override
    public void onStop() {
        Logger.log("");
        nProgressViewer.dismissLoading();

        super.onStop();
    }

    @Override
    @DebugLog
    public void onStart() {
        super.onStart();
        GAnalytics.sendScreenView(mTracker, TAG);
    }

    @Override
    @DebugLog
    public void onResume() {
        super.onResume();
        GAnalytics.sendScreenView(mTracker, TAG);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }


    private void requestNotificationForUser(long userId) {
        Logger.log("");

        if (!SSUtils.isNetworkAvailable(activity)) {
            SSUtils.displayNoNetworkMsg(activity);
            return;
        }

        nProgressViewer.showLoading(getResources().getString(R.string.loading_message), nContext);
        SSNotifUtils.getAppNotificationQuery(userId).findInBackground(new FindCallback<AppNotification>() {
            @Override
            public void done(List<AppNotification> appNotifications, ParseException e) {

                if (null == e) {
                    Logger.log("SUCCESS (Notification fetch)");
                    /// feed data received
                    nAppNotifData = appNotifications;//appNotifications.toArray(new AppNotification[appNotifications.size()]);

                    ///populate data
                    populateNotificationsOnUIThread();

                } else {
                    Logger.log("FAILED (Notification Fetch)");
                    dismissLoadingOnUIThread();
                }
            }
        });
    }

    void populateNotificationsOnUIThread() {
        /// Execute the UI stuff on the main thread
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            public void run() {
                // UI code goes here...

                /// populate the data fetched
                populateNotifications();
                /// remove loading
                nProgressViewer.dismissLoading();
            }
        });
    }

    void dismissLoadingOnUIThread() {
        /// Execute the UI stuff on the main thread
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            public void run() {
                // UI code goes here...

                /// remove loading
                nProgressViewer.dismissLoading();
            }
        });
    }

    private void populateNotifications() {
        Logger.log("");
        /// populate the watchlist data
        if (null == nAppNotifData || nAppNotifData.isEmpty()) {
            Logger.log("WARNING :: nAppNotifData empty !!");
            nNoDataView.setVisibility(View.VISIBLE);
            return;
        }
        nNoDataView.setVisibility(View.GONE);
        SSAdapter adapter = new SSAdapter(activity,
                R.layout.notif_status_row_item, nAppNotifData, mTracker);
        mRecyclerView.setAdapter(adapter);
    }
}
