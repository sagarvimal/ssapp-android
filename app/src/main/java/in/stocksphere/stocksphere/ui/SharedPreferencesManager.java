package in.stocksphere.stocksphere.ui;

import android.content.Context;

import com.google.gson.Gson;

import in.stocksphere.stocksphere.data.common.SSConstants;
import in.stocksphere.stocksphere.data.models.User;

/**
 * Created by srishti on 04/06/15.
 */
public class SharedPreferencesManager {

    /**
     * for local saves, preferences
     */
    private static final String SS_SHARED_PREFS_IDENTIFIER = "STOCK_SPHERE_SHARED_PREFERENCES";
    private static final String LOGGED_IN_USER_IDENTIFIER = "LOGGED_IN_USER";
    private static final String INDEX_PRICE_NOTIF_SEND_TIME = "INDEX_PRICE_NOTIF_SEND_TIME";
    private static final String TRADING_APP_PACKAGE_NAME = "TRADING_APP_PACKAGE_NAME";
    private static final String COMPANY_NAMES_DOWNLOAD_TIME = "COMPANY_NAMES_DOWNLOAD_TIME";
    private static final String TRENDING_COMPANY_NOTIF = "TRENDING_COMPANY_NOTIF";
    private static final String NEWS_DOWNLOAD_TIME = "NEWS_DOWNLOAD_TIME";
    private static android.content.SharedPreferences sharedPrefs;
    private static android.content.SharedPreferences.Editor editor;

    public static void setCurrentlyLoggedInUser(Context context, User user) {

        sharedPrefs = context.getSharedPreferences(SS_SHARED_PREFS_IDENTIFIER, Context.MODE_PRIVATE);
        editor = sharedPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(user);
        editor.putString(LOGGED_IN_USER_IDENTIFIER, json);
        editor.apply();
    }

    public static User getCurrentlyLoggedInUser(Context context) {

        sharedPrefs = context.getSharedPreferences(SS_SHARED_PREFS_IDENTIFIER, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPrefs.getString(LOGGED_IN_USER_IDENTIFIER, "");

        return gson.fromJson(json, User.class);
    }

    public static long getCurrentlyLoggedInUserId(Context context) {

        sharedPrefs = context.getSharedPreferences(SS_SHARED_PREFS_IDENTIFIER, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPrefs.getString(LOGGED_IN_USER_IDENTIFIER, "");
        User user = gson.fromJson(json, User.class);

        if (user != null) {
            return user.getUserId();
        }
        return SSConstants.DEFAULT_USER_ID;
    }

    public static String getCurrentlyLoggedInUserFullName(Context context) {

        sharedPrefs = context.getSharedPreferences(SS_SHARED_PREFS_IDENTIFIER, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPrefs.getString(LOGGED_IN_USER_IDENTIFIER, "");
        User user = gson.fromJson(json, User.class);

        if (user == null) {
            return SSConstants.EMPTY_STRING;
        }

        return user.getName();
    }

    public static void clearUserData(Context context) {

        sharedPrefs = context.getSharedPreferences(SS_SHARED_PREFS_IDENTIFIER, Context.MODE_PRIVATE);
        editor = sharedPrefs.edit();
        editor.remove(LOGGED_IN_USER_IDENTIFIER);
        editor.apply();
    }

    /* Stores the last index (nifty/sensex) point notification time */
    public static void saveLastIndexNotifTime(Context context, long millis) {
        sharedPrefs = context.getSharedPreferences(SS_SHARED_PREFS_IDENTIFIER, Context.MODE_PRIVATE);
        editor = sharedPrefs.edit();
        editor.putLong(INDEX_PRICE_NOTIF_SEND_TIME, millis);
        editor.apply();
    }

    /* returns the last index (nifty/sensex) point notification time */
    public static long getLastIndexNotifTime(Context context) {
        sharedPrefs = context.getSharedPreferences(SS_SHARED_PREFS_IDENTIFIER, Context.MODE_PRIVATE);
        return sharedPrefs.getLong(INDEX_PRICE_NOTIF_SEND_TIME, 0);
    }

    /* Stores the Selected default trading app package name */
    public static void saveTradingAppPackageName(Context context, String appPackageName) {
        sharedPrefs = context.getSharedPreferences(SS_SHARED_PREFS_IDENTIFIER, Context.MODE_PRIVATE);
        editor = sharedPrefs.edit();
        editor.putString(TRADING_APP_PACKAGE_NAME, appPackageName);
        editor.apply();
    }

    /* returns the Selected default trading app package name */
    public static String getTradingAppPackageName(Context context) {
        sharedPrefs = context.getSharedPreferences(SS_SHARED_PREFS_IDENTIFIER, Context.MODE_PRIVATE);
        return sharedPrefs.getString(TRADING_APP_PACKAGE_NAME, SSConstants.EMPTY_STRING);
    }

    /* Stores the time when company names are downloaded */
    public static void saveLastCompanyNameDownloadTime(Context context, long millis) {
        sharedPrefs = context.getSharedPreferences(SS_SHARED_PREFS_IDENTIFIER, Context.MODE_PRIVATE);
        editor = sharedPrefs.edit();
        editor.putLong(COMPANY_NAMES_DOWNLOAD_TIME, millis);
        editor.apply();
    }

    /* returns the the time when company names are downloaded */
    public static long getLastCompanyNameDownloadTime(Context context) {
        sharedPrefs = context.getSharedPreferences(SS_SHARED_PREFS_IDENTIFIER, Context.MODE_PRIVATE);
        return sharedPrefs.getLong(COMPANY_NAMES_DOWNLOAD_TIME, 0);
    }

    /* Stores the time when trenading company notif is sent */
    public static void saveTrendingCompanyNotifTime(Context context, long millis) {
        sharedPrefs = context.getSharedPreferences(SS_SHARED_PREFS_IDENTIFIER, Context.MODE_PRIVATE);
        editor = sharedPrefs.edit();
        editor.putLong(TRENDING_COMPANY_NOTIF, millis);
        editor.apply();
    }

    /* returns the the time when company names are downloaded */
    public static long getTrendingCompanyNotifTime(Context context) {
        sharedPrefs = context.getSharedPreferences(SS_SHARED_PREFS_IDENTIFIER, Context.MODE_PRIVATE);
        return sharedPrefs.getLong(TRENDING_COMPANY_NOTIF, 0);
    }

    /* returns the the time when news are downloaded */
    public static long getLastNewsDownloadTime(Context context) {
        sharedPrefs = context.getSharedPreferences(SS_SHARED_PREFS_IDENTIFIER, Context.MODE_PRIVATE);
        return sharedPrefs.getLong(NEWS_DOWNLOAD_TIME, 0);
    }

    /* Stores the time when company names are downloaded */
    public static void saveLastNewsDownloadTime(Context context, long millis) {
        sharedPrefs = context.getSharedPreferences(SS_SHARED_PREFS_IDENTIFIER, Context.MODE_PRIVATE);
        editor = sharedPrefs.edit();
        editor.putLong(NEWS_DOWNLOAD_TIME, millis);
        editor.apply();
    }
}
