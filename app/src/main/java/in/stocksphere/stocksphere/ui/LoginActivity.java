package in.stocksphere.stocksphere.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

import hugo.weaving.DebugLog;
import in.stocksphere.stocksphere.Logger;
import in.stocksphere.stocksphere.R;
import in.stocksphere.stocksphere.data.common.SSConstants;
import in.stocksphere.stocksphere.data.common.SSNotifUtils;
import in.stocksphere.stocksphere.data.common.SSUtils;
import in.stocksphere.stocksphere.data.fabric.FabricUtils;
import in.stocksphere.stocksphere.data.ganalytics.GAnalytics;
import in.stocksphere.stocksphere.data.ganalytics.GAnalyticsConstants;
import in.stocksphere.stocksphere.data.models.CompanyList;
import in.stocksphere.stocksphere.data.models.User;
import in.stocksphere.stocksphere.data.rest.APIHelper;
import in.stocksphere.stocksphere.data.rest.RequestObjectCreator;
import in.stocksphere.stocksphere.data.sqlite.GreenDaoUtils;
import in.stocksphere.stocksphere.ui.custom.SSProgressViewer;
import in.stocksphere.stocksphere.ui.util.SystemUiHider;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 *
 * @see SystemUiHider
 */
public class LoginActivity extends Activity {

    CallbackManager callbackManager;
    Activity activity;
    Button fbLoginButton;
    User user;
    SSProgressViewer progressViewer;
    Toast loginFailMsg;
    private Tracker mTracker;
    private static final String TAG = LoginActivity.class.getSimpleName();

    @Override
    @DebugLog
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);
        activity = this;
        progressViewer = new SSProgressViewer();
        loginFailMsg = Toast.makeText(activity, "Unable to fetch data from Facebook. Please again.", Toast.LENGTH_SHORT);

        SSUtils.showHashKey(this);
        mTracker = GAnalytics.getTracker(getApplication());

        fbLoginButton = (Button) findViewById(R.id.loginFB);
        fbLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                progressViewer.showLoading("Connecting to Facebook...", activity);

                List<String> permissions = Arrays.asList("email", "user_friends", "user_birthday");
                LoginManager.getInstance().logInWithReadPermissions(activity, permissions);
                callbackManager = CallbackManager.Factory.create();
                LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                    @Override
                    @DebugLog
                    public void onSuccess(LoginResult loginResult) {

                        progressViewer.dismissLoading();
                        fbLoginButton.setVisibility(View.GONE);
                        requestUserDataFromFacebook();
                    }

                    @Override
                    @DebugLog
                    public void onCancel() {
                        progressViewer.dismissLoading();
                        Logger.log("FACEBOOK login cancel");
                    }

                    @Override
                    @DebugLog
                    public void onError(FacebookException e) {
                        progressViewer.dismissLoading();
                        Logger.log("FACEBOOK login Exception");
                        FabricUtils.logException(e);
                    }
                });
            }
        });

        if (isFBLoggedIn() && SharedPreferencesManager.getCurrentlyLoggedInUserId(this) != SSConstants.DEFAULT_USER_ID) {

            showHome();
        }
    }

    @Override
    @DebugLog
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    @DebugLog
    protected void onStart() {
        super.onStart();
        GAnalytics.sendScreenView(mTracker, TAG);
    }

    @Override
    @DebugLog
    protected void onPause() {
        super.onPause();

        // Facebook Logs 'app deactivate' App Event.
        AppEventsLogger.deactivateApp(this);
    }

    @Override
    @DebugLog
    protected void onStop() {
        super.onStop();
    }

    @Override
    @DebugLog
    protected void onResume() {
        super.onResume();

        // Facebook - Logs 'install' and 'app activate' App Events.
        AppEventsLogger.activateApp(this);
        GAnalytics.sendScreenView(mTracker, TAG);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

    }

    @DebugLog
    private void showHome() {

        Intent mainActivityHome = new Intent(this, MainActivity.class);
        mainActivityHome.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
        startActivity(mainActivityHome);
        finish();
    }

    @DebugLog
    private boolean isFBLoggedIn() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken != null;
    }

    @DebugLog
    private void requestUserDataFromFacebook() {

        GraphRequest request = GraphRequest.newMeRequest(
                AccessToken.getCurrentAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(
                            JSONObject object,
                            GraphResponse response) {

                        try {
                            long fb_id = 0;
                            String fb_name = null;
                            String fb_email = null;
                            String fb_birthday = null;
                            String fb_gender = null;

                            if (object.has("id")) {
                                fb_id = object.getLong("id");
                            }
                            if (object.has("name")) {
                                fb_name = object.getString("name");
                            }
                            if (object.has("email")) {
                                fb_email = object.getString("email");
                            }
                            if (object.has("birthday")) {
                                fb_birthday = object.getString("birthday");
                            }
                            if (object.has("gender")) {
                                fb_gender = object.getString("gender");
                            }

                            Logger.log("fb_id >> " + fb_id);
                            Logger.log("fb_name >> " + fb_name);
                            Logger.log("fb_gender >> " + fb_gender);
                            Logger.log("fb_email >> " + fb_email);
                            Logger.log("fb_birthday >> " + fb_birthday);

                            user = new User();
                            user.setFbAndroidUId(fb_id);
                            user.setName(fb_name);
                            user.setGender(fb_gender);
                            user.setEmail(fb_email);
                            user.setDob(fb_birthday);

                            createUser();
                        } catch (Exception e) {
                            FabricUtils.logException(e);
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,birthday,gender");
        request.setParameters(parameters);
        request.executeAsync();
    }

    @DebugLog
    private void createUser() {

        if (user == null) {

            loginFailMsg.show();
            doLogout();
            return;
        }

        showProgressDialogOnUI();
        APIHelper.getSsapi().addUser(RequestObjectCreator.addUserObject(user.getFbAndroidUId()
                , user.getName(), user.getGender(), user.getEmail(), user.getDob()), new Callback<User>() {
            @Override
            @DebugLog
            public void success(User user, Response response) {

                SharedPreferencesManager.setCurrentlyLoggedInUser(activity, user);
                SSNotifUtils.registerUserIdForPushNotification(user.getUserId());
                downLoadCompanyNames();
                GAnalytics.sendEvent(mTracker, TAG, GAnalyticsConstants.LOGIN_SUCCESS, SSConstants.EMPTY_STRING, user.getUserId());
            }

            @Override
            @DebugLog
            public void failure(RetrofitError error) {

                hideProgressDialogOnUI();
                loginFailMsg.show();
                doLogout();
                FabricUtils.logException(error);
                GAnalytics.sendEvent(mTracker, TAG, GAnalyticsConstants.LOGIN_FAILED, SSConstants.EMPTY_STRING, 0);
            }
        });
    }

    @DebugLog
    void showProgressDialogOnUI() {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            public void run() {
                progressViewer.showCreateUserLoading(activity);
            }
        });
    }

    @DebugLog
    void hideProgressDialogOnUI() {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            public void run() {
                progressViewer.dismissLoading();
            }
        });
    }

    /*
    It first checks if company names are already present otherwise download it
     */
    @DebugLog
    private void downLoadCompanyNames() {

        if (!GreenDaoUtils.isCompanyNamesPresent(activity)) {

            APIHelper.getSsapi().getAllCompanyNames(RequestObjectCreator.getAllCompanyNamesObject(), new Callback<CompanyList>() {
                @Override
                public void success(final CompanyList companyList, Response response) {

                    if (companyList == null || companyList.getCompanies() == null || companyList.getCompanies().isEmpty()) {

                        Logger.log("null in all company names");
                        hideProgressDialogOnUI();
                        loginFailMsg.show();
                        doLogout();
                        return;
                    }

                    //save data in new thread
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            GreenDaoUtils.addToCompanyNames(activity, companyList.getCompanies());
                            hideProgressDialogOnUI();
                            showHome();
                        }
                    }).start();
                }

                @Override
                public void failure(RetrofitError error) {

                    Logger.log("RetrofitError in all company names");
                    hideProgressDialogOnUI();
                    loginFailMsg.show();
                    doLogout();
                    FabricUtils.logException(error);
                }
            });
        } else {
            hideProgressDialogOnUI();
            showHome();
        }
    }

    private void doLogout() {

        //clear user data
        SharedPreferencesManager.clearUserData(activity);
        //unSubscribePushNotification
        SSNotifUtils.unSubscribePushNotification();
        //log out from facebook
        LoginManager.getInstance().logOut();
    }
}
