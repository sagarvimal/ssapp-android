package in.stocksphere.stocksphere.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.analytics.Tracker;

import java.util.List;

import hugo.weaving.DebugLog;
import in.stocksphere.stocksphere.Logger;
import in.stocksphere.stocksphere.R;
import in.stocksphere.stocksphere.data.common.SSUtils;
import in.stocksphere.stocksphere.data.fabric.FabricUtils;
import in.stocksphere.stocksphere.data.ganalytics.GAnalytics;
import in.stocksphere.stocksphere.data.models.User;
import in.stocksphere.stocksphere.data.models.UserList;
import in.stocksphere.stocksphere.data.rest.APIHelper;
import in.stocksphere.stocksphere.data.rest.RequestObjectCreator;
import in.stocksphere.stocksphere.ui.custom.IntentConstants;
import in.stocksphere.stocksphere.ui.custom.SSAdapter;
import in.stocksphere.stocksphere.ui.custom.SSProgressViewer;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class UserListActivity extends ActionBarActivity {

    private String operation;
    Activity activity;
    RecyclerView mRecyclerView;
    SSProgressViewer progressViewer;
    private Toolbar toolbar;
    private long USER_ID;
    private Tracker mTracker;
    private static final String TAG = UserListActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);
        activity = this;
        USER_ID = SharedPreferencesManager.getCurrentlyLoggedInUserId(this);
        mRecyclerView = (RecyclerView) this.findViewById(R.id.userListView);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(activity);
        mRecyclerView.setLayoutManager(mLayoutManager);
        progressViewer = new SSProgressViewer();

        mTracker = GAnalytics.getTracker(getApplication());

        // Set a Toolbar to replace the ActionBar.
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_back));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Intent intent = getIntent();
        operation = intent.getStringExtra(IntentConstants.OPERATION);
        long userId = intent.getLongExtra(IntentConstants.USER_ID, 0);
        String ssSymbol = intent.getStringExtra(IntentConstants.COMPANY_SS_SYMBOL);
        String statusId = intent.getStringExtra(IntentConstants.STATUS_ID);

        if (operation.equals(IntentConstants.SHOW_FOLLOWERS)) {
            setTitle("Followers");
            requestUserFollowers(userId);
            GAnalytics.sendEvent(mTracker, TAG, "Followers", String.valueOf(userId), USER_ID);
        }
        else if (operation.equals(IntentConstants.SHOW_FOLLOWING)) {
            setTitle("Following");
            requestUserFollowings(userId);
            GAnalytics.sendEvent(mTracker, TAG, "Following", String.valueOf(userId), USER_ID);
        }
        else if (operation.equals(IntentConstants.SHOW_WATCHING_USERS)) {
            setTitle("Watching");
            requestWatchingUsers(ssSymbol);
            GAnalytics.sendEvent(mTracker, TAG, "Watching", ssSymbol, USER_ID);
        }
        else if (operation.equals(IntentConstants.SHOW_STATUS_LIKED_USERS)) {
            setTitle("Liked By");
            requestStatusLikedUsers(statusId);
            GAnalytics.sendEvent(mTracker, TAG, "Liked By", statusId, USER_ID);
        }
        else if (operation.equals(IntentConstants.SHOW_STATUS_FAVOURITED_USERS)) {
            setTitle("Favourited By");
            requestStatusFavouritedUsers(statusId);
            GAnalytics.sendEvent(mTracker, TAG, "Favourited By", statusId, USER_ID);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Logger.log("");
        GAnalytics.sendScreenView(mTracker, TAG);
    }

    @Override
    @DebugLog
    public void onStart() {
        super.onStart();
        GAnalytics.sendScreenView(mTracker, TAG);
    }

    @DebugLog
    void showProgressDialogOnUI() {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            public void run() {
                progressViewer.showLoading(getResources().getString(R.string.loading_message), activity);
            }
        });
    }

    @DebugLog
    void hideProgressDialogOnUI() {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            public void run() {
                progressViewer.dismissLoading();
            }
        });
    }

    @DebugLog
    private void populateUserListOnUI(List<User> users) {
        if (users == null) {
            Logger.log("User List is is null");
            return;
        }

        SSAdapter adapter = new SSAdapter(this, R.layout.user_row_item, users, mTracker);
        mRecyclerView.setAdapter(adapter);
    }

    @DebugLog
    private void requestUserFollowers(long userId) {

        //check if internet connected
        if (!SSUtils.isNetworkAvailable(this)) {
            SSUtils.displayNoNetworkMsg(this);
            return;
        }
        showProgressDialogOnUI();

        APIHelper.getSsapi().getUserFollowers(RequestObjectCreator.getUserFollowerObject(userId), new Callback<UserList>() {
            @Override
            public void success(UserList userList, Response response) {

                hideProgressDialogOnUI();
                if (userList != null) {
                    populateUserListOnUI(userList.getFollowers());
                }
            }

            @Override
            public void failure(RetrofitError error) {
                hideProgressDialogOnUI();
                FabricUtils.logException(error);
                Toast.makeText(activity, "Failed to get users", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

    @DebugLog
    private void requestUserFollowings(long userId) {

        //check if internet connected
        if (!SSUtils.isNetworkAvailable(this)) {
            SSUtils.displayNoNetworkMsg(this);
            return;
        }
        showProgressDialogOnUI();

        APIHelper.getSsapi().getUserFollowing(RequestObjectCreator.getUserFollowingObject(userId), new Callback<UserList>() {
            @Override
            public void success(UserList userList, Response response) {

                hideProgressDialogOnUI();
                if (userList != null) {
                    populateUserListOnUI(userList.getFollowingUsers());
                }
            }

            @Override
            public void failure(RetrofitError error) {
                hideProgressDialogOnUI();
                FabricUtils.logException(error);
                Toast.makeText(activity, "Failed to get users", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

    @DebugLog
    private void requestWatchingUsers(String ssSymbol) {

        //check if internet connected
        if (!SSUtils.isNetworkAvailable(this)) {
            SSUtils.displayNoNetworkMsg(this);
            return;
        }
        showProgressDialogOnUI();

        APIHelper.getSsapi().getWatchingUsers(RequestObjectCreator.getWatchingUsersObject(ssSymbol), new Callback<UserList>() {
            @Override
            public void success(UserList userList, Response response) {

                hideProgressDialogOnUI();
                if (userList != null) {
                    populateUserListOnUI(userList.getWatchingUsers());
                }
            }

            @Override
            public void failure(RetrofitError error) {
                hideProgressDialogOnUI();
                FabricUtils.logException(error);
                Toast.makeText(activity, "Failed to get users", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

    @DebugLog
    private void requestStatusLikedUsers(String statusId) {

        //check if internet connected
        if (!SSUtils.isNetworkAvailable(this)) {
            SSUtils.displayNoNetworkMsg(this);
            return;
        }
        showProgressDialogOnUI();

        APIHelper.getSsapi().getStatusLikedUsers(RequestObjectCreator.getStatusLikedUsersObject(statusId), new Callback<UserList>() {
            @Override
            public void success(UserList userList, Response response) {

                hideProgressDialogOnUI();
                if (userList != null) {
                    populateUserListOnUI(userList.getStatusLikedUsers());
                }
            }

            @Override
            public void failure(RetrofitError error) {
                hideProgressDialogOnUI();
                FabricUtils.logException(error);
                Toast.makeText(activity, "Failed to get users", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

    @DebugLog
    private void requestStatusFavouritedUsers(String statusId) {

        //check if internet connected
        if (!SSUtils.isNetworkAvailable(this)) {
            SSUtils.displayNoNetworkMsg(this);
            return;
        }
        showProgressDialogOnUI();

        APIHelper.getSsapi().getStatusFavouritedUsers(RequestObjectCreator.getStatusFavouritedUsersObject(statusId), new Callback<UserList>() {
            @Override
            public void success(UserList userList, Response response) {

                hideProgressDialogOnUI();
                if (userList != null) {
                    populateUserListOnUI(userList.getStatusFavouritedUsers());
                }
            }

            @Override
            public void failure(RetrofitError error) {
                hideProgressDialogOnUI();
                FabricUtils.logException(error);
                Toast.makeText(activity, "Failed to get users", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }
}
