package in.stocksphere.stocksphere.ui.custom;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by srishti on 20/06/15.
 */
public class SSProgressViewer {

    private ProgressDialog dialog;
    private boolean isLoading;

    public void showLoading(String mssg, Context context){

        if(isLoading){
            return;
        }

        if(dialog != null){
            dialog = null;
        }

        dialog = new ProgressDialog(context);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setMessage(mssg);
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        isLoading = true;
    }

    public void showCreateUserLoading(Context context){

        if(isLoading){
            return;
        }

        if(dialog != null){
            dialog = null;
        }

        dialog = new ProgressDialog(context);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setMessage("Fetching user details from server...");
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        isLoading = true;
    }

    public boolean isLoading(){
        return isLoading;
    }

    public void dismissLoading(){

        if(!isLoading){
            return;
        }

        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
            dialog = null;
        }

        isLoading = false;
    }
}
