package in.stocksphere.stocksphere.ui;


import android.app.Activity;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.Tracker;
import com.squareup.picasso.Picasso;

import java.util.List;

import hugo.weaving.DebugLog;
import in.stocksphere.stocksphere.Logger;
import in.stocksphere.stocksphere.R;
import in.stocksphere.stocksphere.data.aspect.DisplayOnScreen;
import in.stocksphere.stocksphere.data.common.SSConstants;
import in.stocksphere.stocksphere.data.common.SSNotifUtils;
import in.stocksphere.stocksphere.data.common.SSUtils;
import in.stocksphere.stocksphere.data.ftue.FTUEDisplayer;
import in.stocksphere.stocksphere.data.ganalytics.GAnalytics;
import in.stocksphere.stocksphere.data.ganalytics.GAnalyticsConstants;
import in.stocksphere.stocksphere.data.models.SSBase;
import in.stocksphere.stocksphere.data.models.Status;
import in.stocksphere.stocksphere.data.models.User;
import in.stocksphere.stocksphere.data.models.UserStatus;
import in.stocksphere.stocksphere.data.rest.APIHelper;
import in.stocksphere.stocksphere.data.rest.RequestObjectCreator;
import in.stocksphere.stocksphere.data.sqlite.GreenDaoUtils;
import in.stocksphere.stocksphere.ui.custom.IntentConstants;
import in.stocksphere.stocksphere.ui.custom.SSAdapter;
import in.stocksphere.stocksphere.ui.custom.SSProgressViewer;
import in.stocksphere.stocksphere.ui.custom.SSSwipeRefreshLayout;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserActivity extends ActionBarActivity {

    private long USER_ID;
    private long CURRENT_USER_ID;
    private SSProgressViewer upProgressViewer;
    private User userData;
    private List<Status> statusData;
    private RecyclerView mRecyclerView;
    private SSSwipeRefreshLayout mSwipeRefreshLayout = null;
    private Activity activity;
    private TextView upNoDataView;
    private Tracker mTracker;
    private static final String TAG = UserActivity.class.getSimpleName();
    ImageView upFollow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        activity = this;

        mTracker = GAnalytics.getTracker(getApplication());

        Intent intent = getIntent();
        USER_ID = intent.getLongExtra("USER_ID", 0);
        CURRENT_USER_ID = SharedPreferencesManager.getCurrentlyLoggedInUserId(activity);

        upNoDataView = (TextView)this.findViewById(R.id.upNoDataView);

        mRecyclerView = (RecyclerView) this.findViewById(R.id.upListView);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(activity);
        mRecyclerView.setLayoutManager(mLayoutManager);

        //swipe layout
        mSwipeRefreshLayout = (SSSwipeRefreshLayout) this.findViewById(R.id.upSwipeLayout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                mSwipeRefreshLayout.setRefreshing(true);
                requestUserStatus(USER_ID);
                GAnalytics.sendEvent(mTracker, TAG, GAnalyticsConstants.SWIPE_REFRESH, String.valueOf(USER_ID), CURRENT_USER_ID);
            }
        });

        upProgressViewer = new SSProgressViewer();

        ///request to get the timeline data for the user
        requestUserInfoByUserId(USER_ID);

        //follow button listener
        upFollow = (ImageView) findViewById(R.id.upFollow);
        upFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                followUser();
            }
        });

        //followers button listener
        Button upFollowers = (Button) findViewById(R.id.upFollowers);
        upFollowers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewFollowers();
            }
        });

        //following button listener
        Button upFollowing = (Button) findViewById(R.id.upFollowing);
        upFollowing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewFollowing();
            }
        });

        showFTUE();
    }

    @Override
    public void onResume() {
        super.onResume();
        Logger.log("");
        GAnalytics.sendScreenView(mTracker, TAG);
    }

    @Override
    @DebugLog
    public void onStart() {
        super.onStart();
        GAnalytics.sendScreenView(mTracker, TAG);
    }

    @Override
    public void onStop() {
        Logger.log("");
        super.onStop();
        dismissLoadingOnUIThread();
    }

    @Override
    public void onPause() {
        super.onPause();
        dismissLoadingOnUIThread();
    }

    @DisplayOnScreen
    private void requestUserInfoByUserId(long userId) {
        Logger.log("");
        upProgressViewer.showLoading(getResources().getString(R.string.loading_message), activity);
        //check if internet connected
        if (!SSUtils.isNetworkAvailable(this)) {

            SSUtils.displayNoNetworkMsg(this);
            finish();
            return;
        }
        APIHelper.getSsapi().getUserByUserId(userId, CURRENT_USER_ID, new Callback<User>() {
            @Override
            public void success(User user, Response response) {
                Logger.log("in Success");
                Logger.log("user.toString() = " + user.toString());

                ///fetched data stored locally
                userData = user;

                ///populate data
                populateUserOnUIThread();
            }

            @Override
            public void failure(RetrofitError error) {
                Logger.log("in failure");
                dismissLoadingOnUIThread();
                Toast.makeText(activity, "Details not available", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

    void populateUserOnUIThread() {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            public void run() {
                populateUser();
                upProgressViewer.dismissLoading();
            }
        });
    }

    void dismissLoadingOnUIThread() {
        /// Execute the UI stuff on the main thread
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            public void run() {
                upProgressViewer.dismissLoading();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @DebugLog
    @DisplayOnScreen
    private void populateUser() {
        Logger.log("");
        /// populate the watchlist data
        if (null == userData) {
            Logger.log("WARNING :: userData empty !!");
            return;
        }

        LinearLayout upLinearLayout = (LinearLayout) this.findViewById(R.id.upLinearLayout);
        TextView uName = (TextView) this.findViewById(R.id.upName);
        TextView uHandle = (TextView) this.findViewById(R.id.upHandle);
        TextView uAboutMe = (TextView) this.findViewById(R.id.upAboutMe);
        Button uFollowing = (Button) this.findViewById(R.id.upFollowing);
        Button uFollowers = (Button) this.findViewById(R.id.upFollowers);
        Button uUpdates = (Button) this.findViewById(R.id.upUpdates);
        ImageView upPic = (ImageView) this.findViewById(R.id.upPic);
        ImageView upFollow = (ImageView) this.findViewById(R.id.upFollow);
        ImageView upActionBack = (ImageView) this.findViewById(R.id.upActionBack);
        ImageView upPostStatus = (ImageView) this.findViewById(R.id.upPostStatus);

        upLinearLayout.setVisibility(View.VISIBLE);
        Picasso.with(activity)
                .load(SSUtils.getFacebookProfilePicUrl(userData.getFbAndroidUId()))
                .into(upPic);
        ///setting text details
        uName.setText(userData.getName());
        uHandle.setText(SSConstants.AT_THE_RATE + userData.getUserName());
        uAboutMe.setText(userData.getAboutUser());
        if (userData.getIsCallingUserFollowing()) {
            upFollow.setBackgroundResource(R.drawable.ic_user_follow_done);
        } else {
            upFollow.setBackgroundResource(R.drawable.ic_user_follow);
        }

        uFollowing.setText("Following (" + String.valueOf(userData.getcFollowingUsers()) + ")");
        uFollowers.setText("Followers (" + String.valueOf(userData.getcFollowsers()) + ")");
        uUpdates.setText("Updates (" + String.valueOf(userData.getcStatus()) + ")");

        upActionBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        upPostStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPostDialog();
            }
        });

        //populate the status list
        populateUserStatusOnUIThread();
        requestUserStatus(USER_ID);
    }

    @DebugLog
    @DisplayOnScreen
    private void requestUserStatus(long userId) {
        Logger.log("");
        APIHelper.getSsapi().getUserStatusUpdates(RequestObjectCreator.getUserStatusUpdatesObject(userId,CURRENT_USER_ID), new Callback<UserStatus>() {
            @Override
            public void success(UserStatus userStatus, Response response) {

                Logger.log("in Success of requestUserStatus");

                if (userStatus == null && userStatus.getStatuses() == null) {
                    Logger.log("userStatus or userStatus.getStatuses() null");
                    return;
                }

                GreenDaoUtils.addToUserTimeline(activity, userStatus.getStatuses(), USER_ID);

                //populate data
                populateUserStatusOnUIThread();
            }

            @Override
            @DebugLog
            public void failure(RetrofitError error) {
                Logger.log("in Fail of requestUserStatus");
                dismissLoadingOnUIThread();
            }
        });
    }

    private void populateUserStatusOnUIThread() {
        /// Execute the UI stuff on the main thread
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            public void run() {

                mSwipeRefreshLayout.setRefreshing(false);
                statusData = GreenDaoUtils.getUserTimeLineStatus(activity, USER_ID);
                populateStatus();
            }
        });
    }

    private void populateStatus() {
        Logger.log("");

        if (statusData == null || statusData.isEmpty()) {

            Logger.log("statusData is null");
            upNoDataView.setVisibility(View.VISIBLE);
            return;
        }

        upNoDataView.setVisibility(View.GONE);
        SSAdapter adapter = new SSAdapter(this,
                R.layout.status_row_item, statusData, mTracker);
        mRecyclerView.setAdapter(adapter);
    }

    private void followUser() {
       final boolean isFollowing = !userData.getIsCallingUserFollowing();
        if (!SSUtils.isNetworkAvailable(activity)) {
            SSUtils.displayNoNetworkMsg(activity);
            return;
        }

        if(isFollowing) {
            APIHelper.getSsapi().followUser(RequestObjectCreator.getFollowUserObject(userData.getUserId(), CURRENT_USER_ID, isFollowing), new Callback<SSBase>() {
                @Override
                public void success(SSBase SSBase, Response response) {
                    SSUtils.showSnackBarMsg(activity, "Follow Successful");

                    //send push notification on success
                    User thisUser = SharedPreferencesManager.getCurrentlyLoggedInUser(activity);
                    if (thisUser.getUserId() != userData.getUserId() && isFollowing) {
                        SSNotifUtils.sendPushNotification(userData.getUserId(), thisUser.getUserId(), thisUser.getName(), thisUser.getFbAndroidUId(), SSConstants.USER_OPERATION_FOLLOW_USER, null);
                    }

                    updateFollowOperationToUI();
                }

                @Override
                public void failure(RetrofitError error) {
                    Toast.makeText(activity, "Unable to perform operation", Toast.LENGTH_SHORT).show();
                }
            });
        }
        else{ // show alert on un follow

            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            builder.setTitle("Unfollow")
                    .setMessage("Are you sure you want to unfollow?")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            APIHelper.getSsapi().followUser(RequestObjectCreator.getFollowUserObject(userData.getUserId(), CURRENT_USER_ID, isFollowing), new Callback<SSBase>() {
                                @Override
                                public void success(SSBase SSBase, Response response) {
                                    //Toast.makeText(activity, "Unfollow Success", Toast.LENGTH_SHORT).show();
                                    updateFollowOperationToUI();
                                }

                                @Override
                                public void failure(RetrofitError error) {
                                    Toast.makeText(activity, "Unable to perform operation", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // do nothing
                        }
                    })
                    .show();
        }
    }

    private void updateFollowOperationToUI() {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            public void run() {
                ImageView upFollow = (ImageView) activity.findViewById(R.id.upFollow);

                if (userData.getIsCallingUserFollowing()) {
                    userData.setIsCallingUserFollowing(false);
                } else {
                    userData.setIsCallingUserFollowing(true);
                }

                if (userData.getIsCallingUserFollowing()) {
                    upFollow.setBackgroundResource(R.drawable.ic_user_follow_done);
                } else {
                    upFollow.setBackgroundResource(R.drawable.ic_user_follow);
                }
            }
        });
    }

    private void  viewFollowers(){
        Intent intent = new Intent(activity, UserListActivity.class);
        intent.putExtra(IntentConstants.OPERATION, IntentConstants.SHOW_FOLLOWERS);
        intent.putExtra(IntentConstants.USER_ID, userData.getUserId());
        activity.startActivity(intent);
    }

    private void  viewFollowing(){
        Intent intent = new Intent(activity, UserListActivity.class);
        intent.putExtra(IntentConstants.OPERATION, IntentConstants.SHOW_FOLLOWING);
        intent.putExtra(IntentConstants.USER_ID, userData.getUserId());
        activity.startActivity(intent);
    }

    private void showPostDialog() {
        Intent intent = new Intent(activity, StatusUpdateActivity.class);
        intent.putExtra(IntentConstants.MENTION_HANDLE, userData.getUserName());
        activity.startActivity(intent);
    }

    private void showFTUE() {
        FTUEDisplayer.showFTUEOnUserPage(activity, upFollow);
    }
}
