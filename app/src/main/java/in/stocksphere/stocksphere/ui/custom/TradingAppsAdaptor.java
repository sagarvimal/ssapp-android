package in.stocksphere.stocksphere.ui.custom;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import in.stocksphere.stocksphere.R;
import in.stocksphere.stocksphere.data.models.TradingApp;

/**
 * Created by vtiwari on 10/3/15.
 */
public class TradingAppsAdaptor extends ArrayAdapter<TradingApp> {

    private Context context;
    private int resourceId;
    private List<TradingApp> tradingApps;

    public TradingAppsAdaptor(Context context, int resource, List<TradingApp> objects) {
        super(context, resource, objects);
        this.context = context;
        this.resourceId = resource;
        this.tradingApps = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = LayoutInflater.from(context).inflate(resourceId, null);

        ImageView taAppIcon = (ImageView) convertView.findViewById(R.id.taAppIcon);
        TextView taAppName = (TextView) convertView.findViewById(R.id.taAppName);

        TradingApp tradingApp = tradingApps.get(position);

        taAppIcon.setImageDrawable(tradingApp.getAppIcon());
        taAppName.setText(tradingApp.getAppName());

        return convertView;
    }
}
