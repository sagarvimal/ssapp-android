package in.stocksphere.stocksphere;

import android.util.Log;

/**
 * Created by srishti on 06/06/15.
 */
public class Logger {
    private static Logger ourInstance = new Logger();

    public static Logger getInstance() {
        return ourInstance;
    }

    private Logger() {
    }

    public static void log(final String msg) {

        final Throwable t = new Throwable();
        final StackTraceElement[] elements = t.getStackTrace();

        final String callerClassName = elements[1].getFileName();
        final String callerMethodName = elements[1].getMethodName();

        String TAG = "[" + callerClassName + "]";

        Log.v(TAG, "[" + callerMethodName + "] " + msg);

    }
}
