package in.stocksphere.stocksphere;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookSdk;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

import in.stocksphere.stocksphere.data.common.SSNotifUtils;
import in.stocksphere.stocksphere.data.receivers.RefreshWatchlistReceiver;
import io.fabric.sdk.android.Fabric;

public class SSApplication extends Application {

    /* Google analytics tracker */
    private Tracker mTracker;

    /**
     * Gets the default {@link Tracker} for this {@link Application}.
     *
     * @return tracker
     */
    synchronized public Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
            mTracker = analytics.newTracker(R.xml.global_tracker);
        }
        return mTracker;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        //Initialize parse SDK
        SSNotifUtils.initializeParseSDK(this);

        //Subscribe for parse push notifications
        SSNotifUtils.subscribePushNotification();

        //Set alarm to refresh watch list
        RefreshWatchlistReceiver.setAlarmToRefreshWatchlist(this);

        //Initialize Crashlytics
        Fabric.with(this, new Crashlytics());

        //Initialize facebook sdk
        FacebookSdk.sdkInitialize(getApplicationContext());
    }
}
